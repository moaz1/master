
export default {
    AssignmentApiRequestBody: {
        "name": "",
        "description": "",
        "alwaysOnTop": false,
        "assignmentDeadline": null,
        "isCommon": 0,
        "allUnits": false,
        "dateCriteria": 0,
        "datePrerequisiteActivityId": null,
        "datePrerequisiteActivityHours": 0,
        "activityConditionalBeginDate": null,
        "activityConditionalEndDate": null,
        "prerequisiteActivityId": null,
        "termWeekIds": null, // ["string"]
        "activityCompletionType": 0,
        "activityComplatetionValue": 0,
        "selectedClass": null, // ["string"]
        "selectedActivityUnitsTagIds": null, // ["string"],
        "selectedActivityOwnersTagIds": null, // ["string"],
        "courseId": null,
        "fileInputKey": null
    },

    DocumentApiRequestBody: {
        "name": "",
        "description": "",
        "alwaysOnTop": false,
        "documentDeadline": null,
        "isCommon": 0,
        "allUnits": false,
        "dateCriteria": 0,
        "datePrerequisiteActivityId": null,
        "datePrerequisiteActivityHours": 0,
        "activityConditionalBeginDate": null,
        "activityConditionalEndDate": null,
        "prerequisiteActivityId": null,
        "termWeekIds": null, // ["string"],
        "activityCompletionType": 0,
        "activityComplatetionValue": 0,
        "selectedClass": null, //["string"],
        "selectedActivityUnitsTagIds": null, // ["string"],
        "selectedActivityOwnersTagIds": null, // ["string"],
        "courseId": null,
        "fileInputKey": null
    },

    LinkApiRequestBody: {
        "activityId": null,
        "isCommon": 0,
        "selectedClass": null, //["string"],
        "url": "string",
        "name": "string",
        "description": "string",
        "dateCriteria": 0,
        "showByDateBeginDate": "2019-10-02T14:28:19.902Z",
        "showByDateEndDate": "2019-10-02T14:28:19.902Z",
        "showByActivityId": "string",
        "showByActivityHourCriteria": 0,
        "alwaysTop": true,
        "deadLine": "2019-10-02T14:28:19.903Z",
        "selectedActivityUnitsTagIds": [
            "string"
        ],
        "selectedWeeks": [
            "string"
        ],
        "selectedActivityOwnersTagIds": [
            "string"
        ],
        "preConditionActivityId": "string",
        "courseId": "string",
        "fileUploadkey": "string",
        "saveAsDraft": true,
        "allUnits": true
    },

    VideoApiRequestBody: {
        "name": "",
        "description": "",
        "alwaysOnTop": false,
        "taskDeadline": null,
        "isCommon": 0,
        "allUnits": false,
        "dateCriteria": 0,
        "datePrerequisiteActivityId": null,
        "datePrerequisiteActivityHours": 0,
        "activityConditionalBeginDate": null,
        "activityConditionalEndDate": null,
        "prerequisiteActivityId": null,
        "termWeekIds": null, // ["string"]
        "activityCompletionType": 0,
        "activityComplatetionValue": 0,
        "selectedClass": null, // ["string"]
        "selectedActivityUnitsTagIds": null, // ["string"],
        "selectedActivityOwnersTagIds": null, // ["string"],
        "courseId": null,
        "fileInputKey": null,
        "fileEmbed": null,
        "allowDownload": false,
        "videoType": 0,
    },

}