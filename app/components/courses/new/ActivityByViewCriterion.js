import React from 'react';
import { } from 'react-native';
import { View, TouchableOpacity, FlatList, StyleSheet } from 'react-native';
import { CheckBox } from 'react-native-elements';

import Colors from '../../../theme/Colors';
import Constants from '../../../services/Constants';
import TextView from '../../TextView';

import { connect } from 'react-redux';
import { strings } from '../../../locales/i18n';

class ActivityByViewCriterion extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ActivityViewCriterion: 0,
            completionTypes: [
                { type: Constants.ActivityViewCriterion.ByWeek, text: this.props.main.languageResource.r_activity_add_content_view_by_week || strings('r_activity_add_content_view_by_week') },
                { type: Constants.ActivityViewCriterion.ByDateRange, text: this.props.main.languageResource.r_activity_add_content_view_by_range_date || strings('r_activity_add_content_view_by_range_date') },
                { type: Constants.ActivityViewCriterion.ByActivity, text: this.props.main.languageResource.r_activity_add_content_view_by_activity || strings('r_activity_add_content_view_by_activity') },
            ]
        }
    }
    componentDidMount() {
        console.log("completionTypes: ", this.state.completionTypes);
    }

    keyExtractor = (item, index) => item.key
    render() {
        return (
            <FlatList
                data={this.state.completionTypes}
                extraData={this.state}
                numColumns={3}
                style={{ marginTop: 5 }}
                keyExtractor={this.keyExtractor}
                renderItem={({ item }) => (
                    <TouchableOpacity style={style.criterionContainer}
                        onPress={() => { //buradaki değişiklikleri checkbox onpress'de de yapmalısın.
                            this.props.onChange(item.type)
                            this.setState({ ActivityViewCriterion: item.type })
                        }}>
                        <CheckBox
                            checkedIcon='dot-circle-o'
                            uncheckedIcon='circle-o'
                            onPress={() => {
                                this.props.onChange(item.type)
                                this.setState({ ActivityViewCriterion: item.type })
                            }}
                            checked={this.state.ActivityViewCriterion === item.type}
                            containerStyle={{ backgroundColor: 'transparent', padding: 0, margin: 0 }}
                            checkedColor={Colors.primary} />
                        <TextView weight="bold" style={style.criterionText}>{item.text}</TextView>
                    </TouchableOpacity>
                )}
            />
        )
    }
}

//STYLE
const style = StyleSheet.create({
    criterionContainer: {
        flex: 1,
        margin: 2,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.background
    },
    criterionText: {
        color: 'black',
        alignSelf: 'center',
        textAlign: 'center'
    }
})

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(ActivityByViewCriterion)