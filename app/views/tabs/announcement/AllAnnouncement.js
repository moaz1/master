import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Alert, ScrollView, RefreshControl } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

//redux
import { connect } from 'react-redux';
import AnnouncementActions from '../../../redux/AnnouncementRedux';

import AnnouncementListItem from '../../../components/announcement/AnnouncementListItem'
import Constants from '../../../services/Constants';

import { strings } from '../../../locales/i18n';
import Loader from '../../../components/Loader';

import NetInfo from '@react-native-community/netinfo';

import AnnouncementEmpty from '../../../components/announcement/AnnouncementEmpty';

import ActionButton from 'react-native-action-button';
import Colors from '../../../theme/Colors';
import { ErrorAlert } from '../../../components/ErrorAlert';

class AllAnnouncement extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            refreshing: false,
            scrolled: false,
        }
        this.announcementApiRequestBody = {
            remote: true,
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            state: Constants.AnnouncementTypes.All,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }
    }


    async componentDidMount() {
        if (this.props.announcements.announcementsData.length === 0)
            this._getAllAnnouncement();
    }

    async _getAllAnnouncement() {
        await this.setState({ scrolled: false })
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.announcementApiRequestBody.remote = true; else this.announcementApiRequestBody.remote = false
            this.props.getAnnouncements(this.announcementApiRequestBody)
        });
    }

    componentWillReceiveProps(props) {
        if (props.announcements.error) {
            Alert.alert(strings('error.error_title'), props.announcements.errorMessage)
        }
        if (!props.announcements.fetching) {
            this.setState({ refreshing: false })
        }
    }

    keyExtractor = (item, index) => index.toString()

    _onItemClick(item) {
        this.props.navigation.navigate('announcementDetail', { Announcement: item })
    }

    _onRefresh() {
        this.props.setAllAnnouncement()
        this.setState({ refreshing: true })
        this.announcementApiRequestBody.skip = 0

        this._getAllAnnouncement();
    }


    _renderMoreItem = () => {
        if (this.state.scrolled && (this.props.announcements.announcementsData.length % Constants.ApiResponseQuantity === 0)) {
            this.announcementApiRequestBody.skip = this.announcementApiRequestBody.skip + Constants.ApiResponseQuantity;
            this._getAllAnnouncement()
        }
    }


    _renderData() {
        if (this.props.announcements.announcementsData.length == 0 && !this.props.announcements.fetching) {
            return <AnnouncementEmpty />
        } else if (this.props.announcements.announcementsData.length != 0 && !this.props.announcements.error) {
            return (
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.props.announcements.announcementsData}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    onEndReached={this._renderMoreItem}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={Constants.ApiResponseQuantity}
                    onTouchStart={() => {
                        this.setState({
                            scrolled: true
                        })
                    }}
                    renderItem={({ item }) => (
                        <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column' }} onPress={() => this._onItemClick(item)}>
                            <AnnouncementListItem announcement={item} />
                        </TouchableOpacity>
                    )}
                />
            );
        } else {
            return null;
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Loader loading={this.props.announcements.fetching && !this.state.refreshing} />
                <View style={{ flex: 1 }}>
                    {this._renderData()}
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getAnnouncements: (...args) => dispatch(AnnouncementActions.getAnnouncementRequest(...args)),
        setAllAnnouncement: (...args) => dispatch(AnnouncementActions.setAllAnnouncement(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        announcements: state.announcement,
        main: state.main
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllAnnouncement);