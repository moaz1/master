import Constants from './Constants';
import fetch from '../helpers/FetchWithTimeout';
import RNFetchBlob from 'rn-fetch-blob';
const { config, fs, android } = RNFetchBlob;
let DownloadDir = fs.dirs.DownloadDir;

const create = () => {
  const getEnrolledCourses = (body) =>
    fetch(body.almsPlusApiUrl + Constants.EnrolledCourses, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + body.accessToken,
      },
      body: JSON.stringify({
        activeStatus: body.activeStatus,
        courseDateFilter: body.courseDateFilter,
        isNotifications: body.isNotifications,
        take: body.take,
        skip: body.skip,
      }),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          throw {
            error: true,
            code: response.status,
            text: response.statusText,
          };
        }
      })
      .then(function (json) {
        return json;
      })
      .catch((error) => {
        if (error.hasOwnProperty('error')) {
          //api call completed (success or fail)
          return error;
        } else {
          //api call uncompleted (connection problems.. etc.)
          return { error: true, code: 600, text: 'connection error' };
        }
      });

  //get course announcements
  const getCourseAnnouncements = (body) =>
    fetch(body.almsPlusApiUrl + Constants.CourseAnnouncement, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + body.accessToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        take: body.take,
        skip: body.skip,
        items: [
          {
            state: 1,
            contextType: body.contextType,
            contextId: body.classId,
          },
        ],
        address: body.address,
        port: body.port,
      }),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          throw {
            error: true,
            code: response.status,
            text: response.statusText,
          };
        }
      })
      .then(function (json) {
        console.log('getCourseAnnouncements api result: ', json);
        return json;
      })
      .catch((error) => {
        if (error.hasOwnProperty('error')) {
          //api call completed (success or fail)
          return error;
        } else {
          //api call uncompleted (connection problems.. etc.)
          return { error: true, code: 600, text: 'connection error' };
        }
      });

  //get incoming activities
  const getIncomingActivities = (
    take,
    skip,
    classId,
    apiEndPoint,
    accessToken
  ) =>
    fetch(apiEndPoint + Constants.IncomingActivities, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + accessToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        take: take,
        skip: skip,
        classId: classId,
      }),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          throw {
            error: true,
            code: response.status,
            text: response.statusText,
          };
        }
      })
      .then(function (json) {
        return json;
      })
      .catch((error) => {
        if (error.hasOwnProperty('error')) {
          //api call completed (success or fail)
          return error;
        } else {
          //api call uncompleted (connection problems.. etc.)
          return { error: true, code: 600, text: 'connection error' };
        }
      });

  //get course weeks
  const getCourseWeeks = (body) =>
    fetch(body.almsPlusApiUrl + Constants.CourseWeeks, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + body.accessToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        classId: body.classId,
      }),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          throw {
            error: true,
            code: response.status,
            text: response.statusText,
          };
        }
      })
      .then(function (json) {
        return json;
      })
      .catch((error) => {
        if (error.hasOwnProperty('error')) return error;
        else return { error: true, code: 600, text: 'connection error' };
      });
  //deprecated
  const getWeeksActivities = (data) => {
    fetch(data.apiEndPoint + Constants.CourseWeeksActivities, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + data.accessToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        termWeekId: data.termWeekId,
        weekZero: data.weekZero,
        classId: data.classId,
        courseId: data.courseId,
        notWeekly: data.notWeekly,
        take: data.take,
        skip: data.skip,
      }),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          throw {
            error: true,
            code: response.status,
            text: response.statusText,
          };
        }
      })
      .then(function (json) {
        return json;
      })
      .catch((error) => {
        if (error.hasOwnProperty('error')) return error;
        else return { error: true, code: 600, text: 'connection error' };
      });
  };

  const getActivityList = (body) =>
    fetch(body.almsPlusApiUrl + Constants.CourseActivityList, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + body.accessToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        getActivityType: body.getActivityType,
        classId: body.classId,
        courseId: body.courseId,
        termWeekId: body.termWeekId,
        take: body.take,
        skip: body.skip,
      }),
    })
      .then(function (response) {
        if (response.ok) {
          // console.log(' response.json()', response.json());
          console.log(
            '--------------------------------------------------------'
          );
          console.log(
            'body.almsPlusApiUrl + Constants.CourseActivityList: ',
            body.almsPlusApiUrl + Constants.CourseActivityList
          );
          console.log('getActivityType----- ', body.getActivityType);
          console.log('classId ', body.classId);
          console.log('courseId ', body.courseId);
          console.log('termWeekId ', body.termWeekId);
          console.log('take ', body.take);
          console.log('skip ', body.skip);
          console.log(
            '--------------------------------------------------------'
          );
          return response.json();
        } else {
          throw { error: true, code: response.status, text: response };
        }
      })
      .then(function (json) {
        return json;
      })
      .catch((error) => {
        if (error.hasOwnProperty('error')) return error;
        else return { error: true, code: 600, text: 'connection error' };
      });

  const getActivityDetail = (body) =>
    fetch(body.almsPlusApiUrl + Constants.CourseActivityList, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + body.accessToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        getActivityType: body.getActivityType,
        classId: body.classId,
        courseId: body.courseId,
        activityId: body.activityId,
        termWeekId: body.termWeekId,
        take: body.take,
        skip: body.skip,
      }),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          throw { error: true, code: response.status, text: response };
        }
      })
      .then(function (json) {
        return json;
      })
      .catch((error) => {
        if (error.hasOwnProperty('error')) return error;
        else return { error: true, code: 600, text: 'connection error' };
      });

  /* File download api */

  const fileDownload = (body) => {
    let options = {
      fileCache: true,
      addAndroidDownloads: {
        useDownloadManager: true, //uses the device's native download manager.
        notification: true,
        mediaScannable: true,
        // mime: 'text/plain',
        title: 'Notification Title', // Title of download notification.
        path:
          DownloadDir +
          '/' +
          body.activity.file.fileId +
          body.activity.file.extension, // this is the path where your download file will be in
        description: 'Downloading file.',
      },
    };
    return config(options)
      .fetch('GET', body.activity.file.filePath)
      .progress((received, total) => {
        console.log('progress', received / total);
      })
      .then((res) => {
        console.log('Success');
        console.log('file path:', res.path());
        let response = { path: res.path() };

        return response;
      })
      .catch((err) => {
        console.log('Error', err);
        return { error: true, code: 600, text: 'connection error' };
      });
  };

  const getTeacherCourses = (body) =>
    fetch(body.almsPlusApiUrl + Constants.TeacherCourses, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + body.accessToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        take: body.take,
        skip: body.skip,
        status: body.activeStatus,
      }),
    })
      .then(function (response) {
        if (response.ok) {
          return response.json();
        } else {
          throw {
            error: true,
            code: response.status,
            text: response.statusText,
          };
        }
      })
      .then(function (json) {
        return json;
      })
      .catch((error) => {
        console.log('getTeacherCourses api errır: ', error);

        if (error.hasOwnProperty('error')) return error;
        else return { error: true, code: 600, text: 'connection error' };
      });

  const enrollmentProgress = (body) =>
    fetch(body.almsPlusApiUrl + Constants.EnrollmentProgress, {
      method: 'POST',
      headers: {
        Authorization: 'Bearer ' + body.accessToken,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    })
      .then(function (response) {
        if (response.ok) {
          console.log('enrollmentProgress response.ok');
          return response.json();
        } else {
          throw {
            error: true,
            code: response.status,
            text: response.statusText,
          };
        }
      })
      .then(function (json) {
        console.log('enrollmentProgress result: ', json);
        return json;
      })
      .catch((error) => {
        console.log('enrollmentProgress error: ', error);
        if (error.hasOwnProperty('error')) return error;
        else return { error: true, code: 600, text: 'connection error' };
      });

  return {
    getEnrolledCourses,
    getCourseAnnouncements,
    getIncomingActivities,
    getCourseWeeks,
    getWeeksActivities,
    getActivityList,
    getActivityDetail,
    fileDownload,
    getTeacherCourses,
    enrollmentProgress,
  };
};
export default {
  create,
};
