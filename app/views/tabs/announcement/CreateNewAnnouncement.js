import React from 'react';
import { View, SafeAreaView, ScrollView, TextInput, Platform, FlatList, TouchableOpacity, Alert } from 'react-native';
import { CheckBox, Button } from 'react-native-elements';

//3.rd libraries
import { TextField } from 'react-native-material-textfield';
import { NavigationActions } from 'react-navigation';
import DateTimePicker from "react-native-modal-datetime-picker";
import { showMessage, hideMessage } from 'react-native-flash-message';
import DocumentPicker from 'react-native-document-picker';
import PercentageCircle from 'react-native-percentage-circle';
import NetInfo from '@react-native-community/netinfo';

//components and styles
import Icon from 'react-native-vector-icons/AntDesign'
import Ionicons from 'react-native-vector-icons/Ionicons';
import Uploader from '../../../components/Uploader';
import AnnouncementActions from '../../../redux/AnnouncementRedux';
import Colors from '../../../theme/Colors';
import SettingsActions from '../../../redux/SettingsRedux';
import AddActivityActions from '../../../redux/AddActivityRedux';
import Loader from '../../../components/Loader';
import TextView from '../../../components/TextView';


//settings
import Moment from 'moment'
import Constants from '../../../services/Constants';
import { connect } from 'react-redux'
import { bytesToSize, createGuid, generateMimeTypes } from '../../../helpers/Calculate';
import { strings } from '../../../locales/i18n';


class CreateNewAnnouncement extends React.Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            title: params.languageResource !== undefined ? params.languageResource.r_announcement_create_new || strings('r_announcement_create_new') : strings('r_announcement_create_new'),
            headerTitleStyle: {
                alignSelf: 'center'
            },
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, color: 'black' }} name="ios-arrow-back" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            contextType: 16,
            subject: '',
            content: '',
            fileUploadKey: createGuid(),
            isSms: false,
            isEMail: false,
            isNotification: false,
            isPin: false,
            isShowOnStartUp: false,
            smsContent: '',
            isSpecificDate: false,
            nextButtonEnabled: false,
            receivers: [],
            receiverInputText: '',
            startDate: null,
            endDate: null,
            startDateProcessing: false,
            endDateProcessing: false,
            isDatePickerVisible: false,
            minimumDate: undefined,
            selectedFile: null,
            saveAsDraft: false,
            uploadingVisibility: false,
            onBackground: false,
            uploadingSuccess: false,
            mimeTypes: null,
        }

        this.composeNewAnnouncementRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            targets: null,
            subject: null,
            content: null,
            smsContent: null,
            fileUploadKey: createGuid(),
            isSms: null,
            isEmail: null,
            isNotification: null,
            isPin: null,
            isShowOnStartUp: null,
            startDate: null,
            endDate: null

        }

        //TODO: sadece sınıf üzerinden arama yapabiliyor? 
        this.searchReceiverApiRequestBody = {
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            searchKey: '',
            getTypes: 'Class',
            take: Constants.ApiResponseQuantity,
            skip: 0
        }

        this.uploadFileRequestApiBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            selectedFile: null,
            formData: null,
            fileUploadId: null
        }

        this.changeStateApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            state: Constants.AnnouncementTypes.Draft,
            announcementId: null
        }
        this.getFileSettingsApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
        }
    }
    componentWillUnmount() {
        this.props.clearAnnouncementReceiver()
    }
    componentDidMount() {
        this.props.navigation.setParams({
            languageResource: this.props.main.languageResource
        })
        this.props.clearAnnouncementReceiver()

        if (this.props.settings.fileSettings.length === 0) {
            this.props.getFileSettings(this.getFileSettingsApiRequestBody);
        } else {
            this.setState({ mimeTypes: generateMimeTypes(this.props.settings.fileSettings.announcementSettings.extensions) })
        }


    }
    componentWillReceiveProps(props) {

        if (this.props.addActivity.uploadFileError !== props.addActivity.uploadFileError) {
            Alert.alert('', this.props.main.languageResource.r_announcement_file_upload_error || strings('r_announcement_file_upload_error'))
        }


        if (this.props.announcements.addAnnouncementResponseData !== props.announcements.addAnnouncementResponseData && !this.state.saveAsDraft) {
            showMessage({
                message: this.props.main.languageResource.r_announcement_create_success_title || strings('r_announcement_create_success_title'),
                description: this.props.main.languageResource.r_announcement_create_success_description || strings('r_announcement_create_success_description'),
                type: 'success',
                duration: 5000,
            })

            const backActions = NavigationActions.back({
                key: null
            });

            this.props.navigation.dispatch(backActions)
        }

        //save as draft
        if (this.props.announcements.addAnnouncementResponseData !== props.announcements.addAnnouncementResponseData && this.state.saveAsDraft) {
            this.changeStateApiRequestBody.announcementId = props.announcements.addAnnouncementResponseData
            this.props.changeAnnouncementState(this.changeStateApiRequestBody);
        }

        if (this.props.announcements.announcementChangeStateData !== props.announcements.announcementChangeStateData) {
            showMessage({
                message: this.props.main.languageResource.r_announcement_create_success_title || strings('r_announcement_create_success_title'),
                description: this.props.main.languageResource.r_announcement_create_success_draft_description || strings('r_announcement_create_success_draft_description'),
                type: 'success',
                duration: 5000,
            })

            const backActions = NavigationActions.back({
                key: null
            });

            this.props.navigation.dispatch(backActions)
        }

        if (this.props.settings.fileSettings !== props.settings.fileSettings) {
            this.setState({ mimeTypes: generateMimeTypes(props.settings.fileSettings.announcementSettings.extensions) })
        }

        if (this.props.announcements.addAnnouncementError !== props.announcements.addAnnouncementError && props.announcements.addAnnouncementError) {
            Alert.alert('', this.props.main.languageResource.r_announcement_create_error || strings('r_announcement_create_error'))
        }
    }

    saveAsDraft() {
        this.setState({ saveAsDraft: true })
        this.publishAnnouncement();
    }

    publishAnnouncement() {
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) {
                if (this.inputsValidation()) {
                    this.saveAnnouncementApiCall();
                }
            } else {
                Alert.alert('', this.props.main.languageResource.r_activity_connection_error || strings('r_activity_connection_error'))
                return false
            }
        });

    }

    saveAnnouncementApiCall() {
        let edittedReceivers = [];
        this.state.receivers.map(data => {
            edittedReceivers.push({
                userType: Constants.UserTypes.Student,
                contextId: data.id,
                contextType: Constants.AnnouncementContextTypes.Class
            })
        })

        this.composeNewAnnouncementRequestBody.targets = edittedReceivers
        this.composeNewAnnouncementRequestBody.subject = this.state.subject
        this.composeNewAnnouncementRequestBody.content = this.state.content
        this.composeNewAnnouncementRequestBody.smsContent = this.state.smsContent
        this.composeNewAnnouncementRequestBody.isSms = this.state.isSms
        this.composeNewAnnouncementRequestBody.isEmail = this.state.isEMail
        this.composeNewAnnouncementRequestBody.isNotification = this.state.isNotification
        this.composeNewAnnouncementRequestBody.isPin = this.state.isPin
        this.composeNewAnnouncementRequestBody.isShowOnStartUp = this.state.isShowOnStartUp
        this.composeNewAnnouncementRequestBody.startDate = this.state.startDate
        this.composeNewAnnouncementRequestBody.endDate = this.state.endDate
        // this.composeNewAnnouncementRequestBody.fileUploadKey = this.state.fileUploadKey

        console.log("composeNewAnnouncementRequestBody :", this.composeNewAnnouncementRequestBody);
        this.props.composeNewAnnouncement(this.composeNewAnnouncementRequestBody)
    }

    uploadAttachment() {
        const formData = new FormData()
        formData.append('fileUploadId', this.state.fileUploadKey)

        var file = {
            uri: this.state.selectedFile.uri,
            name: this.state.selectedFile.name,
            type: this.state.selectedFile.type,
        };
        formData.append('', file)
        console.log("form 2: ", formData);
        this.uploadFileRequestApiBody.formData = formData
        this.props.uploadFile(this.uploadFileRequestApiBody)
    }



    searchText(item) {
        this.searchReceiverApiRequestBody.searchKey = item
        if (item.length > 2) {
            this.props.searchAnnouncementReceiver(this.searchReceiverApiRequestBody)
        } else {
            this.props.clearAnnouncementReceiver()
        }
    }

    addReceiver(item) {
        let checkList = this.state.receivers.filter(receiver => receiver.id === item.id)
        if (checkList.length === 0) {
            this.setState({
                receivers: [...this.state.receivers, item],
                receiverInputText: '',
            })
        } else {
            Alert.alert('', this.props.main.languageResource.r_announcement_receiver_already_added || strings('r_announcement_receiver_already_added'))
        }

        this.props.clearAnnouncementReceiver()
    }
    removeReceiver(e) {
        let filteredArray = this.state.receivers.filter(item => item.id !== e.id)
        this.setState({ receivers: filteredArray })
    }

    async openFilePicker() {
        if (this.state.onBackground) {
            this.setState({ uploadingVisibility: true })
        } else {
            try {
                const res = await DocumentPicker.pick({
                    type: Platform.OS === "android" ? Constants.AndroidMimeTypes : Constants.IOSMimeTypes,
                });
                if (this.state.selectedFile === null || (this.state.selectedFile !== null && res.name !== this.state.selectedFile.name)) {
                    this.uploadFileRequestApiBody.selectedFile = res;
                    this.uploadFileRequestApiBody.fileUploadId = this.state.fileUploadKey;
                    this.composeNewAnnouncementRequestBody.fileUploadKey = this.state.fileUploadKey;
                    await this.setState({selectedFile:null})
                    await this.setState({ selectedFile: res, uploadingVisibility: true })
                }

            } catch (error) {
                console.log('document picker error ', error)
                if (DocumentPicker.isCancel(error)) {
                    // User cancelled the picker, exit any dialogs or menus and move on
                } else {

                    throw error;
                }
            }
        }

    }


    //FIXME: genel, system'e eşit mi?
    _renderReceiverGroups() {
        return (
            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around' }}>
                <View style={{ flex: 1 }}>
                    <CheckBox title={this.props.main.languageResource.r_announcement_receiver_group_class || strings('r_announcement_receiver_group_class')}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.contextType === Constants.AnnouncementContextTypes.Class}
                        textStyle={{ color: 'black' }}
                        onPress={() => this.setState({ contextType: Constants.AnnouncementContextTypes.Class })}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, backgroundColor: Colors.background, margin: 0 }}

                    />
                </View>
                <View style={{ flex: 1 }}>
                    <CheckBox title={this.props.main.languageResource.r_announcement_receiver_group_general || strings('r_announcement_receiver_group_general')}
                        checkedIcon='dot-circle-o'
                        uncheckedIcon='circle-o'
                        checked={this.state.contextType === Constants.AnnouncementContextTypes.System}
                        textStyle={{ color: 'black' }}
                        onPress={() => this.setState({ contextType: Constants.AnnouncementContextTypes.System })}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, backgroundColor: Colors.background, margin: 0 }}
                    />
                </View>
            </View>

        )
    }
    keyExtractor = (item, index) => item.id

    _renderSearchReceiver() {
        return (
            <View style={{ flexDirection: 'column' }}>
                <TextView weight="bold" style={{ color: 'black' }} >
                    {this.props.main.languageResource.r_announcement_receivers || strings('r_announcement_receivers')}
                </TextView>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                    {
                        this.state.receivers.map(item => (
                            <TouchableOpacity key={item.id} style={{ margin: 5, padding: 10, backgroundColor: Colors.background, flexDirection: 'row', alignItems: 'center' }}
                                onPress={() => this.removeReceiver(item)}>
                                <TextView weight="bold" style={{ color: 'black' }}>{item.value}</TextView>
                                <Ionicons name="ios-close" size={20} color="black" style={{ marginStart: 10 }} />
                            </TouchableOpacity>
                        ))
                    }
                </View>
                <TextInput
                    style={{ fontSize: 16, height: 40, borderColor: Colors.background, borderWidth: 1, marginTop: 5 }}
                    placeholder={this.props.main.languageResource.r_announcement_add_receiver_input || strings('r_announcement_add_receiver_input')}
                    onChangeText={(text) => {
                        this.searchText(text)
                        this.setState({ receiverInputText: text })
                    }}>{this.state.receiverInputText}</TextInput>
                <FlatList keyboardShouldPersistTaps={'handled'}
                    keyExtractor={this.keyExtractor}
                    data={this.props.announcements.searchAnnouncementReceiverData}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ padding: 10 }} onPress={() => this.addReceiver(item)}>
                            <TextView weight="regular" style={{ color: 'black' }}>{item.value}</TextView>
                        </TouchableOpacity>
                    )}
                />
            </View>
        )
    }


    _renderAnnouncementTitle() {
        return (
            <TextInput
                style={{ fontSize: 16, height: 40, borderColor: Colors.background, borderWidth: 1, marginTop: 5 }}
                placeholder={this.props.main.languageResource.r_announcement_title_input || strings('r_announcement_title_input')}
                onChangeText={(text) => this.setState({ subject: text })}>{this.state.subject}</TextInput>
        )
    }
    _renderAnnouncementContent() {
        return (
            <TextInput
                multiline={true}
                numberOfLines={Platform.OS === 'ios' ? null : 5}
                minHeight={Platform.OS === 'ios' ? (20 * 5) : null}
                style={{ fontSize: 16, borderColor: Colors.background, textAlignVertical: 'top', borderWidth: 1, marginTop: 5 }}
                placeholder={this.props.main.languageResource.r_announcement_description_input || strings('r_announcement_description_input')}
                onChangeText={(text) => this.setState({ content: text })}>{this.state.content}</TextInput>
        )
    }

    _renderAnnouncementAttach() {
        return (
            <TouchableOpacity
                style={{ alignItems: 'center', backgroundColor: Colors.background, flexDirection: 'row', flex: 1, marginTop: 20, padding: 10 }}
                onPress={() =>
                    this.openFilePicker()}>
                <View style={{ flex: 0.1, alignItems: 'center' }}>
                    {this.state.onBackground
                        ? <PercentageCircle
                            radius={17}
                            bgcolor={Colors.background}

                            borderWidth={3}
                            percent={this.state.uploadingPercentage}
                            color={Colors.primary}>

                            <TextView weight="medium" style={{ fontSize: 11, color: 'black' }}>
                                {this.state.uploadingPercentage}
                            </TextView>
                        </PercentageCircle>
                        : this.state.uploadingSuccess ?
                            <Ionicons size={30} name="ios-checkmark" color="black" />
                            : <Ionicons size={30} name="ios-attach" color="black" />
                    }

                </View>
                <View style={{ flex: 0.8 }}>
                    <TextView style={{ color: 'black', marginStart: 10 }} weight="bold">
                        {this.state.selectedFile !== null ? this.state.selectedFile.name + "(" + bytesToSize(this.state.selectedFile.size) + ")" : this.props.main.languageResource.r_announcement_new_add_attach || strings('r_announcement_new_add_attach')}
                    </TextView>
                </View>
                <TouchableOpacity style={{ flex: 0.1, alignItems: 'center' }}
                    onPress={() => {
                        this.setState({ selectedFile: null, onBackground: false })
                        this.composeNewAnnouncementRequestBody.fileUploadKey = createGuid();
                    }} >
                    {this.state.selectedFile !== null ? <Ionicons size={20} name="ios-close" color="black" /> : null}
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }
    _renderSmsContent() {
        if (this.state.isSms) {
            return (
                <TextField
                    containerStyle={{ margin: 0, padding: 0 }}
                    labelHeight={20}
                    label={this.props.main.languageResource.r_announcement_new_input_sms_content || strings('r_announcement_new_input_sms_content')}
                    disabled={!this.state.isSms}
                    value={this.state.smsContent}
                    onChangeText={(text) => this.setState({ smsContent: text })}
                />
            )
        } else return null
    }
    _sendSettings() {
        return (
            <View style={{ marginTop: 20 }}>
                <TextView weight="bold" style={{ color: 'black', fontSize: 17 }}>
                    {this.props.main.languageResource.r_announcement_shipping_channel_settings_text || strings('r_announcement_shipping_channel_settings_text')}
                </TextView>
                <View style={{ flexDirection: 'column', marginTop: 5 }}>
                    <CheckBox title={this.props.main.languageResource.r_announcement_send_as_email || strings('r_announcement_send_as_email')}
                        checked={this.state.isEMail}
                        textStyle={{ color: 'black' }}
                        onPress={() => this.setState({ isEMail: !this.state.isEMail })}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, margin: 0, padding: 5, backgroundColor: 'white', }}
                    />
                    <CheckBox title={this.props.main.languageResource.r_announcement_send_as_sms || strings('r_announcement_send_as_sms')}
                        checked={this.state.isSms}
                        textStyle={{ color: 'black' }}
                        onPress={() => this.setState({ isSms: !this.state.isSms })}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, margin: 0, padding: 5, backgroundColor: 'white', }}
                    />
                    <CheckBox title={this.props.main.languageResource.r_announcement_send_as_notification || strings('r_announcement_send_as_notification')}
                        checked={this.state.isNotification}
                        textStyle={{ color: 'black' }}
                        onPress={() => this.setState({ isNotification: !this.state.isNotification })}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, margin: 0, padding: 5, backgroundColor: 'white' }}
                    />
                    {this._renderSmsContent()}

                </View>

            </View>
        )
    }

    _renderSpecificDatePicker() {
        if (this.state.isSpecificDate)
            return (
                <View>
                    <TextView style={{ marginStart: 29, fontSize: 13 }}>
                        {this.props.main.languageResource.r_announcement_specific_date_selected_message || strings('r_announcement_specific_date_selected_message')}
                    </TextView>
                    <View style={{ flexDirection: 'row', marginTop: 5, }}>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ startDateProcessing: true, isDatePickerVisible: true, minimumDate: new Date() })}>
                            <View style={{ borderWidth: 1, padding: 10, borderColor: Colors.background, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TextView weight="bold" style={{ color: 'black' }}>
                                    {this.state.startDate !== null ? this.state.startDate : this.props.main.languageResource.r_announcement_start_date_input || strings('r_announcement_start_date_input')}
                                </TextView>
                                <Ionicons size={20} name="ios-calendar" color="black" />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ flex: 1 }}
                            onPress={() => this.state.startDate !== null ? this.setState({ endDateProcessing: true, isDatePickerVisible: true }) : null}>
                            <View style={{ borderWidth: 1, padding: 10, borderColor: Colors.background, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TextView weight="bold" style={{ color: 'black' }}>
                                    {this.state.endDate !== null ? this.state.endDate : this.props.main.languageResource.r_announcement_end_date_input || strings('r_announcement_end_date_input')}
                                </TextView>
                                <Ionicons size={20} name="ios-calendar" color="black" />
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
            )
        else return null
    }
    _viewSettings() {
        return (
            <View style={{ marginTop: 20 }}>
                <TextView weight="bold" style={{ color: 'black', fontSize: 17 }}>
                    {this.props.main.languageResource.r_announcement_view_settings || strings('r_announcement_view_settings')}
                </TextView>
                <View style={{ flexDirection: 'column', marginTop: 5 }}>
                    <CheckBox title={this.props.main.languageResource.r_announcement_pin_always_on_top || strings('r_announcement_pin_always_on_top')}
                        checked={this.state.isPin}
                        textStyle={{ color: 'black' }}
                        onPress={() => this.setState({ isPin: !this.state.isPin })}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, margin: 0, padding: 5, backgroundColor: 'white' }}
                    />
                    <CheckBox title={this.props.main.languageResource.r_announcement_show_after_login || strings('r_announcement_show_after_login')}
                        checked={this.state.isShowOnStartUp}
                        textStyle={{ color: 'black' }}
                        onPress={() => this.setState({ isShowOnStartUp: !this.state.isShowOnStartUp })}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, margin: 0, padding: 5, backgroundColor: 'white' }}
                    />
                    <CheckBox title={this.props.main.languageResource.r_announcement_show_range_date || strings('r_announcement_show_range_date')}
                        checked={this.state.isSpecificDate}
                        textStyle={{ color: 'black' }}
                        onPress={() => {
                            this.setState({ isSpecificDate: !this.state.isSpecificDate, disabledOpacity: 0.2 })
                        }}
                        checkedColor={Colors.primary} containerStyle={{ borderWidth: 0, margin: 0, padding: 5, backgroundColor: 'white' }}
                    />
                    {this._renderSpecificDatePicker()}
                </View>

            </View>
        )
    }

    _actionButtons() {
        return (
            <View style={{ flexDirection: 'row', flex: 1, justifyContent: 'space-around', marginBottom: 15 }}>
                <View style={{ flex: 1, margin: 5 }}>
                    <Button
                        buttonStyle={{ backgroundColor: '#e2e1dd' }}
                        disabled={this.state.nextButtonEnabled}
                        title={this.props.main.languageResource.r_announcement_save_as_draft || strings('r_announcement_save_as_draft')}
                        onPress={() => this.saveAsDraft()}
                        titleStyle={{ marginEnd: 8, color: 'black', fontFamily: Platform.OS === 'ios' ? 'SFProText-Medium' : 'Roboto-Medium' }} />
                </View>
                <View style={{ flex: 1, margin: 5 }}>
                    <Button
                        buttonStyle={{ backgroundColor: Colors.primary }}
                        disabled={this.state.nextButtonEnabled}
                        title={this.props.main.languageResource.r_announcement_publish || strings('r_announcement_publish')}
                        onPress={() => this.publishAnnouncement()}
                        icon={<Icon name="arrowright" size={20} color="white" />}
                        iconRight titleStyle={{ marginEnd: 8, fontFamily: Platform.OS === 'ios' ? 'SFProText-Medium' : 'Roboto-Medium' }} />
                </View>

            </View>
        )
    }
    hideDatePicker = () => {
        this.setState({ startDateProcessing: false, endDateProcessing: false, isDatePickerVisible: false })
    }
    handleDatePicked = date => {
        if (this.state.startDateProcessing) {
            this.setState({ startDate: Moment(date).format('YYYY-MM-DD'), minimumDate: date })
        } else if (this.state.endDateProcessing) {
            this.setState({ endDate: Moment(date).format('YYYY-MM-DD') })
        }
        this.hideDatePicker();
    };
    _renderDatePicker() {
        return (
            <DateTimePicker
                mode="date"
                minimumDate={this.state.minimumDate}
                isVisible={this.state.isDatePickerVisible}
                onConfirm={this.handleDatePicked}
                onCancel={this.hideDatePicker}
            />
        )
    }
    inputsValidation() {
        if (this.state.receivers.length === 0) {
            Alert.alert('', this.props.main.languageResource.r_announcement_any_selected_receiver || strings('r_announcement_any_selected_receiver'))
            return false
        }
        if (this.state.subject === "") {
            Alert.alert('', this.props.main.languageResource.r_announcement_subject_empty_error || strings('r_announcement_subject_empty_error'))
            return false
        }
        if (this.state.content === "") {
            Alert.alert('', this.props.main.languageResource.r_announcement_content_empty_error || strings('r_announcement_content_empty_error'))
            return false
        }
        if (this.state.selectedFile !== null && !this.state.uploadingSuccess) {
            Alert.alert('', this.props.main.languageResource.r_announcement_content_file_not_uplaoding_yet || strings('r_announcement_content_file_not_uplaoding_yet'))
            return false
        }
        return true

    }
    uploadOnCancel() {
        this.setState({ selectedFile: null, onBackground: false })
        this.composeNewAnnouncementRequestBody.fileUploadKey = createGuid();
    }

    uplaodOnBackground() {
        this.setState({ uploadingVisibility: false, onBackground: true })
    }
    onError() {
        Alert.alert('', this.props.main.languageResource.r_announcement_content_file_upload_error || strings('r_announcement_content_file_upload_error'))
        this.setState({ selectedFile: null, onBackground: false })
    }
    _renderUploading() {
        if (this.state.selectedFile !== null) {
            return <Uploader uploadingData={this.uploadFileRequestApiBody}
                visibility={this.state.uploadingVisibility}
                onCancel={() => this.uploadOnCancel()}
                onError={() => this.onError()}
                onBackground={() => this.uplaodOnBackground()}
                uplaodingProgress={((percentage) => this.setState({ uploadingPercentage: percentage }))}
                onSuccess={() => {
                    this.composeNewAnnouncementRequestBody.fileUploadKey = this.state.fileUploadKey
                    this.setState({ uploadingSuccess: true, onBackground: false, uploadingVisibility: false })
                }
                }
            />
        } else return null
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.props.announcements.addAnnouncementSending || this.props.addActivity.uploadFileProcessing} />
                <ScrollView style={{ padding: 10 }} keyboardShouldPersistTaps={'handled'}>
                    {/* {this._renderReceiverGroups()} */}
                    {this._renderSearchReceiver()}
                    {this._renderAnnouncementTitle()}
                    {this._renderAnnouncementContent()}
                    {this._renderAnnouncementAttach()}
                    {this._sendSettings()}
                    {this._viewSettings()}
                    {this._actionButtons()}
                    {this._renderDatePicker()}
                </ScrollView>
                {this._renderUploading()}
            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        composeNewAnnouncement: (...args) => dispatch(AnnouncementActions.addAnnouncementRequest(...args)),
        searchAnnouncementReceiver: (...args) => dispatch(AnnouncementActions.searchAnnouncementReceiverRequest(...args)),
        clearAnnouncementReceiver: (...args) => dispatch(AnnouncementActions.clearAnnouncementReceiver(...args)),
        changeAnnouncementState: (...args) => dispatch(AnnouncementActions.announcementChangeStateRequest(...args)),
        getFileSettings: (...args) => dispatch(SettingsActions.getFileSettingsRequest(...args)),
        uploadFile: (...args) => dispatch(AddActivityActions.uploadFileRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        announcements: state.announcement,
        addActivity: state.addActivity,
        settings: state.settings,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateNewAnnouncement);