import React from 'react';
import { Text, StyleSheet, Platform } from 'react-native';
export default class TextView extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        if (this.props.weight === 'bold') {
            return (
                <Text  {...this.props} style={[styles.boldStyle, this.props.style]}>
                    {this.props.children}
                </Text>
            );
        } else if (this.props.weight === 'light') {
            return (
                <Text {...this.props} style={[styles.lightStyle, this.props.style]}>
                    {this.props.children}
                </Text>
            );
        } else if (this.props.weight === 'medium') {
            return (
                <Text {...this.props} style={[styles.mediumStyle, this.props.style]}>
                    {this.props.children}
                </Text>
            );
        } else {
            return (
                <Text {...this.props} style={[styles.regularStyle, this.props.style]}>
                    {this.props.children}
                </Text>
            );
        }


    }
}
const styles = StyleSheet.create({
    boldStyle: {
        fontFamily: Platform.OS === 'ios' ? 'SFProText-Bold' : 'Roboto-Bold'
    },
    lightStyle: {
        fontFamily: Platform.OS === 'ios' ? 'SFProText-Light' : 'Roboto-Light'
    },
    mediumStyle: {
        fontFamily: Platform.OS === 'ios' ? 'SFProText-Medium' : 'Roboto-Medium'
    },
    regularStyle: {
        fontFamily: Platform.OS === 'ios' ? 'SFProText-Regular' : 'Roboto-Regular'
    }
});