import React from 'react';
import { View, Alert, FlatList, TouchableOpacity, ScrollView, RefreshControl } from 'react-native';
import NetInfo from '@react-native-community/netinfo';

//redux
import { connect } from 'react-redux';
import AnnouncementActions from '../../../redux/AnnouncementRedux';

import Constants from '../../../services/Constants';
import { strings } from '../../../locales/i18n';

import AnnouncementListItem from '../../../components/announcement/AnnouncementListItem'
import AnnouncementEmpty from '../../../components/announcement/AnnouncementEmpty';
import Loader from '../../../components/Loader';
class DeletedAnnouncement extends React.Component {
  
    constructor(props) {
        super(props);

        this.state = {
            refreshing: false,
            scrolled: false,

        }
        this.announcementApiRequestBody = {
            remote: true,
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            state: Constants.AnnouncementTypes.Deleted,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }
    }

    componentDidMount() {
        this._getDeletedAnnouncements()
    }

    async _getDeletedAnnouncements() {
        await this.setState({ scrolled: false })

        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.announcementApiRequestBody.remote = true; else this.announcementApiRequestBody.remote = false
            this.props.getDeletedAnnouncements(this.announcementApiRequestBody)
        })
    }
    componentWillReceiveProps(props) {
        if (props.announcements.announcementsDeletedError) {
            Alert.alert(strings('error.error_title'), props.announcements.announcementsDeletedErrorMessage)
        }

        if (!props.announcements.announcementsDeletedFetching) {
            this.setState({
                refreshing: false
            });
        }
    }
    keyExtractor = (item, index) => index.toString()

    _onItemClick(item) {
        this.props.navigation.navigate('announcementDetail', { Announcement: item })
    }

    _onRefresh() {
        this.props.setDeletedAnnouncement()
        this.announcementApiRequestBody.skip = 0
        this.setState({ refreshing: true })
        this._getDeletedAnnouncements();
    }


    _renderMoreItem = () => {
        if (this.state.scrolled && (this.props.announcements.announcementsDeletedData.length % Constants.ApiResponseQuantity === 0)) {
            this.announcementApiRequestBody.skip = this.announcementApiRequestBody.skip + Constants.ApiResponseQuantity;
            this._getDeletedAnnouncements()
        }
    }

    _renderData() {
        if (this.props.announcements.announcementsDeletedData.length == 0 && !this.props.announcements.announcementsDeletedFetching) {
            return (
                <AnnouncementEmpty />
            )
        } else if (!this.props.announcements.announcementsDeletedError && this.props.announcements.announcementsDeletedData.length != 0) {
            return (
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.props.announcements.announcementsDeletedData}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    onEndReached={this._renderMoreItem}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={Constants.ApiResponseQuantity}
                    onTouchStart={() => {
                        this.setState({
                            scrolled: true
                        })
                    }}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ flexDirection: 'column' }} onPress={() => this._onItemClick(item)}>
                            <AnnouncementListItem announcement={item} />
                        </TouchableOpacity>
                    )} />
            )
        } else {
            return null;
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Loader loading={this.props.announcements.announcementsDeletedFetching && !this.state.refreshing} />
                <View style={{ flex: 1 }}>
                    {this._renderData()}
                </View>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getDeletedAnnouncements: (...args) => dispatch(AnnouncementActions.getAnnouncementDeletedRequest(...args)),
        setDeletedAnnouncement: (...args) => dispatch(AnnouncementActions.setDeletedAnnouncement(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        announcements: state.announcement,
        main: state.main,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DeletedAnnouncement);