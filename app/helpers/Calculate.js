import { Dimensions, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info'
import Constants from '../services/Constants';

import * as mime from 'react-native-mime-types';

//Calculating bytes to kb,mb,gb..
export function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}


const isPortrait = (width, height) => {
    return height >= width
}
const isLandscape = (width, height) => {
    return width >= height
}

export function calculateLoginPageWidthPercent() {
    const { width, height } = Dimensions.get('screen');

    // console.log("calculateLoginPageWidthPercent: ", width)
    if (DeviceInfo.isTablet()) {
        // console.log("tablet active")
        return Math.min(width) * 0.5
    } else {
        if (isLandscape(width, height))
            return Math.min(width) * 0.5
        else
            return width
    }
}

export function createGuid() {
    function S4() {
        return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    }
    return 'm_' + (S4() + S4() + "-" + S4() + "-4" + S4().substr(0, 3) + "-" + S4() + "-" + S4() + S4() + S4()).toLowerCase();
}

export function generateMimeTypes(extensions) {
    var extensionsArray = extensions.split(',')
    var mimeTypes = [];
    extensionsArray.forEach(function (extension) {
        if (mime.lookup(extension) !== false)
            mimeTypes.push(mime.lookup(extension))
    })
    console.log("generated mime types: ",mimeTypes);
    return mimeTypes;
}