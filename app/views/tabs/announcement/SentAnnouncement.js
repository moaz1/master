import React from 'react';
import { View, Text, FlatList, TouchableOpacity, Alert, ScrollView, RefreshControl } from 'react-native';
import Constants from '../../../services/Constants';
import NetInfo from '@react-native-community/netinfo';

//redux
import { connect } from 'react-redux';
import AnnouncementActions from '../../../redux/AnnouncementRedux';

import AnnouncementListItem from '../../../components/announcement/AnnouncementListItem'
import AnnouncementEmpty from '../../../components/announcement/AnnouncementEmpty';
import Loader from '../../../components/Loader';

class SentAnnouncement extends React.Component {
    
    constructor(props) {
        super(props)

        this.state = {
            refreshing: false,
            scrolled: false,
        }

        this.announcementApiRequestBody = {
            remote: true,
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            state: Constants.AnnouncementTypes.Sent,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }
    }
    componentDidMount() {
        this._getSentAnnouncements()
    }
    async _getSentAnnouncements() {
        await this.setState({ scrolled: false })

        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.announcementApiRequestBody.remote = true; else this.announcementApiRequestBody.remote = false;
            this.props.getSentAnnouncements(this.announcementApiRequestBody)

        })
    }

    componentWillReceiveProps(props) {
        if (props.announcements.announcementsSentError) {
            Alert.alert(strings('error.error_title'), props.announcements.announcementsSentErrorMessage, [
                { text: strings('common.ok') },
            ])
        }

        if (!props.announcements.announcementsInboxFetching) {
            this.setState({ refreshing: false })
        }
    }

    keyExtractor = (item, index) => index.toString()

    _onItemClick(item) {
        this.props.navigation.navigate('announcementDetail', { Announcement: item })
    }

    _onRefresh() {
        this.props.setSentAnnouncement()
        this.announcementApiRequestBody.skip = 0
        this.setState({ refreshing: true })
        this._getSentAnnouncements();
    }

    _renderMoreItem = () => {
        console.log("scrolled _renderMoreItem: ", this.state.scrolled);
        console.log("calculate: ", this.props.announcements.announcementsSentData.length % Constants.ApiResponseQuantity);
        if (this.state.scrolled && (this.props.announcements.announcementsSentData.length % Constants.ApiResponseQuantity === 0)) {
            console.log("Load more item")
            this.announcementApiRequestBody.skip = this.announcementApiRequestBody.skip + Constants.ApiResponseQuantity;
            this._getSentAnnouncements()
        }
    }

    _renderData() {
        if (this.props.announcements.announcementsSentData.length == 0 && !this.props.announcements.announcementsSentFetching) {
            return (<AnnouncementEmpty />);
        } else if (!this.props.announcements.announcementsSentError && this.props.announcements.announcementsSentData.length != 0) {
            return (
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.props.announcements.announcementsSentData}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    onEndReached={this._renderMoreItem}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={Constants.ApiResponseQuantity}
                    onTouchStart={() => {
                        this.setState({
                            scrolled: true
                        })
                    }}
                    renderItem={({ item }) => (
                        <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column' }} onPress={() => this._onItemClick(item)}>
                            <AnnouncementListItem announcement={item} />
                        </TouchableOpacity>
                    )} />

            )
        } else {
            return null;
        }
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <Loader loading={this.props.announcements.announcementsSentFetching && !this.state.refreshing} />
                <View style={{ flex: 1 }}>
                    {this._renderData()}
                </View>

            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getSentAnnouncements: (...args) => dispatch(AnnouncementActions.getAnnouncementSentRequest(...args)),
        setSentAnnouncement: (...args) => dispatch(AnnouncementActions.setSentAnnouncement(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        announcements: state.announcement,
        main: state.main,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SentAnnouncement);