//react libraries
import React from 'react';
import { View, TouchableOpacity, ScrollView, Linking } from 'react-native';
import { Card, Button } from 'react-native-elements';

//3.rd libraries
import Ionicons from 'react-native-vector-icons/Ionicons';
import Moment from 'moment';
import HTML from 'react-native-render-html';
import PercentageCircle from 'react-native-percentage-circle';

//component &  styles
import Colors from '../../theme/Colors';
import ActivityInteractActions from '../../redux/ActivityInteractRedux';
import CoursesActions from '../../redux/CoursesRedux';
import styles from '../../theme/Style';
import TextView from '../TextView';

//settings
import { connect } from 'react-redux';
import { strings } from '../../locales/i18n';
import { template } from '../../locales/StringTemplate';
import Constants from '../../services/Constants';

class ActivityExamType extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      activity: props.activity,
      image: { uri: props.activity.cardImgName },
    };
  }

  _renderDueDate() {
    if (this.state.activity.taskDeadLine !== null) {
      var curDate = Moment();
      var isAfter = Moment(curDate).isAfter(this.state.activity.taskDeadLine);
      if (isAfter) {
        return (
          <View style={{ flexDirection: 'row' }}>
            <TextView
              weight="bold"
              style={{ fontSize: 13, color: Colors.activity_due_date_past }}
            >
              {this.props.main.languageResource.r_activity_due_date ||
                strings('r_activity_due_date')}
            </TextView>
            <TextView
              weight="regular"
              style={{
                fontSize: 12,
                marginStart: 5,
                color: Colors.activity_due_date_past,
              }}
            >
              {this.props.main.languageResource.r_activiy_due_outdate_day !==
              undefined
                ? template(
                    this.props.main.languageResource.r_activiy_due_outdate_day,
                    {
                      day: curDate.diff(
                        this.state.activity.taskDeadLine,
                        'days'
                      ),
                    }
                  )
                : curDate.diff(this.state.activity.taskDeadLine, 'days') !== 0
                ? template(strings('r_activiy_due_outdate_day'), {
                    day: curDate.diff(this.state.activity.taskDeadLine, 'days'),
                  })
                : this.props.main.languageResource.r_activity_due_date_today ||
                  strings('r_activity_due_date_today')}
            </TextView>
          </View>
        );
      } else {
        return (
          <View style={{ flexDirection: 'row' }}>
            <TextView weight="bold" style={{ fontSize: 12 }}>
              {this.props.main.languageResource.r_activity_due_date ||
                strings('r_activity_due_date')}
            </TextView>
            <TextView weight="regular" style={{ fontSize: 12, marginStart: 5 }}>
              {Moment(this.state.activity.taskDeadLine).format(
                'D MMMM, hh:mm '
              )}
            </TextView>
          </View>
        );
      }
    } else return null;
  }

  _renderCompletionIcon() {
    const enrollmentData = this.props.courses.enrollmentProgressData.find(
      (data) => data.activityId === this.state.activity.activityId
    );
    if (enrollmentData) {
      if (
        enrollmentData.status === Constants.ActivityCompletedStatus.Completed
      ) {
        return (
          <View
            style={{
              flex: 0.1,
              alignItems: 'center',
              justifyContent: 'center',
              height: styles.marked_completed_activity_icon_size,
              width: 30,
              borderRadius: 30 / 2,
              backgroundColor: Colors.primary,
            }}
          >
            <Ionicons
              name="ios-checkmark"
              size={styles.marked_completed_activity_icon_size}
              color="white"
            />
          </View>
        );
      } else
        return (
          <View
            style={{
              flex: 0.1,
              alignItems: 'center',
              justifyContent: 'center',
              height: styles.marked_completed_activity_icon_size,
              width: 30,
              borderRadius: 30 / 2,
              backgroundColor: Colors.primary,
            }}
          >
            <PercentageCircle
              radius={17}
              borderWidth={3}
              percent={enrollmentData.progress}
              color={Colors.primary}
            >
              <TextView weight="medium" style={{ fontSize: 11 }}>
                %{enrollmentData.progress}
              </TextView>
            </PercentageCircle>
          </View>
        );
    } else return null;
  }

  _renderCardHeader() {
    return (
      <View style={{ flexDirection: 'row', padding: 10 }}>
        <Ionicons
          style={{ flex: 0.1 }}
          name="ios-list"
          size={25}
          color="black"
        />
        <View style={{ flex: 0.8, flexDirection: 'column', marginStart: 5 }}>
          <TextView
            numberOfLines={2}
            weight="bold"
            style={{ color: 'black', fontSize: 18 }}
          >
            {this.state.activity.name}
          </TextView>
          {this._renderDueDate()}
        </View>
        {this._renderCompletionIcon()}
      </View>
    );
  }

  //TODO:changed by moaz
  /*
  creater a now two function

 getAttemptCount() {
    return this.state.activity.exam == null
      ? 0
      : this.state.activity.exam.attemptCount;
  }
  getAllowedCount() {
    return this.state.activity.exam == null
      ? 0
      : this.state.activity.exam.allowedCount;
  }
  
and changed code from 

attemptCount: this.state.activity.exam.attemptCount 
repeatCount: this.state.activity.exam.allowedCount 

to

  attemptCount: this.getAttemptCount(),
  repeatCount: this.getAllowedCount(),


  and  this line from
      if (this.state.activity.exam.attemptCount < this.state.activity.exam.allowedCount)
      to 
     if (this.getAttemptCount() < this.getAllowedCount())

    reason of change  
    because there no exam when first program start , and attemptCount , repeatCount can't have a null value in the first 
    */
  getAttemptCount() {
    return this.state.activity.exam == null
      ? 0
      : this.state.activity.exam.attemptCount;
  }
  getAllowedCount() {
    return this.state.activity.exam == null
      ? 0
      : this.state.activity.exam.allowedCount;
  }

  _renderCardContainer() {
    return (
      <View style={{ marginTop: 10, padding: 10 }}>
        <Card
          containerStyle={{
            flex: 1,
            flexDirection: 'column',
            margin: 0,
            padding: 0,
          }}
        >
          <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
            <TextView weight="bold" style={{ flex: 0.5, color: 'black' }}>
              {this.props.main.languageResource.r_elesson_exam_type_text ||
                strings('r_elesson_exam_type_text')}
            </TextView>
            <TextView
              weight="regular"
              style={{ flex: 0.5, color: 'black' }}
            ></TextView>
          </View>

          <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
            <TextView weight="bold" style={{ flex: 0.5, color: 'black' }}>
              {this.props.main.languageResource.r_elesson_grade_type_text ||
                strings('r_elesson_grade_type_text')}
            </TextView>
            <TextView
              weight="regular"
              style={{ flex: 0.5, color: 'black' }}
            ></TextView>
          </View>

          <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
            <TextView weight="bold" style={{ flex: 0.5, color: 'black' }}>
              {this.props.main.languageResource.r_elesson_start_date_text ||
                strings('r_elesson_start_date_text')}
            </TextView>
            <TextView weight="regular" style={{ flex: 0.5, color: 'black' }}>
              {this.state.activity.beginDate !== null
                ? Moment(this.state.activity.beginDate).format(
                    'DD.MM.YYYY / hh:mm'
                  )
                : null}{' '}
            </TextView>
          </View>

          <View style={{ flexDirection: 'row', flex: 1, padding: 10 }}>
            <TextView weight="bold" style={{ flex: 0.5, color: 'black' }}>
              {this.props.main.languageResource.r_elesson_end_date_text ||
                strings('r_elesson_end_date_text')}
            </TextView>
            <TextView weight="regular" style={{ flex: 0.5, color: 'black' }}>
              {this.state.activity.endDate !== null
                ? Moment(this.state.activity.endDate).format(
                    'DD.MM.YYYY / hh:mm'
                  )
                : null}{' '}
            </TextView>
          </View>
        </Card>

        {this._renderDescription()}

        <View
          style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}
        >
          <Ionicons
            name="ios-information-circle-outline"
            size={25}
            color={Colors.warning}
          />

          <TextView
            weight="medium"
            style={{ fontSize: 13, marginStart: 5, color: 'black' }}
          >
            {this.props.main.languageResource.r_elesson_repeat_counter_text !==
            undefined
              ? template(
                  this.props.main.languageResource
                    .r_elesson_repeat_counter_text,
                  {
                    attemptCount: getAttemptCount(),
                    repeatCount: getAllowedCount(),
                  }
                )
              : template(strings('r_elesson_repeat_counter_text'), {
                  attemptCount: this.getAttemptCount(),
                  repeatCount: this.getAllowedCount(),
                })}
          </TextView>
        </View>
      </View>
    );
  }

  _renderDescription() {
    if (this.state.activity.description != null) {
      return (
        <View style={{ padding: 0, marginTop: 10 }}>
          <HTML html={this.state.activity.description} />
        </View>
      );
    } else return null;
  }
  openExamDetail() {
    //Linking.openURL(this.props.main.selectedOrganization.almsPlusApiUrl + this.state.activity.activityDetailUrl)
    this.props.navigation.navigate('ActivityDetailWebView', {
      activity: this.state.activity,
    });
  }

  _renderActionButtons() {
    if (this.getAttemptCount() < this.getAllowedCount())
      return (
        <View>
          <View style={{ flexDirection: 'row', alignContent: 'center' }}>
            <View style={{ flex: 0.5 }} />
            <View style={{ justifyContent: 'flex-end', flex: 0.5, padding: 5 }}>
              <Button
                icon={<Ionicons name="ios-eye" size={25} color="white" />}
                onPress={() => this.openExamDetail()}
                title={
                  this.props.main.languageResource.r_activity_show_btn_text ||
                  strings('r_activity_show_btn_text')
                }
                titleStyle={{ marginStart: 5 }}
                buttonStyle={{ backgroundColor: Colors.primary }}
              />
            </View>
          </View>
        </View>
      );
  }
  render() {
    return (
      <View>
        <ScrollView>
          <Card
            containerStyle={{
              backgroundColor: 'white',
              flexDirection: 'column',
              margin: 5,
              padding: 0,
            }}
          >
            <View>
              {/* Card Header */}
              {this._renderCardHeader()}

              {/* Card Container */}
              {this._renderCardContainer()}
              <View
                style={{
                  marginTop: 10,
                  height: 1,
                  backgroundColor: Colors.lineColor,
                }}
              />
              {/* Card Action Buttons */}
              {this._renderActionButtons()}
            </View>
          </Card>
        </ScrollView>
      </View>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setAsCompleted: (...args) =>
      dispatch(
        ActivityInteractActions.activityCompletionViewCriteriaRequest(...args)
      ),
    getEnrollmentProgress: (...args) =>
      dispatch(CoursesActions.getEnrollmentProgressRequest(...args)),
  };
};

const mapStateToProps = (state) => {
  return {
    main: state.main,
    courseDetail: state.courseDetail,
    activityInteract: state.activityInteract,
    courses: state.courses,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ActivityExamType);
