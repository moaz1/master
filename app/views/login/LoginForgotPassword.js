import React from 'react';
import { View, Text, ImageBackground, StatusBar, Platform, TouchableOpacity, SafeAreaView } from 'react-native'

import { Card, Input, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/AntDesign'
import { strings } from '../../locales/i18n'

//assets
import login_bg from '../../assets/images/login-bg.jpg';
import Colors from '../../theme/Colors';
import TextView from '../../components/TextView';
import FontSize from '../../theme/FontSize';

export default class LoginForgotPassword extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            nextButtonDisabled: true
        }
    }
    inputChange(text) {
        if (text.length === 0) {
            this.setState({ nextButtonDisabled: true })
        } else {
            this.setState({ nextButtonDisabled: false })
        }
    }

    render() {
        const { navigate } = this.props.navigation;

        return (
            <View style={{ flex: 1 }}>
                <ImageBackground source={login_bg} style={{ width: '100%', height: '100%', position: 'absolute' }} />
                <SafeAreaView style={{ flex: 1 }}>
                    <StatusBar hidden={true} />
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Card containerStyle={{ padding: 20, backgroundColor: Colors.login_card_background, margin: 20, borderRadius: 5 }}>

                            <TextView style={{ fontSize: FontSize.login_header, color: Colors.login_card_text }} weight="bold">{strings('login.forgot_password_title')}</TextView>
                            <TextView style={{ fontSize: 15, color: Colors.login_card_text,marginTop:10 }} weight="regular">{strings('login.forgot_password_description')}</TextView>

                            <Input keyboardType={"email-address"} placeholder={strings('login.login_input_only_email')} shake={true} containerStyle={{ marginTop: 10 }} onChangeText={(text) => this.inputChange(text)} />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20 }}>
                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => navigate('SignIn')} >
                                    <Icon name="arrowleft" size={20} color="black" />
                                    <TextView style={{ paddingStart: 10}} >{strings('common.back')}</TextView>
                                </TouchableOpacity>

                                <Button
                                    buttonStyle={{ backgroundColor: Colors.primary }}
                                    disabled={this.state.nextButtonDisabled}
                                    title={strings('common.send')}
                                    icon={<Icon name="arrowright" size={20} color="white" />}
                                    iconRight
                                    titleStyle={{ marginEnd: 8 }} />
                            </View>

                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 50 }}>
                                <Text style={{ fontSize: FontSize.login_alms }}>2013-{new Date().getFullYear()} {strings('login.alms')}</Text>
                            </View>
                        </Card>

                    </View>

                </SafeAreaView>
            </View>
        );
    }
}
