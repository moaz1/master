import React from 'react';
import { View, Text, Picker, Dimensions, TouchableOpacity, ScrollView, FlatList, SafeAreaView, Platform, RefreshControl } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Ionicons from 'react-native-vector-icons/Ionicons';
import NetInfo from '@react-native-community/netinfo';

//redux
import { connect } from 'react-redux';
import CourseDetailActions from '../../../../redux/CourseDetailRedux';
import CoursesActions from '../../../../redux/CoursesRedux';
import ActionSheet from 'react-native-actionsheet';
import TextView from '../../../../components/TextView';
import Moment from 'moment';
import ActionButton from 'react-native-action-button';
import Constants from '../../../../services/Constants';

//activity card
import ActivityDictionaryType from '../../../../components/courses/ActivityDictionaryType';
import ActivityDocumentType from '../../../../components/courses/ActivityDocumentType';
import ActivityELessonType from '../../../../components/courses/ActivityELessonType';
import ActivityVideoType from '../../../../components/courses/ActivityVideoType';
import ActivitySurveyType from '../../../../components/courses/ActivitySurveyType';
import ActivityLinkType from '../../../../components/courses/ActivityLinkType';
import ActivityAssignmentText from '../../../../components/courses/ActivityAssignmentText';
import ActivityExamType from '../../../../components/courses/ActivityExamType';
import ActivityVirtualClassType from '../../../../components/courses/ActivityVirtualClassType';

import PageEmpty from '../../../../components/courses/PageEmpty';
import Loader from '../../../../components/Loader';
import Colors from '../../../../theme/Colors';

import { strings } from '../../../../locales/i18n';
import FontSize from '../../../../theme/FontSize';
import { template } from '../../../../locales/StringTemplate';


class CourseActivities extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            weeksReady: false,
            selectedWeek: '',
            menuPosition: 0,
            scrolled: false,
            width: Dimensions.get('window').width,
            height: Dimensions.get('window').height,
            refreshing: false,
            orientation: false,
        }

        this.activityListApiRequestBody = {
            remote: null,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            getActivityType: Constants.ActivityListType.GetActivitiesBySelectedWeek,
            classId: null,
            courseId: null,
            termWeekId: null,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }

        this.courseWeeksApiRequestBody = {
            remote: true,
            classId: null,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
        }

        this.enrollmentProgressApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            remote: true,
        }

        this._onLayout = this._onLayout.bind(this);

    }



    componentDidMount() {


        this.courseWeeksApiRequestBody.classId = this.props.courseDetail.course.classId

        this.activityListApiRequestBody.classId = this.props.courseDetail.course.classId
        this.activityListApiRequestBody.courseId = this.props.courseDetail.course.courseId

        NetInfo.fetch().then(statu => {
            if (statu.isConnected) {
                this.courseWeeksApiRequestBody.remote = true
                this.enrollmentProgressApiRequestBody.remote = true
            }
            else {
                this.courseWeeksApiRequestBody.remote = false
                this.enrollmentProgressApiRequestBody.remote = false
            }
            console.log("test");
            this.props.getCourseWeeks(this.courseWeeksApiRequestBody);
            this.props.getEnrollmentProgress(this.enrollmentProgressApiRequestBody)

        })

    }


    componentWillUnmount() {
    }

    componentWillReceiveProps(props) {
        if (this.props.courseDetail.courseWeeks != props.courseDetail.courseWeeks && props.courseDetail.courseWeeks.length !== 0) {
            this.setState({ weeksReady: true, selectedWeek: props.courseDetail.courseWeeks[this.state.menuPosition] })

            this.activityListApiRequestBody.termWeekId = this.state.menuPosition !== 0 ? props.courseDetail.courseWeeks[this.state.menuPosition].termWeekId : null
            this._getActivityList();
        }

        if (props.courseDetail.activityList !== this.props.courseDetail.activityList) {
            console.log("activityListFetching success");
            this.setState({ refreshing: false })
        }
    }

    _getActivityList() {
        this.setState({ scrolled: false })
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.activityListApiRequestBody.remote = true
            else this.activityListApiRequestBody.remote = false

            this.props.getActivityList(this.activityListApiRequestBody)
        })
    }

    async _weekSelected(week, index) {
        console.log("_weekSelected :", week.termWeekId);
        console.log("_weekSelected :", index);
        await this.props.setActivityList()
        this.setState({ selectedWeek: week, menuPosition: index })

        //tanımlamamış haftaların aktivitelerini almak için api terweekId:null gönderilmesli.
        this.activityListApiRequestBody.skip = 0
        this.activityListApiRequestBody.termWeekId = index !== 0 ? week.termWeekId : null
        this._getActivityList();
    }

    keyExtractor = (item, index) => index.toString()

    async _previousWeek() {
        if (this.state.menuPosition !== 0) {
            await this.props.setActivityList()
            this.activityListApiRequestBody.skip = 0
            this.activityListApiRequestBody.termWeekId = this.props.courseDetail.courseWeeks[this.state.menuPosition - 1].termWeekId
            this._getActivityList();

            this.setState({
                selectedWeek: this.props.courseDetail.courseWeeks[this.state.menuPosition - 1],
                menuPosition: this.state.menuPosition - 1
            })
        }

    }
    async _forwardWeek() {

        if (this.state.menuPosition + 1 !== this.props.courseDetail.courseWeeks.length) {
            await this.props.setActivityList()
            this.activityListApiRequestBody.skip = 0
            this.activityListApiRequestBody.termWeekId = this.props.courseDetail.courseWeeks[this.state.menuPosition + 1].termWeekId
            this._getActivityList();
            this.setState({
                selectedWeek: this.props.courseDetail.courseWeeks[this.state.menuPosition + 1],
                menuPosition: this.state.menuPosition + 1
            })
        }
    }

    _renderMenuText() {

        if (this.state.selectedWeek.week !== null) {
            if (this.state.selectedWeek.week === 0) {
                return (
                    <View style={{ marginTop: 5, marginBottom: 5 }}>
                        <TextView weight="bold" style={{ color: Colors.course_weeks_menu_item_text_color, fontSize: FontSize.course_weeks_item }}>
                            {this.props.main.languageResource.r_course_unplanned_activities || strings('r_course_unplanned_activities')}
                        </TextView>
                    </View>
                );
            } else {
                return (
                    <View style={{ alignItems: 'center' }}>
                        <TextView weight="bold" style={{ color: Colors.course_weeks_menu_item_text_color, fontSize: FontSize.course_weeks_item }}>
                            {this.props.main.languageResource.r_course_week_text !== undefined
                                ? template(this.props.main.languageResource.r_course_week_text, { weekName: this.state.selectedWeek.week })
                                : template(strings('r_course_week_text'), { weekName: this.state.selectedWeek.week })}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: FontSize.course_weeks_item_description }}>
                            {Moment(this.state.selectedWeek.startDate).format('DD') + " - " + Moment(this.state.selectedWeek.endDate).format('DD MMMM YYYY')}
                        </TextView>
                    </View>
                );
            }
        } else {
            return (
                <View style={{ marginTop: 5, marginBottom: 5 }}>
                    <TextView weight="bold" style={{ color: Colors.course_weeks_menu_item_text_color, fontSize: FontSize.course_weeks_item }}>
                        {this.props.main.languageResource.r_course_unplanned_activities || strings('r_course_unplanned_activities')}
                    </TextView>
                </View>
            );
        }

    }


    showWeekActionSheet = () => {
        this.ActionSheet.show()
    }


    _renderPicker() {
        const menuOptions = []; //menuOptions contains only string on the ios platform
        this.props.courseDetail.courseWeeks.map((data, index) => {
            if (data.week !== null) {
                if (data.week === 0) {
                    if (Platform.OS === 'android')
                        menuOptions.push(
                            <TextView weight="bold" style={{ color: Colors.course_weeks_menu_item_text_color, fontSize: FontSize.course_weeks_item }}>
                                {this.props.main.languageResource.r_course_unplanned_activities || strings('r_course_unplanned_activities')}
                            </TextView>)
                    else
                        menuOptions.push(this.props.main.languageResource.r_course_unplanned_activities || strings('r_course_unplanned_activities'))
                } else {
                    if (Platform.OS === 'android') {
                        menuOptions.push(
                            <View style={{ alignItems: 'center' }}>
                                <TextView weight="bold" style={{ color: Colors.course_weeks_menu_item_text_color, fontSize: 15 }}>
                                    {this.props.main.languageResource.r_course_week_text !== undefined
                                        ? template(this.props.main.languageResource.r_course_week_text, { weekName: data.week })
                                        : template(strings('r_course_week_text'), { weekName: data.week })}
                                </TextView>
                                <TextView weight="regular" style={{ fontSize: 13 }}>
                                    {Moment(data.startDate).format('DD MMMM') + " - " + Moment(data.endDate).format('DD MMMM YYYY')}
                                </TextView>
                            </View>
                        )
                    } else {
                        menuOptions.push(this.props.main.languageResource.r_course_week_text !== undefined
                            ? template(this.props.main.languageResource.r_course_week_text, { weekName: data.week })
                            : template(strings('r_course_week_text'), { weekName: data.week }))
                    }
                }
            } else {
                if (Platform.OS === 'android')
                    menuOptions.push(
                        <TextView weight="bold" style={{ color: Colors.course_weeks_menu_item_text_color, fontSize: FontSize.course_weeks_item }}>
                            {this.props.main.languageResource.r_course_unplanned_activities || strings('r_course_unplanned_activities')}
                        </TextView>)
                else
                    menuOptions.push(this.props.main.languageResource.r_course_unplanned_activities || strings('r_course_unplanned_activities'))
            }

        })

        if (Platform.OS === 'android') {
            menuOptions.push(<TextView weight="bold" style={{ color: 'black', fontSize: 20 }}>
                {this.props.main.languageResource.r_course_week_menu_close || strings('r_course_week_menu_close')}
            </TextView>);
        } else {
            menuOptions.push(this.props.main.languageResource.r_course_week_menu_close || strings('r_course_week_menu_close'))
        }

        if (this.state.weeksReady) {
            return (
                <View style={{ flexDirection: 'column' }}>

                    <View style={{ height: 1, backgroundColor: Colors.lineColor }} />

                    <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: 'white' }}>
                        <View style={{ flex: 0.2 }}>
                            <Ionicons style={{ paddingLeft: 20, paddingRight: 20, }} name="ios-arrow-back" size={25} onPress={() => this._previousWeek()} />
                        </View>

                        <View style={{ flex: 0.8, alignItems: 'center' }}>
                            <TouchableOpacity style={{ flexDirection: 'row', padding: 10 }} activeOpacity={0.7} onPress={this.showWeekActionSheet}>
                                {this._renderMenuText()}
                            </TouchableOpacity>
                            <ActionSheet ref={o => this.ActionSheet = o}
                                options={menuOptions}
                                cancelButtonIndex={menuOptions.length - 1}
                                onPress={(index) => {
                                    if (index !== menuOptions.length - 1) {
                                        this._weekSelected(this.props.courseDetail.courseWeeks[index], index)
                                    }
                                }}
                            />
                        </View>

                        <View style={{ flex: 0.2 }}>
                            <Ionicons style={{ paddingLeft: 20, paddingRight: 20, }} name="ios-arrow-forward" size={25} onPress={() => this._forwardWeek()} />
                        </View>
                    </View>

                </View>
            );
        } else {
            return null;
        }
    }

    _renderActivityItem(item) {
        switch (item.activityType) {
            case Constants.ActivityType.Dictionary:
                return <ActivityDictionaryType activity={item} navigation={this.props.navigation} />
            case Constants.ActivityType.Document:
                return <ActivityDocumentType activity={item} navigation={this.props.navigation} />
            case Constants.ActivityType.Elesson:
                return <ActivityELessonType activity={item} navigation={this.props.navigation} />
            case Constants.ActivityType.LTI:
                return null;
            case Constants.ActivityType.Video:
                return <ActivityVideoType activity={item} navigation={this.props.navigation} />;
            case Constants.ActivityType.Survey:
                return <ActivitySurveyType activity={item} navigation={this.props.navigation} />;
            case Constants.ActivityType.LinkActivity:
                return <ActivityLinkType activity={item} navigation={this.props.navigation} />;
            case Constants.ActivityType.VirtualClass:
                return <ActivityVirtualClassType activity={item} navigation={this.props.navigation} />;
            case Constants.ActivityType.Assignment:
              
                return <ActivityAssignmentText activity={item} navigation={this.props.navigation} />
            case Constants.ActivityType.Exam:
                return <ActivityExamType activity={item} navigation={this.props.navigation} />
        }
    }
    _renderMoreItem = () => {
        if (this.state.scrolled && (this.props.courseDetail.activityList.length % Constants.ApiResponseQuantity === 0)) {
            console.log("Load more item")
            this.activityListApiRequestBody.skip = this.activityListApiRequestBody.skip + Constants.ApiResponseQuantity;
            this._getActivityList()
        }
    }
    async  _onRefresh() {
        console.log("_onRefresh")
        await this.props.setActivityList()
        this.setState({ refreshing: true })
        this.activityListApiRequestBody.skip = 0;
        this._getActivityList();
    }
    _onLayout() {
        const { width, height } = Dimensions.get('window');

        if (width > height) {
            this.setState({ orientation: 'landscape' });
        } else {
            this.setState({ orientation: 'portrait' });
        }
    }
    _renderActivityList() {
        if ((this.props.courseDetail.activityList === false || this.props.courseDetail.activityList.length === 0) && !this.props.courseDetail.activityListFetching) {
            return (
                <View style={{ flex: 1, height: '100%', width: '100%' }}>

                    <PageEmpty text={this.props.main.languageResource.r_courses_activity_empty || strings('r_courses_activity_empty')} />
                </View>
            )
        } else if (this.props.courseDetail.activityList.length != 0) {
            return (
                <View onLayout={this._onLayout}>
                    <FlatList
                        refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                        keyExtractor={this.keyExtractor}
                        data={this.props.courseDetail.activityList}
                        extraData={this.state.orientation}
                        onEndReached={this._renderMoreItem}
                        onEndReachedThreshold={0.5}
                        initialNumToRender={Constants.ApiResponseQuantity}
                        onMomentumScrollBegin={() => {
                            this.setState({
                                scrolled: true
                            })
                        }}
                        renderItem={({ item }) => (
                            this._renderActivityItem(item)
                        )}
                    />
                </View>
            )
        } else return null
    }
    _composeActivity() {
        this.props.navigation.navigate('NewActivitySelector')
    }
    _renderAddActivityButton() {
        if (this.props.main.userIdentity.userType == Constants.UserTypes.Instructor && this.state.weeksReady) {
            return (
                <ActionButton buttonColor={Colors.primary} offsetX={10} offsetY={70} onPress={() => this._composeActivity()} />
            )
        } else return null;
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.props.courseDetail.activityListFetching} />
                <View style={{ flex: 1, flexDirection: 'column', backgroundColor: Colors.background, width: this.state.width }} >
                    <View style={{ flex: 1 }}>
                        {this._renderActivityList()}
                    </View>
                    {this._renderPicker()}
                    {this._renderAddActivityButton()}

                </View>
            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getCourseWeeks: (...args) => dispatch(CourseDetailActions.getCourseWeeksRequest(...args)),
        setActivityList: (...args) => dispatch(CourseDetailActions.setActivityList(...args)),
        fileDownloadRequest: (...args) => dispatch(CourseDetailActions.fileDownloadRequest(...args)), //duruma göre sonra kullanılabilir
        getActivityList: (...args) => dispatch(CourseDetailActions.getActivityListRequest(...args)),
        getEnrollmentProgress: (...args) => dispatch(CoursesActions.getEnrollmentProgressRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        courseDetail: state.courseDetail,
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CourseActivities)