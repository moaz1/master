//react libraries
import React from 'react';
import { View, SafeAreaView } from 'react-native'
import { WebView } from 'react-native-webview';
import { NavigationActions } from 'react-navigation';

//component & styles
import Loader from '../../../components/Loader';
import Ionicons from 'react-native-vector-icons/Ionicons';

//settings 
import { connect } from 'react-redux';

class ActivityDetailWebView extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loader: false,
            uri: '',
        }
    }
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, }} name="ios-arrow-back" color="black" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
            title: params.activity.name
        }
    }

    componentDidMount() {

        console.log("activity url :", this.props.main.selectedOrganization.almsPlusApiUrl + this.props.navigation.getParam('activity', null)['activityDetailUrl']);
        this.setState({
            uri: this.props.main.selectedOrganization.almsPlusApiUrl + this.props.navigation.getParam('activity', null)['activityDetailUrl']
        })
    }
    loadStart() {
        this.setState({ loader: true })
    }
    loadEnd() {
        this.setState({ loader: false })
    }
    error(error) {
        console.log('error: ', error)
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.state.loader} />
                <WebView
                    onLoadStart={() => this.loadStart()}
                    onLoadEnd={() => this.loadEnd()}
                    source={{ uri: this.state.uri }}
                    onError={(error) => this.error(error)} />
            </SafeAreaView>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ActivityDetailWebView)