import React from 'react'
import { View, Text, TouchableOpacity, SafeAreaView } from 'react-native'
import AnnouncementTabRouterInstructorContainer from '../../../navigation/AnnouncementTabsRouterInstructor';
import AnnouncementTabRouterStudentContainer from '../../../navigation/AnnouncementTabsRouterStudent';

import Ionicons from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../../services/Constants';
import Loader from '../../../components/Loader';
import Colors from '../../../theme/Colors';
import ActionButton from 'react-native-action-button';

import { connect } from 'react-redux';

class AnnouncementMain extends React.Component {
    constructor(props) {
        super(props);
    }
    //TODO: eğitmen ise routing kısmının düzenlenmesi gerek.
    static router = AnnouncementTabRouterStudentContainer.router
    _renderTabBar() {
      
        return <AnnouncementTabRouterStudentContainer navigation={this.props.navigation} screenProps={{languageResource:this.props.main.languageResource}} />

        // if (this.props.main.userIdentity.userType == Constants.UserTypes.Student) {
        //     return <AnnouncementTabRouterStudentContainer navigation={this.props.navigation} />
        // } else {
        //     return <AnnouncementTabRouterInstructorContainer />
        // }
    }

    _renderAddButton() {
        if (this.props.main.userIdentity.userType == Constants.UserTypes.Instructor) {
            return (
                <ActionButton buttonColor={Colors.primary} offsetX={20} offsetY={20} onPress={() => this._composeNewAnnouncement()} />
            )
        } else return null;
    }

    _composeNewAnnouncement() {
        this.props.navigation.navigate('newAnnouncement')
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: Colors.background }}>
                {this._renderTabBar()}
                {this._renderAddButton()}
            </SafeAreaView>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(AnnouncementMain)
