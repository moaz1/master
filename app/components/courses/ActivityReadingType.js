import React from 'react';
import { View, TouchableOpacity, Text, ImageBackground, ScrollView } from 'react-native';
import { Card, Image, Button } from 'react-native-elements';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'

import TextView from '../TextView';
import HTML from 'react-native-render-html';
import Colors from '../../theme/Colors';

//KULLANILMIYOR!
export default class ActivityReadingType extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View>
                <ScrollView>
                    <Card containerStyle={{ backgroundColor: 'white', flexDirection: 'column', padding: 0, margin: 5 }}>
                        <View>
                            {/* Card Header */}
                            <View style={{ flexDirection: 'row', padding: 10 }}>
                                <Ionicons style={{ flex: 0.1 }} name="ios-book" size={25} />
                                <View style={{ flex: 0.8, flexDirection: 'column', marginStart: 5 }}>
                                    <TextView numberOfLines={2} weight="bold" style={{ color: 'black', fontSize: 18 }}>Activity Reading Type Card</TextView>
                                    <View style={{ flexDirection: 'row' }}>
                                        <TextView weight="bold" style={{ fontSize: 12 }}>Due date: </TextView>
                                        <TextView weight="regular" style={{ fontSize: 12 }}>19 August, 15:00</TextView>
                                    </View>
                                </View>
                            </View>

                            {/* Card Container */}
                            <View style={{ marginTop: 10, flexDirection: 'column' }}>
                                <View style={{ flex: 1 }}>
                                    <Image style={{ aspectRatio: 16 / 9 }} source={{ uri: 'https://images.pexels.com/photos/368893/pexels-photo-368893.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=700' }} />
                                </View>


                                <View style={{ padding: 10 }}>
                                    <TextView style={{ color: 'black', fontSize: 16 }} weight="bold" >DİYAFRAM AYARLARI VE İPUÇLARI</TextView>
                                    <TextView style={{ marginTop: 5, marginBottom: 5 }} weight="medium">Figcaption burada yer alacak. Çok uzun açıklamalar vermemeye dikkat edilmeli. 200 karakter sınırlaması verilebilir. Örneğin burada 189 karakter var. Link, bold ve italic’e izin verilebilir.</TextView>
                                    <HTML html={"Thanks for enrolling in our course Algorithms, Part I. You can review the syllabus for an overview of the course components. The course is based on a variety of material that we have prepared over many years:"} />

                                </View>

                            </View>
                            <View style={{ marginTop: 20, height: 1, backgroundColor: Colors.lineColor }} />


                            {/* Card Action Button */}
                            <View style={{ flexDirection: 'row', padding: 10 }}>
                                <View style={{ flex: 0.5 }}>
                                </View>
                                <Button icon={<Ionicons name="ios-checkmark" size={25} color="white" style={{ marginEnd: 5 }} />} title="Tamamlandı" buttonStyle={{ backgroundColor: Colors.primary }} containerStyle={{ flex: 0.5 }} />
                            </View>
                        </View>
                    </Card>
                </ScrollView>
            </View>
        );
    }
}