import React from 'react';
import { View, Dimensions, StyleSheet, SafeAreaView, FlatList } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationActions } from 'react-navigation';
import NetInfo from '@react-native-community/netinfo';

import { connect } from 'react-redux';
import CourseDetailActions from '../../../redux/CourseDetailRedux';
import CoursesActions from '../../../redux/CoursesRedux';
import Constants from '../../../services/Constants';
import Loader from '../../../components/Loader';

//activity card
import ActivityDictionaryType from '../../../components/courses/ActivityDictionaryType';
import ActivityDocumentType from '../../../components/courses/ActivityDocumentType';
import ActivityELessonType from '../../../components/courses/ActivityELessonType';
import ActivityVideoType from '../../../components/courses/ActivityVideoType';
import ActivitySurveyType from '../../../components/courses/ActivitySurveyType';
import ActivityLinkType from '../../../components/courses/ActivityLinkType';
import ActivityAssignmentText from '../../../components/courses/ActivityAssignmentText';
import ActivityVirtualClassType from '../../../components/courses/ActivityVirtualClassType';
class ActivityDetail extends React.Component {

    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('Activity')['activityName'],
            headerTitleStyle: {
                alignSelf: 'center'
            },
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, color: 'black' }} name="ios-arrow-back" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
        }
    }

    constructor(props) {
        super(props);
        this.state = {
            activity: this.props.navigation.getParam('Activity'),
            orientation: false,
        }

        this.activityDetailApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            getActivityType: Constants.ActivityListType.GetActivityBySelectedActivity,
            classId: this.props.navigation.getParam('Activity')['classId'],
            courseId: this.props.navigation.getParam('Activity')['courseId'],
            activityId: this.props.navigation.getParam('Activity')['activityId'],
            termWeekId: null,
            take: 1,
            skip: 0
        }

        this.enrollmentProgressApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            remote: true,
        }

        this._onLayout = this._onLayout.bind(this);

    }

    componentDidMount() {
        this.props.getActivityDetail(this.activityDetailApiRequestBody)

        if (this.props.courses.enrollmentProgressData.length === 0) {
            NetInfo.fetch().then(statu => {
                if (statu.isConnected)
                    this.enrollmentProgressApiRequestBody.remote = true
                else
                    this.enrollmentProgressApiRequestBody.remote = false

                this.props.getEnrollmentProgress(this.enrollmentProgressApiRequestBody)

            })
        }
    }

    componentWillReceiveProps(props) {
        if (this.props.courseDetail.activityDetail !== props.courseDetail.activityDetail) {
            this.props.setCourse({ classId: this.activityDetailApiRequestBody.classId, courseId: this.activityDetailApiRequestBody.courseId })
        }
    }

    componentWillUnmount() {
        this.props.clearActivityDetail();
        this.props.setCourse()
    }


    _onLayout() {
        console.log("onLayout")
        const { width, height } = Dimensions.get('window');

        if (width > height) {
            this.setState({ orientation: 'landscape' });
        } else {
            this.setState({ orientation: 'portrait' });
        }
    }

    renderTypes(item) {
        switch (item.activityType) {
            case Constants.ActivityType.Dictionary:
                return <ActivityDictionaryType activity={item} navigation={this.props.navigation} />
            case Constants.ActivityType.Document:
                return <ActivityDocumentType activity={item} navigation={this.props.navigation} />
            case Constants.ActivityType.Elesson:
                return <ActivityELessonType activity={item} navigation={this.props.navigation} />
            case Constants.ActivityType.LTI:
                return null;
            case Constants.ActivityType.Video:
                return <ActivityVideoType activity={item} navigation={this.props.navigation} />;
            case Constants.ActivityType.Survey:
                return <ActivitySurveyType activity={item} navigation={this.props.navigation} />;
            case Constants.ActivityType.LinkActivity:
                return <ActivityLinkType activity={item} navigation={this.props.navigation} />;
            case Constants.ActivityType.VirtualClass:
                return <ActivityVirtualClassType activity={item} navigation={this.props.navigation} />;
            case Constants.ActivityType.Assignment:

                return <ActivityAssignmentText activity={item} navigation={this.props.navigation} />
        }
    }

    keyExtractor = (item, index) => index.toString()
    renderMainContent() {
        if (!this.props.courseDetail.activityDetailFetching && this.props.courseDetail.activityDetail.length !== 0) {
            return (
                <View onLayout={this._onLayout}>
                    <FlatList
                        keyExtractor={this.keyExtractor}
                        data={this.props.courseDetail.activityDetail}
                        extraData={this.state.orientation}
                        renderItem={({ item }) => (
                            this.renderTypes(item)
                        )}
                    />
                </View>
            )
        } else return null

    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.props.courseDetail.activityDetailFetching} />
                <View>
                    {/* {this.renderMainContent()} */}
                    {this.renderMainContent()}
                </View>
            </SafeAreaView>

        )
    }

}


const mapDispatchToProps = (dispatch) => {
    return {
        getActivityDetail: (...args) => dispatch(CourseDetailActions.activityDetailRequest(...args)),
        clearActivityDetail: () => dispatch(CourseDetailActions.clearActivityDetail()),
        setCourse: (...args) => dispatch(CourseDetailActions.setCourse(...args)),
        getEnrollmentProgress: (...args) => dispatch(CoursesActions.getEnrollmentProgressRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        courseDetail: state.courseDetail,
        courses: state.courses,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActivityDetail);