import React from 'react'
import { View, Text, SafeAreaView } from 'react-native'
import MyNotesCard from '../../../components/notes/MyNotesCard'
import Colors from '../../../theme/Colors';
import AnnouncementEmpty from '../../../components/announcement/AnnouncementEmpty'
export default class NotesMain extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, backgroundColor: Colors.background }}>
                    <AnnouncementEmpty />
                </View>
            </SafeAreaView>
        );
    }
}