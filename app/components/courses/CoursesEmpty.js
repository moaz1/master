import React from 'react';
import { View, Image } from 'react-native';

//assets
import announcement_empty from '../../assets/images/announcement-empty.png';
import { strings } from '../../locales/i18n';
import TextView from '../TextView';

import { connect } from 'react-redux';
import Colors from '../../theme/Colors';
import FontSize from '../../theme/FontSize';
class CoursesEmpty extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <Image source={announcement_empty} style={{ width: '30%', height: '30%', resizeMode: "contain" }} />
                <TextView style={{ color: Colors.empty_page_text, fontSize: FontSize.page_empty_text, }} weight="bold">{this.props.main.languageResource.r_courses_empty_desctiption || strings('r_courses_empty_desctiption')}</TextView>
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(CoursesEmpty)