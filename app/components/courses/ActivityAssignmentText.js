//react libraries
import React from 'react';
import { View, TouchableOpacity, Image, PermissionsAndroid, Platform, Alert, ScrollView, TextInput, StyleSheet } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NetInfo from '@react-native-community/netinfo';
import { Card } from 'react-native-elements';
import { CheckBox } from 'react-native-elements';

//3.rd libraries
import FileViewer from 'react-native-file-viewer';
import Ionicons from 'react-native-vector-icons/Ionicons';
import HTML from 'react-native-render-html';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import PercentageCircle from 'react-native-percentage-circle';
import Moment from 'moment';
import RNFetchBlob from 'rn-fetch-blob';
import DocumentPicker from 'react-native-document-picker';


//Components and Styles
import LocalStorageConstants from '../../local/LocalStorageConstants';
import Constants from '../../services/Constants';
import ActivityInteractActions from '../../redux/ActivityInteractRedux';
import CoursesActions from '../../redux/CoursesRedux';
import TextView from '../TextView';
import DownloadAlert from '../DownloadAlert';
import Loader from '../Loader';
import Colors from '../../theme/Colors';

//settings
import { bytesToSize, createGuid } from '../../helpers/Calculate';
import { connect } from 'react-redux';
import { strings } from '../../locales/i18n';
import { template } from '../../locales/StringTemplate';
import styles from '../../theme/Style';


class ActivityAssignmentText extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            activity: props.activity,
            image: { uri: props.activity.cardImgName },
            downloadPercentage: 0,
            downloaderVisibility: false,
            canceled: false,
            onBackground: false,
            answerByTyping: true,
            answerText: "",
            selectedFile: null,
            enrollmentData: this.props.courses.enrollmentProgressData.find(data => data.activityId === props.activity.activityId),
        }

        this.setActivityAsCompletedApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            getActivityType: Constants.ActivityListType.GetActivityBySelectedActivity,
            classId: null,
            courseId: props.activity.courseId,
            activityId: props.activity.activityId,
            termWeekId: null,
            take: 1,
            skip: 0
        }
        this.enrollmentProgressApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
        }
    }


    componentWillReceiveProps(props) {

        if (this.props.activityInteract.viewCompletionCriteriaData !== props.activityInteract.viewCompletionCriteriaData
            && this.state.activity.activityId === props.activityInteract.viewCompletionCriteriaData[0].activityId) {
            this.props.getEnrollmentProgress(this.enrollmentProgressApiRequestBody)
        }
    }

    onError(error) {
        this.setState({ image: require('../../assets/images/image_not_found.png') })
    }
    _renderCardImage() {
        if (this.state.activity.cardImgName !== null) {
            return (<Image style={{ aspectRatio: 16 / 9, width: undefined, height: undefined, alignSelf: 'stretch' }} source={this.state.image} onError={this.onError.bind(this)} />)
        } else
            return null
    }

    _renderDueDate() {
        if (this.state.activity.taskDeadLine !== null) {
            var curDate = Moment();
            var isAfter = Moment(curDate).isAfter(this.state.activity.taskDeadLine);
            if (isAfter) {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={style.dueDateText}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={style.dueDateData}>
                            {this.props.main.languageResource.r_activiy_due_outdate_day !== undefined
                                ? template(this.props.main.languageResource.r_activiy_due_outdate_day, { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                : curDate.diff(this.state.activity.taskDeadLine, 'days') !== 0
                                    ? template(strings('r_activiy_due_outdate_day'), { day: curDate.diff(this.state.activity.taskDeadLine, 'days') })
                                    : this.props.main.languageResource.r_activity_due_date_today || strings('r_activity_due_date_today')
                            }
                        </TextView>
                    </View>
                );
            } else {
                return (
                    <View style={{ flexDirection: 'row' }}>
                        <TextView weight="bold" style={{ fontSize: 12 }}>
                            {this.props.main.languageResource.r_activity_due_date || strings('r_activity_due_date')}
                        </TextView>
                        <TextView weight="regular" style={{ fontSize: 12, marginStart: 5 }}>
                            {Moment(this.state.activity.taskDeadLine).format('D MMMM, hh:mm ')}
                        </TextView>
                    </View>
                );
            }
        }
        else
            return null;
    }

    _renderFileAttachtements() {
        if (this.state.activity.file.fileId !== null) {
            return (
                <View style={{ padding: 10 }}>
                    <TextView weight="bold" style={style.fileAttachmentTitle}>
                        {this.props.main.languageResource.r_activity_attached_file_title || strings('r_activity_attached_file_title')}
                    </TextView>
                    <TouchableOpacity activeOpacity={0.7} style={style.fileAttachmentContainer}
                        onPress={() => this.attachmentClicked()}>
                        <Ionicons style={{ flex: 0.05 }} name="ios-attach" size={20} />
                        <TextView style={{ flex: 0.75, marginStart: 5, color: 'black' }} weight="regular" numberOfLines={1} ellipsizeMode={'middle'}>{this.state.activity.file.fileName}</TextView>
                        <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                            <TextView style={style.fileAttachmentSize} weight="bold">{bytesToSize(this.state.activity.file.size)}</TextView>
                        </View>
                    </TouchableOpacity>
                </View>
            );
        } else return null;
    }

    _renderDescription() {
        if (this.state.activity.description != null) {
            return (
                <View style={style.activityDescription}>
                    <HTML html={this.state.activity.description} />
                </View>
            )
        } else return null
    }

    downloadAttachment() {
        this.checkDocumentIsExist().then(exist => {
            if (!exist) {
                NetInfo.fetch().then(statu => {
                    if (statu.isConnected) {
                        this.setState({
                            downloaderVisibility: true,
                            canceled: false,
                        })
                        let dirs = RNFetchBlob.fs.dirs
                        let options = {
                            fileCache: true,
                            path: dirs.DocumentDir + '/' + this.state.activity.file.fileId + this.state.activity.file.extension,
                            appendExt: this.state.activity.file.extension,
                        }

                        this.task = RNFetchBlob.config(options).fetch('GET', this.state.activity.file.filePath);
                        this.task.progress((received, total) => {
                            console.log('a progress', received / total)
                            this.setState({
                                downloadPercentage: ((received / total) * 100).toFixed(1)
                            })
                        }).then((res) => {
                            if (!this.state.canceled) {
                                if (this.state.activity.completionType === Constants.ActivityCompletionTypes.DownloadAttachment) {
                                    //TODO: Aktivite bitirme kriteri dosyayı indirme ise burada kullanılacak apiyi entegre et! (Api hazır değil!)
                                }
                                this.saveDownloadedFile(res).then(result => {
                                    this.setState({
                                        downloaderVisibility: false,
                                        onBackground: false,
                                    })
                                    if (this.state.activity.file.extension == '.pdf') {
                                        this.props.navigation.navigate('PdfViewer', { activity: this.state.activity })
                                    } else {
                                        FileViewer.open(res.path()).then().catch(error => {
                                            Alert.alert('', this.props.main.languageResource.r_activity_attach_file_open_error || strings('r_activity_attach_file_open_error'))
                                        })
                                    }
                                })
                            }
                        }).catch((err) => {
                            console.log("Error", err);
                            this.setState({
                                downloaderVisibility: false,
                                onBackground: false
                            })
                            return { 'error': true, 'code': 600, 'text': 'connection error' }
                        })
                    } else {
                        Alert.alert('', this.props.main.languageResource.r_activity_attach_file_download_connection_error || strings('r_activity_attach_file_download_connection_error'))
                    }
                })

            }
            else {
                if (this.state.activity.file.extension == '.pdf') {
                    this.props.navigation.navigate('PdfViewer', { activity: this.state.activity })
                } else {
                    this.getAlreadyDownloadedFile().then(filePath => {
                        FileViewer.open(filePath).then().catch(error => {
                            Alert.alert('', this.props.main.languageResource.r_activity_attach_file_open_error || strings('r_activity_attach_file_open_error'))
                        })
                    })

                }
            }
        })
    }

    async openFilePicker() {
        try {
            const res = await DocumentPicker.pick({
                type: Platform.OS === "android" ? Constants.AndroidMimeTypes : Constants.IOSMimeTypes,
            });
            console.log("selected res: ", res);
            this.setState({ isFilePicked: true, selectedFile: res, uploadingVisibility: true })
        } catch (error) {

        }
    }

    async getAlreadyDownloadedFile() {
        const paths = await AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths);
        if (paths !== null) {
            let downloadedData = JSON.parse(paths);
            for (var i = 0; i < downloadedData.length; i++) {
                if (this.state.activity.activityId === downloadedData[i].activityId) {
                    return downloadedData[i].downloadedFilePaths;
                }
            }
        }
    }

    async checkDocumentIsExist() {
        const paths = await AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths)
        if (paths !== null) {
            let downloadedData = JSON.parse(paths);
            var exist = false
            for (let i = 0; i < downloadedData.length; i++) {
                if (downloadedData[i].file.fileId === this.props.activity.file.fileId)
                    exist = true
            }
            return exist
        } else {
            return false
        }
    }

    async saveDownloadedFile(res) {
        var objActivity = Object.assign({}, this.state.activity);
        objActivity.downloadedFilePaths = res.path();
        await this.setState({ activity: objActivity })

        var downloadedFilePaths = await AsyncStorage.getItem(LocalStorageConstants.DownloadedFilePaths);
        if (downloadedFilePaths === null) {
            downloadedFilePaths = [];
            downloadedFilePaths.push(objActivity)
            return await AsyncStorage.setItem(LocalStorageConstants.DownloadedFilePaths, JSON.stringify(downloadedFilePaths)).then(result => {
                console.log("file path saved")
                return true;
            })
        } else {
            let downloadedFiles = JSON.parse(downloadedFilePaths);
            downloadedFiles.push(objActivity);
            return await AsyncStorage.setItem(LocalStorageConstants.DownloadedFilePaths, JSON.stringify(downloadedFiles)).then(result => {
                console.log("file path saved")
                return true;
            })
        }
    }

    attachmentClicked() {
        if (Platform.OS === 'android') {
            this.requestStoragePermission()
        } else {
            this.downloadAttachment();
        }
    }

    requestStoragePermission() {
        try {
            PermissionsAndroid.requestMultiple([PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE, PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE]).then((result) => {
                if (result['android.permission.READ_EXTERNAL_STORAGE'] && result['android.permission.WRITE_EXTERNAL_STORAGE'] === 'granted') {
                    this.downloadAttachment();
                } else {
                    console.log('Permissions denied');
                }
            })
        } catch (error) {
        }
    }
    downloadOnBackground() {
        console.log("downloadOnBackground")
        this.setState({ onBackground: true, downloaderVisibility: false })
    }
    downloadOnCancel() {
        this.setState({ downloaderVisibility: false, canceled: true, downloadPercentage: 0 })
        this.task.cancel()
    }

    _renderDownloadButtons() {
        if (this.state.onBackground) {
            return (
                <View style={style.downloadButtons}>
                    <PercentageCircle
                        radius={17}
                        borderWidth={3}
                        percent={this.state.downloadPercentage}
                        color={Colors.primary}>
                        <TextView weight="medium" style={{ fontSize: 11 }}>
                            {this.state.downloadPercentage}
                        </TextView>
                    </PercentageCircle>
                </View>
            )
        } else
            return <View style={{ flex: 0.5 }} />
    }

    setAsCompleted() {
        this.setActivityAsCompletedApiRequestBody.classId = this.props.courseDetail.course.classId;
        this.props.setAsCompleted(this.setActivityAsCompletedApiRequestBody);
    }

    replyAssignment() {

    }

    _renderCompletionIcon() {
        if (this.state.enrollmentData) {
            if (this.state.enrollmentData.status === Constants.ActivityCompletedStatus.Completed)
                return <View style={{ flex: 0.1, alignItems: 'center', justifyContent: 'center', height: styles.marked_completed_activity_icon_size, width: 30, borderRadius: 30 / 2, backgroundColor: Colors.primary }}>
                    <Ionicons name="ios-checkmark" size={styles.marked_completed_activity_icon_size} color="white" />
                </View>
        } else return null;
    }

    _renderCardHeader() {
        return (
            <View style={style.cardHeaderContainer}>
                <MaterialCommunityIcons style={{ flex: 0.1 }} name="format-text" size={25} color="black" />
                <View style={style.cardHeaderTextContainer}>
                    <TextView weight="bold" numberOfLines={2} style={style.cardHeaderText}>{this.state.activity.name}</TextView>
                    {this._renderDueDate()}
                </View>
                {this._renderCompletionIcon()}
            </View>
        )
    }

    _renderCardContainer() {
        return (
            <View style={{ marginTop: 10, flexDirection: 'column' }}>
                {this._renderCardImage()}
                {this._renderDescription()}
            </View>
        )
    }
    _renderAnswerContent() {
        if (this.state.activity.completionType === Constants.ActivityCompletionTypes.Grade || this.state.activity.completionType === Constants.ActivityCompletionTypes.Upload)
            return (
                <View style={{ padding: 10, marginTop: 10, flexDirection: 'column' }}>
                    <View style={{ height: 1, backgroundColor: Colors.lineColor }} />

                    <TextView weight="bold" style={{ color: 'black', fontSize: 18 }}>{this.props.main.languageResource.r_acitivity_assignment_answer_title || strings('r_acitivity_assignment_answer_title')}</TextView>
                    <TextView weight="light" style={{ color: 'black', fontSize: 13 }}>{this.props.main.languageResource.r_activity_assignment_upload_attemps !== undefined
                        ? template(this.props.main.languageResource.r_activity_assignment_upload_attemps, { attemp: this.state.activity.assignmentMaximumUploadCount })
                        : template(strings('r_activity_assignment_upload_attemps'), { attemp: this.state.activity.assignmentMaximumUploadCount ? this.state.activity.assignmentMaximumUploadCount : 0 })}</TextView>

                    <View style={{ flexDirection: 'row', marginTop: 5 }}>
                        <TouchableOpacity style={{ flex: 1, margin: 2, flexDirection: 'row', }}
                            onPress={() => { //buradaki değişiklikleri checkbox onpress'de de yapmalısın.
                                this.setState({ answerByTyping: true })
                            }}>
                            <CheckBox
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                onPress={() => {
                                    this.setState({ answerByTyping: true })
                                }}
                                checked={this.state.answerByTyping}
                                containerStyle={{ backgroundColor: 'transparent', padding: 0, margin: 0 }}
                                checkedColor={Colors.primary} />
                            <TextView weight="bold" style={{ color: 'black', alignSelf: 'center', textAlign: 'center' }}>
                                {this.props.main.languageResource.r_activity_answer_by_typing || strings('r_activity_answer_by_typing')}
                            </TextView>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ flex: 1, margin: 2, flexDirection: 'row', }}
                            onPress={() => { //buradaki değişiklikleri checkbox onpress'de de yapmalısın.
                                this.setState({ answerByTyping: false })
                            }}>
                            <CheckBox
                                checkedIcon='dot-circle-o'
                                uncheckedIcon='circle-o'
                                onPress={() => {
                                    this.setState({ answerByTyping: false })
                                }}
                                checked={!this.state.answerByTyping}
                                containerStyle={{ backgroundColor: 'transparent', padding: 0, margin: 0 }}
                                checkedColor={Colors.primary} />
                            <TextView weight="bold" style={{ color: 'black', alignSelf: 'center', textAlign: 'center' }}>
                                {this.props.main.languageResource.r_activity_answer_by_file_upload || strings('r_activity_answer_by_file_upload')}
                            </TextView>
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginTop: 10 }}>
                        {this.state.answerByTyping
                            ? <View style={{ flex: 1, flexDirection: 'column' }} >
                                <TextInput
                                    multiline={true}
                                    numberOfLines={Platform.OS === 'ios' ? null : 5}
                                    minHeight={Platform.OS === 'ios' ? (20 * 5) : null}
                                    placeholder={this.props.main.languageResource.r_activity_answer_by_typing_input_placeholder || strings('r_activity_answer_by_typing_input_placeholder')}
                                    style={{ fontSize: 16, borderColor: Colors.background, textAlignVertical: 'top', borderWidth: 1, marginTop: 5 }}
                                    onChangeText={(text) => this.setState({ answerText: text })}>{this.state.answerText}</TextInput>
                            </View>
                            : this._renderAnswerAttach()
                        }
                    </View>

                </View>
            )
    }
    _renderAnswerAttach() {
        return (
            <TouchableOpacity
                style={{ alignItems: 'center', backgroundColor: Colors.background, flexDirection: 'row', flex: 1, marginTop: 20, padding: 10 }}
                onPress={() =>
                    this.openFilePicker()}>
                <View style={{ flex: 0.1, alignItems: 'center' }}>
                    {this.state.onBackground
                        ? <PercentageCircle
                            radius={17}
                            bgcolor={Colors.background}

                            borderWidth={3}
                            percent={this.state.uploadingPercentage}
                            color={Colors.primary}>

                            <TextView weight="medium" style={{ fontSize: 11, color: 'black' }}>
                                {this.state.uploadingPercentage}
                            </TextView>
                        </PercentageCircle>
                        : this.state.uploadingSuccess ?
                            <Ionicons size={30} name="ios-checkmark" color="black" />
                            : <Ionicons size={30} name="ios-attach" color="black" />
                    }

                </View>
                <View style={{ flex: 0.8 }}>
                    <TextView style={{ color: 'black', marginStart: 10 }} weight="bold">
                        {this.state.selectedFile !== null
                            ? this.state.selectedFile.name + "(" + bytesToSize(this.state.selectedFile.size) + ")"
                            : this.props.main.languageResource.r_activity_answer_file_attach_btn_text || strings('r_activity_answer_file_attach_btn_text')}
                    </TextView>
                </View>
                <TouchableOpacity style={{ flex: 0.1, alignItems: 'center' }}
                    onPress={() => {
                        this.setState({ selectedFile: null, onBackground: false })
                    }} >
                    {this.state.selectedFile !== null ? <Ionicons size={20} name="ios-close" color="black" /> : null}
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }
    _renderActions() {
        if (this.state.activity.completionType === Constants.ActivityCompletionTypes.View)
            return this.state.enrollmentData && this.state.enrollmentData.status === Constants.ActivityCompletedStatus.Completed
                ? null
                :
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => { this.setAsCompleted() }}
                    style={{ flexDirection: 'row', padding: 10, flex: 0.5, justifyContent: 'flex-end', alignItems: 'center' }} >
                    <Ionicons name="md-checkmark" size={25} color="black" style={{ marginEnd: 5 }} />
                    <TextView weight="bold" style={{ color: 'black', fontSize: 16 }}>
                        {this.props.main.languageResource.r_activity_completation_button_text || strings('r_activity_completation_button_text')}
                    </TextView>
                </TouchableOpacity>
        else if (this.state.activity.completionType === Constants.ActivityCompletionTypes.Upload || this.state.activity.completionType === Constants.ActivityCompletionTypes.Grade)
            return this.state.enrollmentData && this.state.enrollmentData.status === Constants.ActivityCompletedStatus.Completed
                ? null
                :
                <TouchableOpacity
                    activeOpacity={0.7}
                    onPress={() => { this.replyAssignment() }}
                    style={{ flexDirection: 'row', padding: 10, flex: 0.5, justifyContent: 'flex-end', alignItems: 'center' }} >
                    <Ionicons name="md-checkmark" size={25} color="black" style={{ marginEnd: 5 }} />
                    <TextView weight="bold" style={{ color: 'black', fontSize: 16 }}>
                        {this.props.main.languageResource.r_activity_completation_answer_button_text || strings('r_activity_completation_answer_button_text')}
                    </TextView>
                </TouchableOpacity>

        else return null;
    }
    _renderActionButtons() {
        return (
            <View style={{ flexDirection: 'row', alignContent: 'center' }}>
                {this._renderDownloadButtons()}
                {this._renderActions()}
            </View>
        )
    }
    render() {
        return (
            <View>
                <Loader loading={this.props.activityInteract.viewCompletionCriteriaFetching || this.props.courses.enrollmentProgressFetching} />
                <ScrollView>
                    <DownloadAlert
                        downloading={this.state.downloaderVisibility}
                        percent={this.state.downloadPercentage}
                        onBackground={() => this.downloadOnBackground()}
                        onCancel={() => this.downloadOnCancel()} />
                    <Card containerStyle={{ backgroundColor: 'white', padding: 0, flexDirection: 'column', margin: 5 }}>
                        <View>
                            {/* Card Header */}
                            {this._renderCardHeader()}

                            {/* Card Container */}
                            {this._renderCardContainer()}

                            {/* Card Attachments */}
                            {this._renderFileAttachtements()}

                            {this._renderAnswerContent()}

                            <View style={{ marginTop: 20, height: 1, backgroundColor: Colors.lineColor }} />

                            {/* Card Action Button */}
                            {this._renderActionButtons()}
                        </View>
                    </Card>
                </ScrollView>
            </View>
        );
    }
}

//STYLE
const style = StyleSheet.create({
    dueDateText: {
        fontSize: 13,
        color: Colors.activity_due_date_past
    },
    dueDateData: {
        fontSize: 12,
        marginStart: 5,
        color: Colors.activity_due_date_past
    },
    fileAttachmentTitle: {
        fontSize: 16,
        color: 'black',
        padding: 10
    },
    fileAttachmentContainer: {
        borderRadius: 5,
        borderWidth: 0.3,
        flex: 1,
        flexDirection: 'row',
        paddingTop: 5,
        paddingBottom: 5,
        paddingStart: 10,
        paddingEnd: 10,
        alignItems: 'center'
    },
    fileAttachmentSize: {
        fontSize: 12,
        color: 'black'
    },
    activityDescription: {
        padding: 10
    },
    downloadButtons: {
        flex: 0.5,
        justifyContent: 'center',
        marginStart: 5
    },
    cardHeaderContainer: {
        flexDirection: 'row',
        padding: 10
    },
    cardHeaderTextContainer: {
        flex: 0.9,
        flexDirection: 'column',
        marginStart: 5
    },
    cardHeaderText: {
        color: 'black',
        fontSize: 18
    }
})

const mapDispatchToProps = (dispatch) => {
    return {
        setAsCompleted: (...args) => dispatch(ActivityInteractActions.activityCompletionViewCriteriaRequest(...args)),
        getEnrollmentProgress: (...args) => dispatch(CoursesActions.getEnrollmentProgressRequest(...args)),

    }
}

const mapStateToProps = (state) => {
    return {
        main: state.main,
        courseDetail: state.courseDetail,
        activityInteract: state.activityInteract,
        courses: state.courses
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ActivityAssignmentText)