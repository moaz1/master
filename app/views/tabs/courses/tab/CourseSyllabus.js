import React from 'react';
import { View, Text } from 'react-native';
//redux
import { connect } from 'react-redux';

import PageEmpty from '../../../../components/courses/PageEmpty'

class CourseSyllabus extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <PageEmpty text={"Course Syllabus is empty"} />
            </View>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
    }
}

const mapStateToProps = (state) => {
    return {
        courseDetail: state.courseDetail
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CourseSyllabus)
