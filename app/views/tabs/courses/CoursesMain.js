import React from 'react'
import { View, ScrollView, RefreshControl, SectionList, TouchableOpacity, SafeAreaView } from 'react-native'

import CourseItem from '../../../components/courses/CourseItem';
import DailySchedule from '../../../components/schedule/DailySchedule';
import AsyncStorage from '@react-native-community/async-storage';


//redux
import { connect } from 'react-redux';
import CoursesActions from '../../../redux/CoursesRedux';
import LoginActions from '../../../redux/LoginRedux';
import ActivityInteractActions from '../../../redux/ActivityInteractRedux';
import ScheduleActions from '../../../redux/ScheduleRedux';
import MainActions from '../../../redux/MainRedux';

import Colors from '../../../theme/Colors';
import Loader from '../../../components/Loader';

import { ErrorAlert } from '../../../components/ErrorAlert';
import NetInfo from '@react-native-community/netinfo';

import Moment from 'moment';
import TextView from '../../../components/TextView';
import Constants from '../../../services/Constants';
import CoursesEmpty from '../../../components/courses/CoursesEmpty';
import { strings } from '../../../locales/i18n';
import LocalStorageConstants from '../../../local/LocalStorageConstants';

class CoursesMain extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
            scrolled: false,
            listEmpty: false
        }
        this._isMounted = false;
        this._isStateFull = false;
        this._isStateFullDaily = false;

        this.enrolledCourseApiRequestBody = {
            remote: true,
            userIdentity: this.props.main.userIdentity,
            accessToken: this.props.main.authData.access_token,
            //accessToken: 'eyJhbGciOiJSUzI1NiIsImtpZCI6IkI0Mzg4RUM1QUFBQUY3MkJDMzFEMTRBNjFBNkJFNEVDRTQwREZFQTEiLCJ0eXAiOiJKV1QiLCJ4NXQiOiJ0RGlPeGFxcTl5dkRIUlNtR212azdPUU5fcUUifQ.eyJuYmYiOjE1NzAwODI4MzIsImV4cCI6MTU3MDExODgzMiwiaXNzIjoiaHR0cHM6Ly9hbG1zcC1hdXRoLmFsbXNjbG91ZC5jb20vIiwiYXVkIjpbImh0dHBzOi8vYWxtc3AtYXV0aC5hbG1zY2xvdWQuY29tL3Jlc291cmNlcyIsImFwaSJdLCJjbGllbnRfaWQiOiJhcGkiLCJzdWIiOiI1MTk1ODk6MTc5IiwiYXV0aF90aW1lIjoxNTcwMDgyODMyLCJpZHAiOiJsb2NhbCIsImlkIjoiNTE5NTg5IiwibmFtZSI6IkF2bmkiLCJnaXZlbl9uYW1lIjoiYXlhbGNpbi5vZ3JldG1lbiIsImZhbWlseV9uYW1lIjoiWWFsw6fEsW4iLCJlbWFpbCI6ImZha2VfYXlhbGNpbi5vZ3JldG1lbkBhZHYuY29tLnRyIiwicm9sZSI6IjQiLCJwZXJtaXNzaW9uX2NvbnRlbnQiOiI1MTk1ODk6NDo3M0NBOUM1NDE5MjY4RjNFRTU1ODJENjg4MjExNzcxMjoxNjowN0I1MDIxRUM0QjQxMTBERkY2RjZBNzIzNDZDRjZBRSw1MTk1ODk6NDoyMzcyNjRGMTVCMkQ3QUFENTVCQkE0QTU3Mzc2REY3QToxNjoxNkFGRjkxRjY5N0Q5QjdENTU5NDVDN0MyRTk0OTdCQSw1MTk1ODk6NDpDQTVBNTUzOTUxREM2ODhEQjE5MjVGQTg4REY1MTAyRDoxNjo4MDg3RjhFMTMwRDA4QkMzRTE4NDU2MjZBM0ZFODhCQSw1MTk1ODk6NDoyNTk0OEY4QzJCQTlFNUY0NjNERDI2MDgyNTJFRTU1NjoxNjo2NDU2ODMxNUM4QkY4MDdEQzM2RkRGMkMwQTlGNTI0QSw1MTk1ODk6NDpDQkM2QzIxRDYxN0M5NkU3MjQwOUU2NEQyOTRBOTMwNDoxNjo0QkU2ODlGQjBBMDYyMDM5RjBEQUU1RTM0ODAxQzNCNyw1MTk1ODk6NDpBMUE3RTVCMDFCQjYwMDg2RjlDQUNFMERCMkQwNzY4NDoxNjo0Mzk2MTkzMjEzODBFQTlEQTMyRjJCMkYzRkUxMkFBOSw1MTk1ODk6NDpGRkZBQzY5N0UzNzQ4M0U2OEJGODI3ODI4RjYzMDlBODoxNjo0OEZGRTMzNUMzQ0Q2MjU2N0REMzZEQURGQjU3QzhEOCw1MTk1ODk6NDo1REJBRkU5QTkzNzYwRjM2MUJFNzZBODAyRjExMjVGQToxNjoyMDg3M0Y0NTBDQTQwNjFCNjI3OUJGMDM1NTk4MjYyNiw1MTk1ODk6NDowMDFENThGMTczNUQxQUE1MDdGMzdDODlBQzMzNDYzNjoxNjpDQjFEQUUzRjAzNUI0RDE4M0QwQkIwNjA2NjAyN0M5RCw1MTk1ODk6NDpBQ0I5MDk4OTAzNkRDMUMxNDhGREJFREI5ODI0OTA1MToxNjo3MzVCMkU4MEZEQjE3M0M1NzY2M0Q0MzMxM0ExRkZDMiIsImN1c3RvbV9wZXJtaXNzaW9uX2NvbnRlbnQiOiIiLCJvcmdhbml6YXRpb25faWQiOiIxNzkiLCJzY29wZSI6WyJhcGkiLCJvZmZsaW5lX2FjY2VzcyJdLCJhbXIiOlsiY3VzdG9tIl19.WJide_HmkG4w0fGpc89XjBDdQ6DRv-MIYuY9aYJB9qZu3AG7jKCXWcDBP7Cai0hxmW0lV96UsHKhKZKefQgsH1G22Cq_lHegbcBklkwIr3WllZvOhQjw39jrES7YzMRAzIH7CkFu0HP65Ig7mtqJ5AaTrkU5B6zJxkU13rdypoAyRqiIfTHNJVjI9tYUc5FYdfKgBP-7CLn1lYM00p_FxzdhjhwN--juqF1jwvO-eCwarMeb1cxHgzBOraDOFlf6p-ja1XU0TyvLctKm-o17bNzHRO9JsMop01QnD1OAsHPOrxSJ2gZ8wu_FMXgI1kMqv8TVAyQXwkPCA3lmkOv4kA',
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            activeStatus: Constants.CourseTypes.Active,
            courseDateFilter: 1,
            isNotifications: true,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }


        this.dailyCalendarApiRequestBody = {
            remote: true,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            startDate: Moment().format('YYYY-MM-DD'),
            endDate: Moment().add(1, 'd').format('YYYY-MM-DD'),
            contextType: 16,
            take: Constants.DailyCalendarQuantity,
            skip: 0
        }
        this.refreshTokenApiBody = {
            almsPlusAuthUrl: this.props.main.selectedOrganization.almsPlusAuthUrl,
            userName: '',
            password: '',
            apiAddress: this.props.main.selectedOrganization.apiAddress,
            refreshToken: null,
        }
    }

    async componentDidMount() {
        this._isMounted = true;
        if (this.props.courses.enrolledCoursesData.length != 0) {
            this._isStateFull = true;
        }
        if (this.props.schedule.dailyCalender.length != 0) {
            this._isStateFullDaily = true;
        }
        this._apiCalls();
        this.saveUnSavedVideoTrackingDatas()
    }

    //kaydedilmemiş video tracking dataları internet varsa kaydedilir.
    async saveUnSavedVideoTrackingDatas() {
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) {
                this.props.saveUnsavedVideoTrackingData()
            }
        })
    }

    componentWillUnmount() {
        this._isMounted = false;
        this._isStateFull = false;
        this._isStateFullDaily = false;
        this.state = {
            refreshing: false,
        }
    }

    async componentWillReceiveProps(props) {
        if (this.props.courses.errorMessage !== props.courses.errorMessage && props.courses.error) {
            if (props.courses.errorMessage.code === 401) {
                const authData = await AsyncStorage.getItem('auth')
                const userName = await AsyncStorage.getItem('loggedUserName')
                const userPsw = await AsyncStorage.getItem('loggedUserPsw')
                this.refreshTokenApiBody.userName = userName;
                this.refreshTokenApiBody.password = userPsw;

                this.refreshTokenApiBody.refreshToken = JSON.parse(authData).refresh_token
                console.log("refreshTokenApiBody: ", this.refreshTokenApiBody);
                this.props.refreshToken(this.refreshTokenApiBody)
            } else {
                ErrorAlert(props.courses.errorMessage, this.props.navigation)
            }
        }
        if (!this.props.courses.fetching && !this.props.schedule.dailyCalenderFetching) {
            this.setState({ refreshing: false })
        }

        if (props.courses.enrolledCoursesData.length === 0) {
            this.setState({ listEmpty: true })
        } else {
            this.setState({ listEmpty: false })
        }
        if (this.props.login.tokenData !== props.login.tokenData) {
            console.log("new access token generated")
            this.props.setAuthData(props.login.tokenData);
            this._apiCallsWithNewToken(props.login.tokenData);
        }
    }

    keyExtractor = (item, index) => index.toString()

    _onItemClick(course) {
        this.props.navigation.navigate('courseDetail', { Course: course })
    }

    _onDailyItemClick(activity) {
        this.props.navigation.navigate('activityDetail', { Activity: activity })
    }

    _apiCalls() {
        NetInfo.fetch().then(statu => {
            if (!statu.isConnected) {
                this.dailyCalendarApiRequestBody.remote = false;
                this.enrolledCourseApiRequestBody.remote = false;
            } else {
                this.dailyCalendarApiRequestBody.remote = true;
                this.enrolledCourseApiRequestBody.remote = true;
            }

            if (this.props.courses.fetching || this.state.refreshing)
                this.props.getEnrolledCourses(this.enrolledCourseApiRequestBody)

            if (this.props.schedule.dailyCalenderFetching || this.state.refreshing)
                this.props.getMyDailyCalender(this.dailyCalendarApiRequestBody);
        });
    }

    _apiCallsWithNewToken(tokenData) {
        NetInfo.fetch().then(statu => {
            if (!statu.isConnected) {
                this.dailyCalendarApiRequestBody.remote = false;
                this.enrolledCourseApiRequestBody.remote = false;
            } else {
                this.dailyCalendarApiRequestBody.remote = true;
                this.enrolledCourseApiRequestBody.remote = true;
            }

            this.enrolledCourseApiRequestBody.accessToken = tokenData.access_token
            this.props.getEnrolledCourses(this.enrolledCourseApiRequestBody)

            this.dailyCalendarApiRequestBody.accessToken = tokenData.access_token
            this.props.getMyDailyCalender(this.dailyCalendarApiRequestBody);
        });
    }

    _callEnrolledCoursesApi() {
        this.setState({ scrolled: false })
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.enrolledCourseApiRequestBody.remote = true; else this.enrolledCourseApiRequestBody.remote = false;
            this.props.getEnrolledCourses(this.enrolledCourseApiRequestBody)
        })
    }

    _onRefresh() {
        this.props.setCoursesList()
        this.setState({ refreshing: true, scrolled: false })
        this.enrolledCourseApiRequestBody.skip = 0
        this._apiCalls();
    }

    _renderSectionHeader(section) {
        return (
            <View style={{ backgroundColor: 'white', paddingTop: 10, paddingBottom: 10 }}>
                <TextView weight="bold" style={{ color: 'black', fontSize: 18 }}> {section.title}</TextView>
            </View>
        )
    }
    _renderMoreItem = () => {
        if (this.state.scrolled && !this.state.listEmpty && (this.props.courses.enrolledCoursesData.length % Constants.ApiResponseQuantity === 0)) {
            console.log("Load more item")
            this.enrolledCourseApiRequestBody.skip = this.enrolledCourseApiRequestBody.skip + Constants.ApiResponseQuantity
            this._callEnrolledCoursesApi()
        }
    }
    _renderData() {
        const renderEnrolledCourses = ({ item, index, section: { title, data } }) =>
            <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column' }} onPress={() => this._onItemClick(item)}>
                <CourseItem course={item} />
            </TouchableOpacity>
        const renderDailyCalender = ({ item, index, section: { title, data } }) =>
            <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column', backgroundColor: 'white', padding: 5 }} onPress={() => this._onDailyItemClick(item)}>
                <DailySchedule schedule={item} />
            </TouchableOpacity>


        if (this.props.courses.enrolledCoursesData.length == 0 && !this.props.courses.fetching) {
            return (
                <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}>
                    <CoursesEmpty />
                </ScrollView>
            );
        } else if (this.props.courses.enrolledCoursesData.length != 0 && !this.props.courses.enrolledCoursesData.error
            && !this.props.schedule.dailyCalenderFetching && !this.props.schedule.dailyCalenderError) {
            return (
                <SectionList
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    renderSectionHeader={({ section }) => (
                        section.data.length > 0 && section.title !== '' ? this._renderSectionHeader(section) : (null)
                    )}
                    sections={[
                        { type: 'daily', title: this.props.main.languageResource.r_course_daily_activity || strings('r_course_daily_activity'), data: this.props.schedule.dailyCalender, renderItem: renderDailyCalender },
                        { type: 'courses', title: '', data: this.props.courses.enrolledCoursesData, renderItem: renderEnrolledCourses },
                    ]}
                    keyExtractor={(item, index) => item + index
                    }
                    onEndReached={this._renderMoreItem}
                    onEndReachedThreshold={0.5}
                    initialNumToRender={Constants.ApiResponseQuantity}
                    onTouchStart={() => {
                        this.setState({
                            scrolled: true
                        })
                    }}

                />

            );
        } else {
            return null
        }


    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.props.courses.fetching && !this.state.refreshing} />
                <View style={{ flex: 1 }}>

                    <ScrollView contentContainerStyle={{ flex: 1, backgroundColor: Colors.background }}>
                        <View style={{ flex: 1 }}>
                            {this._renderData()}
                        </View>
                    </ScrollView>

                </View>
            </SafeAreaView >
        );
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        getEnrolledCourses: (...args) => dispatch(CoursesActions.getCoursesRequest(...args)),
        getMyDailyCalender: (...args) => dispatch(ScheduleActions.getMyDailyCalenderRequest(...args)),
        setCoursesList: (...args) => dispatch(CoursesActions.setCoursesList(...args)),
        saveUnsavedVideoTrackingData: () => dispatch(ActivityInteractActions.saveUnsavedVideoTrackingData()),
        refreshToken: (...args) => dispatch(LoginActions.refreshTokenRequest(...args)),
        setAuthData: (...args) => dispatch(MainActions.setAuthData(...args)),

    }
}

const mapStateToProps = (state) => {
    return {
        courses: state.courses,
        schedule: state.schedule,
        main: state.main,
        activityInteract: state.activityInteract,
        login: state.login,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CoursesMain)