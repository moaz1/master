import React from 'react';
import { View, ImageBackground, Image, StatusBar, TouchableOpacity, SafeAreaView, StyleSheet, Linking, Platform } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';


import { Card, Input, Button } from 'react-native-elements'
import Icon from 'react-native-vector-icons/AntDesign'

import { strings } from '../../locales/i18n'
//assets
import login_bg from '../../assets/images/login-bg.jpg';

//redux
import { connect } from 'react-redux';
import LocalStorageConstants from '../../local/LocalStorageConstants';
import { calculateLoginPageWidthPercent } from '../../helpers/Calculate';
import TextView from '../../components/TextView';
import Colors from '../../theme/Colors';
import FontSize from '../../theme/FontSize';
import { template } from '../../locales/StringTemplate';
import Constants from '../../services/Constants';

class SignIn extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            userName: '',
            organizationImage: { uri: this.props.main.selectedOrganization.logoUrl },
            imageError: false,
            nextButtonEnabled: true,
            widthPercent: calculateLoginPageWidthPercent()
        }
    }

    async componentDidMount() {
        const savedUserName = await AsyncStorage.getItem(LocalStorageConstants.SavedUserName);

        if (savedUserName !== null) {
            this.setState({ userName: savedUserName, nextButtonEnabled: false })
        }

        //setTimeout(() => this.inputRef.focus(), 500);

        this.inputRef.focus();
    }

    navigateToSignInPasword() {
        const { navigate } = this.props.navigation;
        AsyncStorage.setItem(LocalStorageConstants.SavedUserName, this.state.userName);
        navigate('SignInPassword', { userName: this.state.userName });
    }

    inputChange(text) {
        this.setState({ userName: text });

        if (text.length === 0) {
            this.setState({ nextButtonEnabled: true })
        } else {
            this.setState({ nextButtonEnabled: false })
        }
    }

    onError(error) {
        // this.setState({ organizationImage: require('../../assets/images/image_not_found.png') })
        this.setState({ imageError: true })
    }

    redirectToPrivacyPolicy() {
        Linking.openURL(this.props.main.selectedOrganization.almsPlusApiUrl + Constants.PrivacyPolicy)
    }
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={{ flex: 1 }}>
                <ImageBackground source={login_bg} style={{ width: '100%', height: '100%', position: 'absolute' }} />
                <SafeAreaView style={styles.mainContainer}>
                    {Platform.OS === "android" ? <StatusBar translucent backgroundColor="transparent" /> : <StatusBar hidden={true} />}

                    <View style={{ flex: 1, flexDirection: 'column', width: this.state.widthPercent, alignSelf: 'center' }}>
                        <Card containerStyle={{ padding: 20, backgroundColor: '#FFFFFF', margin: 20, borderRadius: 5 }}>
                            <View style={{ flexDirection: 'row', justifyContent: "center", marginBottom: 10 }}>
                                {this.state.imageError ? null : <Image source={this.state.organizationImage} onError={this.onError.bind(this)} style={{ width: '100%', height: 100, resizeMode: "contain" }} />}

                            </View>

                            <TextView style={{ fontSize: FontSize.login_header, color: Colors.login_card_text }} weight="bold">
                                {this.props.main.languageResource.r_login_welcome || strings('r_login_welcome')}
                            </TextView>
                            <TextView style={{ fontSize: 15, color: "#000", marginTop: 5 }} weight="regular">
                                {this.props.main.languageResource.r_login_greetings !== undefined
                                    ? template(this.props.main.languageResource.r_login_greetings, { organizationName: this.props.main.selectedOrganization.name })
                                    : template(strings('r_login_greetings'), { organizationName: this.props.main.selectedOrganization.name })}
                            </TextView>

                            <Input
                                ref={ref => (this.inputRef = ref)}
                                autoFocus={true}
                                placeholder={this.props.main.languageResource.login_user_name_input || strings('login_user_name_input')}
                                containerStyle={{ marginTop: 10 }}
                                onChangeText={(text) => this.inputChange(text)}>{this.state.userName}</Input>

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20 }}>
                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => navigate('OrganizationSelection')} >
                                    <Icon name="arrowleft" size={20} color="black" />
                                    <TextView style={{ paddingStart: 10 }} >{this.props.main.languageResource.r_login_back || strings('r_login_back')}</TextView>
                                </TouchableOpacity>

                                <Button
                                    disabled={this.state.nextButtonEnabled}
                                    buttonStyle={{ backgroundColor: Colors.primary }}
                                    title={this.props.main.languageResource.r_login_next || strings('r_login_next')}
                                    onPress={() => this.navigateToSignInPasword()}
                                    icon={<Icon name="arrowright" size={20} color="white" />}
                                    iconRight
                                    titleStyle={{ marginEnd: 8 }} />
                            </View>

                            <View style={{ marginTop: 20, justifyContent: 'flex-end', flexDirection: 'row' }}>
                                <TouchableOpacity onPress={() => {
                                    Linking.openURL(this.props.main.selectedOrganization.almsPlusApiUrl + Constants.ForgottenPassword).catch(err => console.log("link error:", err))
                                }}>
                                    <TextView >{this.props.main.languageResource.r_login_forgot_password || strings('r_login_forgot_password')}</TextView>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                                <TextView style={{ fontSize: 12 }}>2013-{new Date().getFullYear()} {this.props.main.languageResource.r_login_alms || strings('r_login_alms')}</TextView>
                            </View>
                        </Card>
                        <TouchableOpacity style={styles.bottomView} onPress={() => { this.redirectToPrivacyPolicy() }}>
                            <TextView weight="bold" style={{ color: 'black' }}>{this.props.main.languageResource.r_app_privacy_policy || strings('r_app_privacy_policy')}</TextView>
                        </TouchableOpacity>

                    </View>
                </SafeAreaView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,

    },
    bottomView: {
        position: 'absolute',
        bottom: 0,
        padding: 5,
        marginBottom: 5,
        borderRadius: 5,
        alignSelf: 'center',
        backgroundColor: 'white'
    },
});

const mapDispatchToProps = (dispatch) => {
    return {
    }
}
const mapStateToProps = (state) => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)