import React from 'react';
import { View, Text, TouchableOpacity, FlatList } from 'react-native';
import { Card, Divider } from 'react-native-elements';
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { strings } from '../../locales/i18n';
import Colors from '../../theme/Colors';

export default class MyNotesCard extends React.Component {
    constructor(props) {
        super(props)
    }
    keyExtractor = (item, index) => index.toString()

    getIconName(type) {
        if (type === 'note')
            return "ios-list-box"
        else if (type === 'code')
            return "ios-code-working"
        else
            return "ios-play-circle"
    }

    render() {
        //test data
        const notes = [
            {
                lessonType: 'note',
                lessonName: 'Diferansiyel Denklemler 101',
                lessonDate: '03.04.2019',
                noteCounter: 10
            },
            {
                lessonType: 'video',
                lessonName: 'Diferansiyel Denklemler 101',
                lessonDate: '03.04.2019',
                noteCounter: 50
            },
            {
                lessonType: 'code',
                lessonName: 'Diferansiyel Denklemler 101',
                lessonDate: '03.04.2019',
                noteCounter: 50
            }
        ]

        return (
            <Card containerStyle={{ margin: 5, padding: 10 }}>
                {/* Card Header */}
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ fontSize: 20, color: "#000", fontFamily: "Roboto-Bold" }}>{strings('notes.notes_title')}</Text>
                        <Text style={{ backgroundColor: Colors.card_badge, color: 'black', borderRadius: 25, marginStart: 5, paddingStart: 5, paddingEnd: 5, paddingTop: 2, paddingBottom: 2 }}>{notes.length}</Text>
                    </View>
                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={{ paddingEnd: 10, fontFamily: "Roboto-Bold" }} >{strings('common.all')}</Text>
                        <Icon name="arrowright" size={20} color="black" />
                    </TouchableOpacity>
                </View>
                <Divider style={{ backgroundColor: 'black', marginTop: 5, marginBottom: 5 }} />

                {/* Card Container */}
                <FlatList keyExtractor={this.keyExtractor} data={notes} renderItem={({ item }) => (
                    <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
                        {/* Note Icon */}
                        <View style={{ flex: 0.1 }}>
                            {
                                <Ionicons name={this.getIconName(item.lessonType)} size={20} color="black" />
                            }
                        </View>

                        {/* Note Content */}
                        <View style={{ flex: 0.7, flexDirection: 'column', margin: 5 }}>
                            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 17, color: 'black' }}>{item.lessonName}</Text>
                            <Text style={{ fontFamily: 'Roboto-Medium', fontSize: 12 }}>{item.lessonDate}</Text>
                        </View>

                        {/* Note Counter */}
                        <View style={{ flex: 0.2, alignItems: 'flex-end' }}>
                            <Text style={{ fontSize: 12, backgroundColor: Colors.primary, color: 'white', borderRadius: 25, marginStart: 5, paddingStart: 5, paddingEnd: 5, paddingTop: 2, paddingBottom: 2 }}>60/100</Text>

                        </View>
                    </TouchableOpacity>
                )} />

            </Card>
        );
    }
}