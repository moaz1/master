import React from 'react';
import { View, Text, ImageBackground, StatusBar, TouchableOpacity, FlatList, SafeAreaView, Alert, Platform, StyleSheet } from 'react-native';
import { Input, ThemeProvider, ButtonGroup } from 'react-native-elements';
import NetInfo from '@react-native-community/netinfo';

//assets
import login_bg from '../assets/images/login-bg.jpg';
import AsyncStorage from '@react-native-community/async-storage';
import AppTheme from '../theme/AppTheme';
//components
import Loader from '../components/Loader';
import OrganizationItem from '../components/organization/OrganizationItem';

//redux
import OrganizationActions from '../redux/OrganizationRedux';
import MainActions from '../redux/MainRedux';
import { connect } from 'react-redux';

import I18n from 'react-native-i18n';

import LocalStorageConstants from '../local/LocalStorageConstants';
import Colors from '../theme/Colors';
import { calculateLoginPageWidthPercent } from '../helpers/Calculate';
import Constants from '../services/Constants';
import TextView from '../components/TextView';
import FontSize from '../theme/FontSize';

import { strings } from '../locales/i18n';


class OrganizationSelection extends React.Component {

    onLayout(e) {
        this.setState({
            widthPercent: calculateLoginPageWidthPercent()
        })
    }


    constructor(props) {
        super(props);
        this.state = {
            query: '',
            selectedOrganization: null,
            languageIndex: null,
            languageChanged: false,
            widthPercent: calculateLoginPageWidthPercent(),
        };
        this._languageChange = this._languageChange.bind(this)
    }

    componentWillMount() {
        const currentLocale = I18n.currentLocale();
        if (currentLocale.indexOf('tr') === 0) {
            this.setState({ languageIndex: 0 })
        } else {
            this.setState({ languageIndex: 1 })
        }
    }

    componentDidMount() {
        this.props.getAllOrganizations();
    }

    componentWillReceiveProps(props) {
        if (props.organizations.error) {
            Alert.alert(strings('login_instruction_api_error'), props.organizations.errorMessage, [
                { text: strings('ok') },
            ])
        }

        if (!props.main.languageResourceFetching && !props.main.languageResourceError && props.main.languageResource !== null && this.state.selectedOrganization !== null) {
            this.props.setLanguageResources(props.main.languageResource)
            this.props.navigation.navigate('SignIn')
        }
    }

    findOrganizations = (query) => {
        if (query === '') {
            return [];
        }

        var patt1 = /\W/g;
        var result = query.replace(patt1, " ");
        return this.props.organizations.organizationsData.filter(item => item.name.toLowerCase().search(result.toLowerCase()) >= 0);
    }

    inputChange(text) {
        this.setState({ query: text })
    }

    async organizationSelected(item) {
        await this.setState({ selectedOrganization: item })
        await AsyncStorage.setItem('selectedOrganization', JSON.stringify(item))
        this.props.setSelectedOrganization(item)


        NetInfo.fetch().then(statu => {
            var remote = false;
            if (statu.isConnected) {
                remote = true;
            }

            if (this.state.languageIndex === 0) { //0 index: turkish language resource
                const data = {
                    remote: remote,
                    version: LocalStorageConstants.LanguageVersion,
                    resourceType: LocalStorageConstants.LanguageTrResource,
                    almsPlusApiUrl: this.state.selectedOrganization.almsPlusApiUrl
                }
                this.props.getLanguageResourceRequest(data)
                AsyncStorage.setItem(LocalStorageConstants.Language, LocalStorageConstants.LanguageTrResource);
            } else { //1 index: english language resource
                const data = {
                    remote: remote,
                    version: LocalStorageConstants.LanguageVersion,
                    resourceType: LocalStorageConstants.LanguageEnResource,
                    almsPlusApiUrl: this.state.selectedOrganization.almsPlusApiUrl
                }
                this.props.getLanguageResourceRequest(data)
                AsyncStorage.setItem(LocalStorageConstants.Language, LocalStorageConstants.LanguageEnResource);
            }
        })
    }

    _languageChange(selectedIndex) {
        if (selectedIndex === 0) {
            I18n.locale = 'tr'
        } else {
            I18n.locale = 'en'
        }
        this.setState({
            languageIndex: selectedIndex,
            languageChanged: true,
        })

    }


    _keyExtractor = (item, index) => item.organizationId;

    render() {
        const { query } = this.state;
        const organizations = this.findOrganizations(query);

        return (
            <View style={{ flex: 1 }}>
                <ImageBackground source={login_bg} style={mStyles.bacgroundImageStyle} />
                <SafeAreaView style={mStyles.mainContainer}>
                    {Platform.OS === "android" ? <StatusBar translucent backgroundColor="transparent" /> : <StatusBar hidden={true} />}

                    <Loader loading={this.props.organizations.fetching || this.props.main.languageResourceFetching} />
                    <View onLayout={this.onLayout.bind(this)}
                        style={{ flex: 1, flexDirection: 'column', width: this.state.widthPercent, alignSelf: 'center' }}>
                        <View style={mStyles.inputContainer}>

                            <View>
                                <TextView style={mStyles.inputCompany} weight="medium">{strings('login_select_instruction')}</TextView>
                                <Input autoFocus={true} placeholder={strings('login_instruction_input')} shake={true} containerStyle={{ marginTop: 10 }} onChangeText={(text) => this.inputChange(text)}>{this.state.query}</Input>
                            </View>

                            <FlatList keyboardShouldPersistTaps={"always"} data={organizations} keyExtractor={this._keyExtractor} renderItem={({ item }) => (
                                <TouchableOpacity onPress={() => this.organizationSelected(item)}>
                                    <OrganizationItem organization={item} />
                                </TouchableOpacity>
                            )} />

                            <View style={mStyles.almsInfo}>
                                <TextView style={{ fontSize: FontSize.login_alms }}>2013-{new Date().getFullYear()} {this.props.main.languageResource.r_login_alms || strings('r_login_alms')}</TextView>
                            </View>
                        </View>

                        {/* Language Selector */}
                        <View style={{ flex: 1, alignItems: 'center' }}>
                            <View style={{ alignItems: 'center', borderRadius: 5 }}>
                                <ButtonGroup
                                    buttons={[Constants.SupportedLanguage.TR, Constants.SupportedLanguage.EN]}
                                    onPress={this._languageChange}
                                    selectedIndex={this.state.languageIndex} containerStyle={{ width: 100 }}
                                    selectedButtonStyle={{ backgroundColor: Colors.primary }} />
                            </View>
                        </View>
                    </View>
                </SafeAreaView>
            </View>
        );
    }
}

const mStyles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0
    },
    bacgroundImageStyle: {
        width: '100%',
        height: '100%',
        position: 'absolute'
    },
    inputContainer: {
        padding: 20,
        backgroundColor: Colors.login_card_background,
        margin: 20,
        borderRadius: 5
    },
    inputCompany: {
        fontSize: FontSize.login_header,
        color: Colors.login_card_text,
        marginTop: 5
    },
    almsInfo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 30
    }
});

const mapDispatchToProps = (dispatch) => {
    return {
        getAllOrganizations: () => dispatch(OrganizationActions.getOrganizationRequest()),
        setLanguageResources: (...args) => dispatch(MainActions.setLanguageResources(...args)),
        setSelectedOrganization: (...args) => dispatch(MainActions.setSelectedOrganization(...args)),
        getLanguageResourceRequest: (...args) => dispatch(MainActions.getLanguageResourceRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        organizations: state.organizations,
        main: state.main,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(OrganizationSelection)