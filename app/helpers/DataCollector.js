import ActivityApiBody from "../services/ActivityApiBody";

export function collectAssignmentData(activityData) {
    const body = ActivityApiBody.AssignmentApiRequestBody;
    body.name = activityData.name;
    body.description = activityData.description;
    body.alwaysOnTop = activityData.alwaysOnTop;
    body.assignmentDeadline = activityData.deadline;
    body.isCommon = activityData.isCommon;
    body.allUnits = activityData.allUnits;
    body.dateCriteria = activityData.dateCriteria;
    body.datePrerequisiteActivityId = activityData.datePrerequisiteActivityId;
    body.datePrerequisiteActivityHours = activityData.datePrerequisiteActivityHours;
    body.activityConditionalBeginDate = activityData.activityConditionalBeginDate;
    body.activityConditionalEndDate = activityData.activityConditionalEndDate;
    body.prerequisiteActivityId = activityData.prerequisiteActivityId;
    body.termWeekIds = activityData.termWeekIds.length !== 0 ? activityData.termWeekIds : null;
    body.activityCompletionType = activityData.activityCompletionType;
    body.activityComplatetionValue = activityData.activityComplatetionValue;
    body.selectedClass = activityData.selectedClass;
    body.selectedActivityUnitsTagIds = activityData.selectedActivityUnitsTagIds.length !== 0 ? activityData.selectedActivityUnitsTagIds : null;
    body.selectedActivityOwnersTagIds = activityData.selectedActivityOwnersTagIds.length !== 0 ? activityData.selectedActivityOwnersTagIds : null;
    body.courseId = activityData.courseId;
    body.fileInputKey = activityData.fileInputKey

    return body;

}

export function collectDocumentData(activityData) {
    const body = ActivityApiBody.DocumentApiRequestBody;
    body.name = activityData.name;
    body.description = activityData.description;
    body.alwaysOnTop = activityData.alwaysOnTop;
    body.documentDeadline = activityData.deadline;
    body.isCommon = activityData.isCommon;
    body.allUnits = activityData.allUnits;
    body.dateCriteria = activityData.dateCriteria;
    body.datePrerequisiteActivityId = activityData.datePrerequisiteActivityId;
    body.datePrerequisiteActivityHours = activityData.datePrerequisiteActivityHours;
    body.activityConditionalBeginDate = activityData.activityConditionalBeginDate;
    body.activityConditionalEndDate = activityData.activityConditionalEndDate;
    body.prerequisiteActivityId = activityData.prerequisiteActivityId;
    body.termWeekIds = activityData.termWeekIds.length !== 0 ? activityData.termWeekIds : null;
    body.activityCompletionType = activityData.activityCompletionType;
    body.activityComplatetionValue = activityData.activityComplatetionValue;
    body.selectedClass = activityData.selectedClass;
    body.selectedActivityUnitsTagIds = activityData.selectedActivityUnitsTagIds.length !== 0 ? activityData.selectedActivityUnitsTagIds : null;
    body.selectedActivityOwnersTagIds = activityData.selectedActivityOwnersTagIds.length !== 0 ? activityData.selectedActivityOwnersTagIds : null;
    body.courseId = activityData.courseId;
    body.fileInputKey = activityData.fileInputKey

    return body;
}

export function collectLinkData(activityData) {
    const body = ActivityApiBody.LinkApiRequestBody;
    body.name = activityData.name;
    body.description = activityData.description;
    body.alwaysOnTop = activityData.alwaysOnTop;
    body.documentDeadline = activityData.deadline;
    body.isCommon = activityData.isCommon;
    body.allUnits = activityData.allUnits;
    body.dateCriteria = activityData.dateCriteria;
    body.datePrerequisiteActivityId = activityData.datePrerequisiteActivityId;
    body.datePrerequisiteActivityHours = activityData.datePrerequisiteActivityHours;
    body.activityConditionalBeginDate = activityData.activityConditionalBeginDate;
    body.activityConditionalEndDate = activityData.activityConditionalEndDate;
    body.prerequisiteActivityId = activityData.prerequisiteActivityId;
    body.termWeekIds = activityData.termWeekIds.length !== 0 ? activityData.termWeekIds : null;
    body.activityCompletionType = activityData.activityCompletionType;
    body.activityComplatetionValue = activityData.activityComplatetionValue;
    body.selectedClass = activityData.selectedClass;
    body.selectedActivityUnitsTagIds = activityData.selectedActivityUnitsTagIds.length !== 0 ? activityData.selectedActivityUnitsTagIds : null;
    body.selectedActivityOwnersTagIds = activityData.selectedActivityOwnersTagIds.length !== 0 ? activityData.selectedActivityOwnersTagIds : null;
    body.courseId = activityData.courseId;
    body.fileInputKey = activityData.fileInputKey

    return body;
}

export function collectVideoData(activityData) {
    const body = ActivityApiBody.VideoApiRequestBody;
    body.name = activityData.name;
    body.description = activityData.description;
    body.alwaysOnTop = activityData.alwaysOnTop;
    body.taskDeadline = activityData.deadline;
    body.isCommon = activityData.isCommon;
    body.allUnits = activityData.allUnits;
    body.dateCriteria = activityData.dateCriteria;
    body.datePrerequisiteActivityId = activityData.datePrerequisiteActivityId;
    body.datePrerequisiteActivityHours = activityData.datePrerequisiteActivityHours;
    body.activityConditionalBeginDate = activityData.activityConditionalBeginDate;
    body.activityConditionalEndDate = activityData.activityConditionalEndDate;
    body.prerequisiteActivityId = activityData.prerequisiteActivityId;
    body.termWeekIds = activityData.termWeekIds.length !== 0 ? activityData.termWeekIds : null;
    body.activityCompletionType = activityData.activityCompletionType;
    body.activityComplatetionValue = activityData.activityComplatetionValue;
    body.selectedClass = activityData.selectedClass;
    body.selectedActivityUnitsTagIds = activityData.selectedActivityUnitsTagIds.length !== 0 ? activityData.selectedActivityUnitsTagIds : null;
    body.selectedActivityOwnersTagIds = activityData.selectedActivityOwnersTagIds.length !== 0 ? activityData.selectedActivityOwnersTagIds : null;
    body.courseId = activityData.courseId;
    body.fileInputKey = activityData.fileInputKey
    body.fileEmbed = activityData.fileEmbed 
    body.allowDownload = activityData.allowDownload
    body.videoType = activityData.videoType
    return body;
}