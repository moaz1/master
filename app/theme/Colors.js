const Colors = {
    primary: '#39b27b',
    tab_inactive_color:'#808080',
    card_badge:'#eeede9',
    background:'#eeede9',
    lineColor:'#e2e1dd',
    warning:'#ffd699',
    softPink:'#fbf1df',
    tintColors:'#808080',
    empty_page_text:'#000',
    activityIndicator:'#39b27b',

    login_card_background:'#FFFFFF',
    login_card_text:'#000',

    annouceement_item_background:'#FFF',
    announcement_item_title:'#000',
    announcement_display_name:'#000',
    announcement_tab_background:'#FFF',
    announcement_tab_active_text_color:'#000',
    announcement_tab_inactive_text_color:'#808080',

    messages_tab_active_text_color:'#000',
    messages_tab_inactive_text_color:'#808080',
    messages_tab_background:'#FFF',

    activity_due_date_past:'#ff6f61',
    activity_card_background:'#FFF',
    activity_name_text:'#000',
    activity_completed_text:'#000',

    drawer_menu_item_color:'#000',
    drawer_menu_item_icon_color:'#000',

    course_item_title_text:'#000',
    course_daily_activity_text:'#000',
    course_detail_tab_active_text_color:'#39b27b',
    course_detail_tab_inactive_text_color:'#808080',
    course_weeks_menu_item_text_color:'#000',

    calendar_activity_title:'#000',
    calendar_document_activity_card_start_color:'#fed034',
    calendar_document_activity_card_end_color:'#fff3cc',
    calendar_video_activity_card_start_color:'#ab63cf',
    calendar_video_activity_card_end_color:'#ead8f3',
    calendar_assignment_activity_card_start_color:'#63cf9f',
    calendar_assignment_activity_card_end_color:'#d8f3e7',
    calendar_default_activity_card_color:'#FFF',

    messages_item_me_background_color:'#8de2ee',
    messages_item_someone_background_color:'#eeede9',
    message_item_text_color:'#000',

}

export default Colors;