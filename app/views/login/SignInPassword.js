import React from 'react';

import { View, Text, ImageBackground, StatusBar, TouchableOpacity, Alert, Platform, SafeAreaView, NativeModules, StyleSheet } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage';
import { Card, Input, Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';

//asset
import login_bg from '../../assets/images/login-bg.jpg';
import Loader from '../../components/Loader';

import { strings } from '../../locales/i18n';

//redux
import { connect } from 'react-redux';
import LoginActions from '../../redux/LoginRedux';
import MainActions from '../../redux/MainRedux';
import TextView from '../../components/TextView';
import { calculateLoginPageWidthPercent } from '../../helpers/Calculate';
import Colors from '../../theme/Colors';
import FontSize from '../../theme/FontSize';

//native module
import UserDataModule from '../../native/UserDataModule';

var IOSModule = NativeModules.AppDelegate; 

class SignInPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            nextButtonEnabled: true,
            userName: props.navigation.state.params.userName,
            password: '',
            widthPercent: calculateLoginPageWidthPercent(),
            passwordVisibility: false,
        }
    }
    componentDidMount() {
        this.inputRef.focus();
    }
    componentWillUnmount() {
        console.log("SIGN_IN  unmount")
    }
    pswInputChanges(text) {
        this.setState({ password: text })
        if (text.length === 0) {
            this.setState({ nextButtonEnabled: true })
        } else {
            this.setState({ nextButtonEnabled: false })
        }
    }
    async componentWillReceiveProps(props) {
        console.log("signin psw test");
        if (props.login.error)
            Alert.alert(this.props.main.languageResource.r_login_authentication_error_title || strings('r_login_authentication_error_title'),
                this.props.main.languageResource.r_login_authentication_error_message || strings('r_login_authentication_error_message'))


        if (props.login.tokenData !== this.props.login.tokenData) {
            console.log("step1");
            console.log("login auth data: ", props.login.tokenData);

            AsyncStorage.setItem("auth", JSON.stringify(props.login.tokenData))
            await this.props.setAuthData(props.login.tokenData); // for main redux (MainActions)
            //get user identities
            this.props.getUserIdentity(props.login.tokenData.access_token, this.props.main.selectedOrganization) //api call
        }

        if (props.login.userIdentity !== this.props.login.userIdentity) {
            console.log("step2");
            await AsyncStorage.setItem("userIdentity", JSON.stringify(props.login.userIdentity));

            await AsyncStorage.setItem("isLogin", "true");
            await this.props.setUserIdentity(props.login.userIdentity); // for main redux (MainActions)
            console.log("redirect to main activities");

            console.log("native module datas userId: ", props.login.userIdentity.userId);
            console.log("native module datas organization Id: ", props.login.userIdentity.organizationId);

            if (Platform.OS === 'android') {
                UserDataModule.userData(props.login.userIdentity.userId, props.login.userIdentity.organizationId);
            } else {
                IOSModule.registerPushNotifications(props.login.userIdentity.userId, props.login.userIdentity.organizationId)
            }

            console.log("signedIn");
            this.props.navigation.navigate('SignedIn');
        }
    }

    // async _callNativeModules() {
    //     console.log("native module datas userId: ",this.props.main.userIdentity.userId);
    //     console.log("native module datas organization Id: ",this.props.main.userIdentity.organizationId);
    //     if (Platform.OS === 'android') {
    //         UserDataModule.userData(this.props.main.userIdentity.userId, this.props.main.userIdentity.organizationId);
    //     } else {
    //         IOSModule.registerPushNotifications(this.props.main.userIdentity.userId, this.props.main.userIdentity.organizationId)
    //     }
    // }

    async btnAuth() {
        AsyncStorage.setItem("loggedUserName",this.state.userName);
        AsyncStorage.setItem("loggedUserPsw",this.state.password);
        this.props.getAccessToken(this.state.userName, this.state.password, this.props.main.selectedOrganization);

    }
    _renderPasswordVisibility() {
        if (this.state.passwordVisibility) return <Ionicons name="md-eye-off" size={24} onPress={() => this.setState({ passwordVisibility: !this.state.passwordVisibility })} />
        else return <Ionicons name="md-eye" size={24} onPress={() => this.setState({ passwordVisibility: !this.state.passwordVisibility })} />
    }
    render() {
        const { goBack } = this.props.navigation;

        return (
            <View style={{ flex: 1 }}>
                <ImageBackground source={login_bg} style={{ width: '100%', height: '100%', position: 'absolute' }} />
                <SafeAreaView style={styles.mainContainer}>
                    {Platform.OS === "android" ? <StatusBar translucent backgroundColor="transparent" /> : <StatusBar hidden={true} />}
                    <Loader loading={this.props.login.fetching} />
                    <View style={{ flex: 1, flexDirection: 'column', width: this.state.widthPercent, alignSelf: 'center' }}>
                        <Card containerStyle={{ padding: 20, backgroundColor: Colors.login_card_background, margin: 20, borderRadius: 5 }}>
                            <TextView weight="medium" style={{ fontSize: FontSize.login_header, color: Colors.login_card_text }}>{this.props.main.languageResource.r_login_password_greetings || strings('r_login_password_greetings')}</TextView>

                            <Input
                                ref={ref => (this.inputRef = ref)}
                                secureTextEntry={!this.state.passwordVisibility}
                                placeholder={this.props.main.languageResource.r_login_password_input_text || strings('r_login_password_input_text')}
                                shake={true}
                                containerStyle={{ marginTop: 10 }}
                                onChangeText={(text) => this.pswInputChanges(text)}
                                rightIcon={this._renderPasswordVisibility()} />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20 }}>
                                <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }} onPress={() => goBack()}>
                                    <Icon name="arrowleft" size={20} color="black" />
                                    <TextView weight="medium" style={{ paddingStart: 10 }} >{this.props.main.languageResource.r_login_back || strings("r_login_back")}</TextView>
                                </TouchableOpacity>

                                <Button
                                    disabled={this.state.nextButtonEnabled}
                                    title={this.props.main.languageResource.r_login_next || strings('r_login_next')}
                                    onPress={() => this.btnAuth()}
                                    icon={<Icon name="arrowright" size={20} color="white" />}
                                    iconRight
                                    buttonStyle={{ backgroundColor: Colors.primary }}
                                    titleStyle={{ marginEnd: 8 }} />
                            </View>

                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', marginTop: 30 }}>
                                <TextView weight="medium" style={{ fontSize: FontSize.login_alms }}>2013-{new Date().getFullYear()} {this.props.main.languageResource.r_login_alms || strings('r_login_alms')}</TextView>
                            </View>
                        </Card>
                    </View>
                </SafeAreaView>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        marginTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,

    },
});
const mapDispatchToProps = (dispatch) => {
    return {
        getAccessToken: (...args) => dispatch(LoginActions.getAccessTokenRequest(...args)),
        getUserIdentity: (...args) => dispatch(LoginActions.getUserIdentityRequest(...args)),
        setAuthData: (...args) => dispatch(MainActions.setAuthData(...args)),
        setUserIdentity: (...args) => dispatch(MainActions.setUserIdentity(...args)),
    }
}
const mapStateToProps = (state) => {
    return {
        login: state.login,
        main: state.main,
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInPassword)