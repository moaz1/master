import React from 'react';
import { View, Text, SafeAreaView, TouchableOpacity, ScrollView, RefreshControl } from 'react-native';
import { Agenda, LocaleConfig } from 'react-native-calendars';
import Moment from 'moment';
import { connect } from 'react-redux';
import ScheduleActions from '../../../redux/ScheduleRedux';
import TextView from '../../../components/TextView';
import Constants from '../../../services/Constants';
import Loader from '../../../components/Loader';
import PageEmpty from '../../../components/courses/PageEmpty';
import Colors from '../../../theme/Colors';
import { strings } from '../../../locales/i18n';
import AsyncStorage from '@react-native-community/async-storage';
import LocalStorageConstants from '../../../local/LocalStorageConstants';

import NetInfo from '@react-native-community/netinfo';


class CalenderMain extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            items: {},
            startDate: null,
            endDate: null,
            isLoading: true,
            selectedDay: Moment().format('YYYY-MM-DD'),
            daySelected: false,
            dayTimestamp: Moment().unix(),
        };
    }

    calendarApiBody = {
        remote: true,
        almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
        accessToken: this.props.main.authData.access_token,
        startDate: null,
        endDate: null,
        contextType: 16,
        take: Constants.CalendarApiResponseQuantity,
        skip: 0
    }
    componentWillMount() {
        AsyncStorage.getItem(LocalStorageConstants.Language).then(selectedLang => {
            if (selectedLang === LocalStorageConstants.LanguageTrResource) {
                LocaleConfig.locales['tr'] = {
                    monthNames: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
                    monthNamesShort: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
                    dayNames: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
                    dayNamesShort: ['Pzt', 'Sal', 'Çar', 'Per', 'Cum', 'Cmt', 'Paz']
                }
                LocaleConfig.defaultLocale = 'tr';
            }
        })
    }
    componentDidMount() {
        this.loadItems(Moment().valueOf())

    }

    componentWillReceiveProps(props) {
        if (!props.schedule.calendarDataFetching && !props.schedule.calendarDataError) {
            this.setState({refreshing:false})
            props.schedule.calendarData.map((data) => {

                const strTime = Moment(data.startDate).format('YYYY-MM-DD');
                if (!this.state.items[strTime]) {
                    this.state.items[strTime] = [];
                }
                this.state.items[strTime].push({
                    data: data,
                })
            })
        }
    }

    async dayPressed(day) {
        console.log("dat: ", day);
        const time = day.timestamp + 0 * 24 * 60 * 60 * 1000;
        const strTime = this.timeToString(time);
        console.log("day pressed : ", strTime);
        this.setState({
            selectedDay: Moment(time).format('YYYY-MM-DD'),
            daySelected: true,
            dayTimestamp: day.timestamp,
        })
        if (!this.state.items[strTime]) {
            // await this.setState({ //clear previos items state
            //     items: {}
            // })
            console.log("do call api");
            this.loadItems(day.timestamp)
        } else {
            console.log("do nothing");
        }

    }

    _renderAgenda() {
        if (!this.props.schedule.calendarDataFetching || this.state.daySelected) {
            return (
                <Agenda
                    items={this.state.items}
                    //loadItemsForMonth={this.loadItems.bind(this)}
                    selected={this.state.selectedDay}
                    renderItem={this.renderItem.bind(this)}
                    onDayPress={(day) => { this.dayPressed(day) }}
                    //renderEmptyDate={this.renderEmptyDate.bind(this)}
                    renderEmptyData={this.renderEmptyDate.bind(this)}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    rowHasChanged={this.rowHasChanged.bind(this)}
                    //markingType={'period'}
                    // markedDates={{
                    //    '2017-05-08': {textColor: '#666'},
                    //    '2017-05-09': {textColor: '#666'},
                    //    '2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
                    //    '2017-05-21': {startingDay: true, color: 'blue'},
                    //    '2017-05-22': {endingDay: true, color: 'gray'},
                    //    '2017-05-24': {startingDay: true, color: 'gray'},
                    //    '2017-05-25': {color: 'gray'},
                    //    '2017-05-26': {endingDay: true, color: 'gray'}}}
                    // monthFormat={'yyyy'}
                    // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
                    theme={{ calendarBackground: 'white' }}
                //renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
                />
            )
        }
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.props.schedule.calendarDataFetching} />
                <View style={{ flex: 1 }}>
                    {this._renderAgenda()}
                </View>
            </SafeAreaView>
        );
    }

    //getting data from current month and previous month
    loadItems(day) {
        const startTime = day - 5 * 24 * 60 * 60 * 1000;
        const startDate = this.timeToString(startTime);

        const endTime = day + 15 * 24 * 60 * 60 * 1000;
        const endDate = this.timeToString(endTime);

        console.log("startDate: ", startDate);
        console.log("endDate: ", endDate);
        this.setState({
            items: {}
        })
        this.calendarApiBody.startDate = startDate;
        this.calendarApiBody.endDate = endDate

        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.calendarApiBody.remote = true; else this.calendarApiBody.remote = false
            this.props.getCalendarDataRequest(this.calendarApiBody)
        })

    }

    _calendarItemClick() {

    }
    renderItem(item) {
        console.log("claender item :", item);
        let startColor, endColor;

        switch (item.data.activityType) {
            case Constants.ActivityType.Video:
                startColor = Colors.calendar_video_activity_card_start_color
                endColor = Colors.calendar_video_activity_card_end_color
                break;
            case Constants.ActivityType.Assignment:
                startColor = Colors.calendar_assignment_activity_card_start_color
                endColor = Colors.calendar_assignment_activity_card_end_color
                break;
            case Constants.ActivityType.Document:
                startColor = Colors.calendar_document_activity_card_start_color
                endColor = Colors.calendar_document_activity_card_end_color
                break;
            default:
                startColor = Colors.calendar_default_activity_card_color
                endColor = Colors.calendar_default_activity_card_color

        }
        return (
            <TouchableOpacity style={{ flex: 1, borderRadius: 5, marginRight: 10, marginTop: 17, flexDirection: 'row' }}
                onPress={() => this.props.navigation.navigate('activityDetail', { Activity: item.data })}>
                <View style={{ backgroundColor: startColor, width: 5, borderTopStartRadius: 5, borderBottomStartRadius: 5 }} />
                <View style={{ backgroundColor: endColor, flex: 1, padding: 10, borderTopEndRadius: 5, borderBottomEndRadius: 5 }}>
                    <TextView weight="bold" style={{ color: Colors.calendar_activity_title }}>{item.data.activityName}</TextView>
                </View>
            </TouchableOpacity>

        );
    }

    _onRefresh() {
        this.loadItems(this.state.dayTimestamp)
    }
    renderEmptyDate() {
        return (
            <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}>
                <PageEmpty text={this.props.main.languageResource.r_calendar_empty_desctiption || strings('r_calendar_empty_desctiption')} />
            </ScrollView>
            // <View style={{ flex: 1 }}>
            //     <PageEmpty text={this.props.main.languageResource.r_calendar_empty_desctiption || strings('r_calendar_empty_desctiption')} />
            // </View>
        );
    }

    rowHasChanged(r1, r2) {
        return r1.name !== r2.name;
    }

    timeToString(time) {
        const date = new Date(time);
        return date.toISOString().split('T')[0];
    }
}



const mapDispatchToProps = (dispatch) => {
    return {
        getCalendarDataRequest: (...args) => dispatch(ScheduleActions.getCalendarDataRequest(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        schedule: state.schedule,
        main: state.main
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CalenderMain)