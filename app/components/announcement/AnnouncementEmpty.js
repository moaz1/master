import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';

//assets
import announcement_empty from '../../assets/images/announcement-empty.png';
import { strings } from '../../locales/i18n';
import TextView from '../TextView';
import FontSize from '../../theme/FontSize';
import Colors from '../../theme/Colors';

import { connect } from 'react-redux';

class AnnouncementEmpty extends React.Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <View style={style.emptyMain}>
                <Image source={announcement_empty} style={style.emptyImage} />
                <TextView style={style.emptyText} weight="bold">
                    {this.props.main.languageResource.r_announcements_empty || strings('r_announcements_empty')}
                </TextView>
            </View>
        );
    }
}

//Style
const style = StyleSheet.create({
    emptyMain: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    emptyImage: {
        width: '30%',
        height: '30%',
        resizeMode: "contain"
    },
    emptyText: {
        fontSize: FontSize.page_empty_text,
        color: Colors.empty_page_text
    }
})
const mapDispatchToProps = (dispatch) => {
    return {}
}

const mapStateToProps = (state) => {
    return {
        main: state.main
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AnnouncementEmpty);