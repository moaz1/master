import React from 'react';
import { createBottomTabNavigator, createStackNavigator, createAppContainer, createSwitchNavigator, createMaterialTopTabNavigator, createDrawerNavigator } from 'react-navigation';
import Ionicons from 'react-native-vector-icons/Ionicons';
import DeviceInfo from 'react-native-device-info';
//tabs
import CoursesMain from '../views/tabs/courses/CoursesMain';
import CourseDetail from '../views/tabs/courses/CourseDetail';
import NotesMain from '../views/tabs/notes/NotesMain';
import ForumMain from '../views/tabs/forum/ForumMain';
import AnnouncementMain from '../views/tabs/announcement/AnnouncementMain';
import AnnouncementDetail from '../views/tabs/announcement/AnnouncementDetail';
import CreateNewAnnouncement from '../views/tabs/announcement/CreateNewAnnouncement';
import MenuItem2 from '../views/drawer/MenuItem2';
import CalenderMain from '../views/tabs/calender/CalenderMain';
import FilesMain from '../views/tabs/files/FilesMain';
import MessagesMain from '../views/tabs/messages/MessagesMain';
import ProfileMain from '../views/tabs/profile/ProfileMain';

//localization
import { strings } from '../locales/i18n';
import Colors from '../theme/Colors';


//courses stack navigator
const LessonStack = (title) => {
    return createStackNavigator({
        Main: {
            screen: CoursesMain,
            navigationOptions: {
                header: null
            }
        },
    }, {
        headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: {
            tabBarLabel: title,
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'logo-buffer';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }
    })
}



//notes stack navigator
const NotesStack = createStackNavigator({
    Main: {
        screen: NotesMain,
        navigationOptions: {
            header: null
        }
    },
}, { headerMode: 'float', headerLayoutPreset: 'center' });
NotesStack.navigationOptions = {
    tabBarLabel: strings('tabs.notes'),
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
        let IconComponent = Ionicons;
        let iconName = 'md-today';
        return <IconComponent name={iconName} size={25} color={tintColor} />;
    },
}

//Calender stack navigator
const CalenderStack = (title) => {
    return createStackNavigator({
        Main: {
            screen: CalenderMain,
            navigationOptions: {
                header: null
            }
        },
    }, {
        headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: {
            tabBarLabel: title,
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'ios-calendar';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }
    });

}



//Files stack navigator
const FilesStack = (title) => {
    return createStackNavigator({
        Main: {
            screen: FilesMain,
            navigationOptions: {
                header: null
            }
        },
    }, {
        headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: {
            tabBarLabel: title,
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'ios-folder-open';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }
    });
}

const MessagesStack = (title) => {
    return createStackNavigator({
        Main: {
            screen: MessagesMain,
            navigationOptions: {
                header: null
            }
        },
    }, {
        headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: {
            tabBarLabel: title,
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'ios-chatboxes';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }
    });

}



//Profile stack navigator
const ProfileStack = (title) => {
    return createStackNavigator({
        Main: {
            screen: ProfileMain,
            navigationOptions: {
                header: null
            }
        },
    }, {
        headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: {
            tabBarLabel: title,
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'ios-person';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }
    });
}


//Forum stack navigator
const ForumStack = createStackNavigator({
    Main: {
        screen: ForumMain,
        navigationOptions: {
            //title: strings('forum.forum_title')
            header: null
        }
    },
}, { headerMode: 'float', headerLayoutPreset: 'center' })
ForumStack.navigationOptions = {
    tabBarLabel: strings('tabs.forum'),
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
        let IconComponent = Ionicons;
        let iconName = 'ios-chatbubbles';
        return <IconComponent name={iconName} size={25} color={tintColor} />;
    },
}

//Announcement stack navigator
const AnnouncementStack = (title) => {
    return createStackNavigator({

        Main: {
            screen: AnnouncementMain,
            navigationOptions: {
                header: null
            }
        },
    }, {
        headerMode: 'float', headerLayoutPreset: 'center', navigationOptions: {
            tabBarLabel: title,
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'ios-notifications';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        }
    })
}

//Menu stack navigator
const MenuStack = () => {
    return createStackNavigator({
        MenuMain: {
            screen: MenuItem2,
            navigationOptions: {
                header: null
            }
        },
    }, {
        headerMode: 'float', navigationOptions: ({ navigation }) => ({
            tabBarLabel: strings('tabs.menu'),
            tabBarOnPress: () => {
                navigation.openDrawer()
            },
            tabBarIcon: ({ focused, horizontal, tintColor }) => {
                let IconComponent = Ionicons;
                let iconName = 'ios-more';
                return <IconComponent name={iconName} size={25} color={tintColor} />;
            },
        })
    })

}

//tablet ve telefon için dinamik ana tab menüsü (menü string adına göre)
export const createBottomTab = (menu, languageResources) => {
    var arrayStack = [];
    if (DeviceInfo.isTablet()) {
        for (let i = 0; i < 4; i++) {
            switch (menu[i].name) {
                case 'r_menu_my_courses':
                    const courseStack = LessonStack(languageResources.r_menu_my_courses)
                    arrayStack.push(courseStack);
                    break;
                case 'r_menu_calendar':
                    const calanderStack = CalenderStack(languageResources.r_menu_calendar)
                    arrayStack.push(calanderStack);
                    break
                case 'r_menu_announcement':
                    const announcementStack = AnnouncementStack(languageResources.r_menu_announcements)
                    arrayStack.push(announcementStack);
                    break
                case 'r_menu_messages':
                    const messagesStack = MessagesStack(languageResources.r_menu_messages);
                    arrayStack.push(messagesStack);
                    break;
                case 'r_menu_my_files':
                    const filesStack = FilesStack(languageResources.r_menu_my_files);
                    arrayStack.push(filesStack);
                    break;
                case 'r_menu_profile':
                    const profileStack = ProfileStack(languageResources.r_menu_profile)
                    arrayStack.push(profileStack)
                    break;
            }
        }
        const menuStack = MenuStack();
        arrayStack.push(menuStack);
    } else {
        for (let i = 0; i < 4; i++) {
            switch (menu[i].name) {
                case 'r_menu_my_courses':
                    const courseStack = LessonStack(languageResources.r_menu_my_courses)
                    arrayStack.push(courseStack);
                    break;
                case 'r_menu_calendar':
                    const calanderStack = CalenderStack(languageResources.r_menu_calendar)
                    arrayStack.push(calanderStack);
                    break
                case 'r_menu_announcement':
                    const announcementStack = AnnouncementStack(languageResources.r_menu_announcements)
                    arrayStack.push(announcementStack);
                    break
                case 'r_menu_messages':
                    const messagesStack = MessagesStack(languageResources.r_menu_messages);
                    arrayStack.push(messagesStack);
                    break;
                case 'r_menu_my_files':
                    const filesStack = FilesStack(languageResources.r_menu_my_files);
                    arrayStack.push(filesStack);
                    break;
                case 'r_menu_profile':
                    const profileStack = ProfileStack(languageResources.r_menu_profile)
                    arrayStack.push(profileStack)
                    break;
            }
        }
        const menuStack = MenuStack();
        arrayStack.push(menuStack);

    }

    return createBottomTabNavigator(arrayStack, {
        tabBarOptions: {
            activeTintColor: Colors.primary,
            inactiveTintColor: Colors.tab_inactive_color,
        },
    });

} 