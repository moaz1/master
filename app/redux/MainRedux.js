import { createReducer, createActions } from 'reduxsauce';
import Immutable from 'seamless-immutable';

/* ------------- Types and Action Creators ------------- */
const { Types, Creators } = createActions({
    setLanguageResources: ['data'],
    setSelectedOrganization: ['data'],
    setAuthData: ['data'],
    setUserIdentity: ['data'],

    getLanguageResourceRequest: ['data'],
    getLanguageResourceSuccess: ['data'],
    getLanguageResourceFailure: ['error'],

    clearRedux: []

});

export const MainTypes = Types
export default Creators

/* ------------- Initial State ------------- */
export const INITIAL_STATE = Immutable({
    languageResource: [],
    languageResourceFetching: false,
    languageResourceErrorMessage: '',
    languageResourceError: false,

    selectedOrganization: null,
    organizationIsReady: false,
    authData: null,
    authDataIsReady: false,
    userIdentity: null,
    userIdenetityIsReady: false,
});

/* ------------- Reducers ------------- */
export const setLanguageResources = (state, action) => {
    return state.merge({ languageResource: action.data })
}

export const setSelectedOrganization = (state, action) => {
    console.log("main redux selected organ. saved: ", action.data);
    return state.merge({ selectedOrganization: action.data, organizationIsReady: true })
}

export const setAuthData = (state, action) => {
    console.log("main redux auth data saved: ", action.data.access_token);
    return state.merge({ authData: action.data, authDataIsReady: true })
}

export const setUserIdentity = (state, action) => {
    console.log("main redux user identity saved: ", action.data);
    return state.merge({ userIdentity: action.data, userIdenetityIsReady: true })
}

/* ---- */
export const getLanguageResourceRequest = (state, action) => {
    return state.merge({ languageResourceFetching: true, languageResourceError: false, languageResourceErrorMessage: '' })
}

export const getLanguageResourceSuccess = (state, action) => {
    return state.merge({ languageResourceFetching: false, languageResourceError: false, languageResourceErrorMessage: '', languageResource: action.data })
}

export const getLanguageResourceFailure = (state, action) => {
    return state.merge({ languageResourceFetching: false, languageResourceError: true, languageResourceErrorMessage: action.error })
}





export const clearRedux = (state, action) => {
    return state;
}

/* ------------- Connection Reducers To Types ------------- */
export const reducer = createReducer(INITIAL_STATE, {
    [Types.SET_LANGUAGE_RESOURCES]: setLanguageResources,
    [Types.SET_SELECTED_ORGANIZATION]: setSelectedOrganization,
    [Types.SET_AUTH_DATA]: setAuthData,
    [Types.SET_USER_IDENTITY]: setUserIdentity,

    [Types.GET_LANGUAGE_RESOURCE_REQUEST]: getLanguageResourceRequest,
    [Types.GET_LANGUAGE_RESOURCE_SUCCESS]: getLanguageResourceSuccess,
    [Types.GET_LANGUAGE_RESOURCE_FAILURE]: getLanguageResourceFailure,

    [Types.CLEAR_REDUX]: clearRedux,
})