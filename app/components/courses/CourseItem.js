import React from 'react';
import { View, Text, Image, Platform } from 'react-native';
import { Card, Tooltip, Badge } from 'react-native-elements';

import PercentageCircle from 'react-native-percentage-circle';


import Icon from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../theme/Colors';
import TextView from '../TextView';
import FontSize from '../../theme/FontSize';

export default class CourseItem extends React.Component {
    constructor(props) {
        super(props)

        this.state = ({
            course: props.course,
            image: { uri: props.course.logoUrl },
            progress: 0
        })
    }

    componentDidMount() {
        if (this.state.course.hasOwnProperty('progress')) {
            this.setState({
                progress: this.state.course.progress
            })
        }
    }

    onErrorImageLoad() {
        this.setState({
            image: require('../../assets/images/image_not_found.png')
        })
    }
    render() {
        return (
            <Card containerStyle={{ margin: 5, padding: 10 }}>
                {/* <Image source={this.state.image} onError={this.onErrorImageLoad.bind(this)} style={{ height:100, width:100, resizeMode: 'contain', flex: 1 }} /> */}
                <View style={{ flexDirection: 'row' }}>
                    {/* Avatar */}

                    <View style={{ height: 80, width: 80, justifyContent: 'center', alignItems: 'center' }}>
                        <PercentageCircle
                            radius={35}
                            borderWidth={3}
                            percent={this.state.progress}
                            color={Colors.primary}>
                            <Image source={this.state.image} onError={this.onErrorImageLoad.bind(this)} style={{ height: 80, width: 80, resizeMode: 'contain', flex: 1 }} />
                        </PercentageCircle>
                        {/* <Image source={this.state.image} onError={this.onErrorImageLoad.bind(this)} style={{ height: 80, width: 80, resizeMode: 'contain', flex: 1 }} /> */}

                    </View>

                    {/* Course Content */}
                    <View style={{ flex: 1, flexDirection: 'column', marginStart: 10, alignSelf: 'center' }}>
                        <TextView weight="bold" style={{ color: Colors.course_item_title_text, fontSize: FontSize.course_item_title }}>{this.state.course.name}</TextView>
                        <TextView style={{ color: 'black' }}>{this.state.course.teachers}</TextView>

                        {/* Action Buttons */}

                        {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 7, paddingStart: 10, paddingEnd: 10 }}>
                            <View>
                                <Icon name="new-releases" size={20} style={{ padding: 5 }} />
                                <Badge status="success" containerStyle={{ position: 'absolute', top: 0, right: 0 }} />
                            </View>

                            <Tooltip backgroundColor='black' popover={
                                <Text style={{color:'white'}}></Text>
                            }>
                                <View>
                                    <Icon name="access-alarm" size={20} style={{ padding: 5 }} />
                                    <Badge status="error" containerStyle={{ position: 'absolute', top: 0, right: 0 }} />
                                </View>
                            </Tooltip>
                            <View>
                                <Icon name="question-answer" size={20} style={{ padding: 5 }} />
                                <Badge status="warning" containerStyle={{ position: 'absolute', top: 0, right: 0 }} />
                            </View>
                        </View> */}

                    </View>

                </View>
            </Card>
        );
    }
}