import React from 'react';
import { View, Text, SafeAreaView, FlatList, TouchableOpacity, RefreshControl } from 'react-native';
import PageEmpty from '../../../components/courses/PageEmpty'
import { strings } from '../../../locales/i18n';
import { connect } from 'react-redux'
import MessagesActions from '../../../redux/MessagesRedux';

import MessageGroupItem from '../../../components/messages/MessageGroupItem';
import MessagePersonItem from '../../../components/messages/MessagePersonItem';
import Constants from '../../../services/Constants';
import NetInfo from '@react-native-community/netinfo';

class MessagesGroup extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            groupMessage: [],
        }

        this.getMessagesGroupApiRequestBody = {
            remote: true,
            accessToken: this.props.main.authData.access_token,
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            take: Constants.ApiResponseQuantity,
            skip: 0
        }
    }
    componentDidMount() {
        this.generateGroupMessage();
    }

    componentWillReceiveProps(props) {
        if (this.props.messages.messagesGroups !== props.messages.messagesGroups) {
            var filteredGroupMessage = [];
            props.messages.messagesGroups.map((group) => {
                if (group.targetType === Constants.MessagesTargetTypes.ClassGroup) {
                    filteredGroupMessage.push(group);
                }
            })
            this.setState({ groupMessage: filteredGroupMessage, refreshing: false });
        }
    }
    generateGroupMessage() {
        var filteredGroupMessage = [];
        this.props.messages.messagesGroups.map((group) => {
            if (group.targetType === Constants.MessagesTargetTypes.ClassGroup) {
                filteredGroupMessage.push(group);
            }
        })
        this.setState({ groupMessage: filteredGroupMessage, refreshing: false });
    }

    keyExtractor = (item, index) => item.messageId


    _onMessageItemClick(message) {
        this.props.navigation.navigate('MessageDetail', { message: message })
    }

    _onRefresh() {
        this.setState({ refreshing: true })
        this.getMessagesGroupApiRequestBody.skip = 0;
        this.props.clearMessageGroups()
        this.messageGroupApiCall();

    }

    messageGroupApiCall() {
        this.setState({ scrolled: false })
        NetInfo.fetch().then(statu => {
            if (statu.isConnected) this.getMessagesGroupApiRequestBody.remote = true; else this.getMessagesGroupApiRequestBody.remote = false
            this.props.getMessagesGroups(this.getMessagesGroupApiRequestBody);
        })
    }
    _renderContent() {
        if (this.state.groupMessage.length === 0) {
            return (
                <PageEmpty text={this.props.main.languageResource.r_messages_empty_group || strings('r_messages_empty_group')} />
            )
        } else {
            return (
                <FlatList
                    keyExtractor={this.keyExtractor}
                    data={this.state.groupMessage}
                    extraData={this.state.groupMessage}
                    refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={() => this._onRefresh()} />}
                    renderItem={({ item }) => (
                        <TouchableOpacity activeOpacity={0.7} style={{ flexDirection: 'column' }}
                            onPress={() => this._onMessageItemClick(item)}>
                            {item.targetType === Constants.MessagesTargetTypes.ClassGroup
                                ? <MessageGroupItem message={item} />
                                : <MessagePersonItem message={item} />}
                        </TouchableOpacity>
                    )} />
            )
        }
    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1 }}>

                    {this._renderContent()}
                </View>
            </SafeAreaView>
        );
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getMessagesGroups: (...args) => dispatch(MessagesActions.getMessagesGroupsRequest(...args)),
        clearMessageGroups: (...args) => dispatch(MessagesActions.clearMessageGroups(...args)),
    }
}

const mapStateToProps = (state) => {
    return {
        messages: state.messages,
        main: state.main
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MessagesGroup)