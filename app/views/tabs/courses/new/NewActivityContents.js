import React from 'react';
import { SafeAreaView, View, ScrollView, TextInput, TouchableOpacity, Platform, FlatList, Alert, Switch } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NavigationActions } from 'react-navigation';
import Colors from '../../../../theme/Colors';
import TextView from '../../../../components/TextView';
import ActivityCompletionCriterion from '../../../../components/courses/new/ActivityCompletionCriterion';
import ActivityByViewCriterion from '../../../../components/courses/new/ActivityByViewCriterion';
import ActivityWeekList from '../../../../components/courses/new/ActivityWeekList';
import { Button } from 'react-native-elements';
import DateTimePicker from "react-native-modal-datetime-picker";
import Moment from 'moment'
import Loader from '../../../../components/Loader';
import DocumentPicker from 'react-native-document-picker';
import PercentageCircle from 'react-native-percentage-circle';

import Constants from '../../../../services/Constants';
import Icon from 'react-native-vector-icons/AntDesign'

import { connect } from 'react-redux';
import AddActivityActions from '../../../../redux/AddActivityRedux';
import SettingsActions from '../../../../redux/SettingsRedux';
import { bytesToSize, createGuid } from '../../../../helpers/Calculate';
import { strings } from '../../../../locales/i18n';
import Uploader from '../../../../components/Uploader';

class NewActivityContents extends React.Component {
    constructor(props) {
        super(props)

        this.state = {
            addingActivityData: this.props.navigation.getParam('addingActivityData', null),
            activityName: '',
            activityDescription: '',
            assignmentDeadline: null,
            fileUploadKey: createGuid(),
            activityViewCriterion: 0,
            ActivityCompletionType: Constants.ActivityCompletionTypes.None,
            selecteedWeekIds: [],
            isDatePickerVisible: false,
            isTimePickerVisible: false,
            isConditionalActivitiesVisibility: false,
            deliveryDate: null,
            deliveryTime: null,
            viewByBeginDate: null,
            viewByEndDate: null,
            deliveryDateProcessing: false,
            viewByBeginDateProcessing: false,
            viewByEndDateProcessing: false,
            selectedConditionalActivity: null,
            selectedFile: null,
            minimumDate: undefined,
            uploadingVisibility: false,
            videoAllowDownload: true,
            onBackground: false,
            uploadingPercentage: 0,
            uploadingSuccess: false,
            conditionalActivities: [],
            conditionalActivitySearchText: '',
            datePrerequisiteActivityHours: 0,
            activityComplatetionValue: 0,
            dateCriteria: 0,
            linkActivityUrl: "",
            fileEmbed: null,
            videoType: Constants.VideoTypes.Local
        }

        this.conditionalActivitiesRequestApiBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            courseId: this.props.courseDetail.course.courseId,
            classId: this.props.courseDetail.course.classId
        }

        this.uploadFileRequestApiBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
            selectedFile: null,
            formData: null,
            fileUploadId: null
        }

        this.getFileSettingsApiRequestBody = {
            almsPlusApiUrl: this.props.main.selectedOrganization.almsPlusApiUrl,
            accessToken: this.props.main.authData.access_token,
        }
    }
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            headerLeft: (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <Ionicons style={{ paddingLeft: 20, paddingRight: 20, }} name="ios-arrow-back" color="black" size={25} onPress={() => {
                        const backActions = NavigationActions.back({
                            key: null
                        });
                        navigation.dispatch(backActions)
                    }} />
                </View>
            ),
            title: params.languageResource !== undefined ? params.languageResource.r_activity_add_content_title || strings('r_activity_add_content_title') : strings('r_activity_add_content_title'),
        }
    }
    componentDidMount() {
        this.props.navigation.setParams({
            languageResource: this.props.main.languageResource
        })
        this.props.getTagsConditionalActivity(this.conditionalActivitiesRequestApiBody)

        if (this.props.settings.fileSettings.length === 0)
            this.props.getFileSettings(this.getFileSettingsApiRequestBody);
    }

    componentWillReceiveProps(props) {
        if (this.props.addActivity.conditionalActivities !== props.addActivity.conditionalActivities) {
            this.setState({
                conditionalActivities: props.addActivity.conditionalActivities
            })
        }
    }

    _renderActivityNameInput() {
        return (
            <View style={{ marginTop: 10 }}>
                <TextView weight="bold" style={{ color: "black", fontSize: 18 }}>
                    {this.props.main.languageResource.r_activity_add_content_activity_name || strings('r_activity_add_content_activity_name')}
                </TextView>
                <TextInput style={{ fontSize: 16, height: 40, borderColor: Colors.background, borderWidth: 1, marginTop: 5 }}
                    placeholder={this.props.main.languageResource.r_activity_add_content_activity_name_input_placeholder || strings('r_activity_add_content_activity_name_input_placeholder')}
                    onChangeText={text => this.setState({ activityName: text })} />
            </View>

        )
    }
    _renderActivityDesciption() {
        return (
            <View style={{ marginTop: 20 }}>
                <TextView weight="bold" style={{ color: "black", fontSize: 18 }}>
                    {this.props.main.languageResource.r_activity_add_content_activity_description || strings('r_activity_add_content_activity_description')}
                </TextView>
                <TextInput multiline={true}
                    numberOfLines={Platform.OS === 'ios' ? null : 5}
                    minHeight={Platform.OS === 'ios' ? (20 * 5) : null}
                    style={{ fontSize: 16, borderColor: Colors.background, textAlignVertical: 'top', borderWidth: 1, marginTop: 5 }}
                    placeholder={this.props.main.languageResource.r_activity_add_content_activity_description_input_placeholder || strings('r_activity_add_content_activity_description_input_placeholder')}
                    onChangeText={text => this.setState({ activityDescription: text })} />
            </View>
        )
    }
    _renderFileSelector() {
        if (this.state.addingActivityData.activityType === Constants.ActivityType.Assignment ||
            this.state.addingActivityData.activityType === Constants.ActivityType.Document ||
            this.state.addingActivityData.activityType === Constants.ActivityType.Video)
            return (
                <View style={{ marginTop: 20 }}>
                    <TextView weight="bold" style={{ color: "black", fontSize: 18 }}>
                        {this.props.main.languageResource.r_activity_add_content_upload_file || strings('r_activity_add_content_upload_file')}
                    </TextView>
                    {this._renderFileImage()}
                </View>
            )
        else return null;
    }


    async openFilePicker() {
        if (this.state.onBackground) {
            this.setState({ uploadingVisibility: true })
        } else {
            try {
                var types = null
                if (this.state.addingActivityData.activityType === Constants.ActivityType.Video) {
                    types = [DocumentPicker.types.video]
                } else {
                    types = Platform.OS == "android" ? Constants.AndroidMimeTypes : Constants.IOSMimeTypes
                }
                const res = await DocumentPicker.pick({
                    type: types,
                });

                if (this.state.selectedFile === null || (this.state.selectedFile !== null && res.name !== this.state.selectedFile.name)) {
                    this.uploadFileRequestApiBody.selectedFile = res;
                    this.uploadFileRequestApiBody.fileUploadId = this.state.fileUploadKey;
                    await this.setState({ selectedFile: null })
                    await this.setState({ selectedFile: res, uploadingVisibility: true })
                }



            } catch (error) {
                console.log('document picker error ', error)
                if (DocumentPicker.isCancel(error)) {
                    // User cancelled the picker, exit any dialogs or menus and move on
                } else {

                    throw error;
                }
            }
        }

    }
    _renderFileImage() {
        // if (this.state.isFilePicked)
        //     return (
        //         <View style={{ marginTop: 10 }}>
        //             <TouchableOpacity style={{ height: 150, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.background }}
        //                 onPress={() => this.openFilePicker()}>
        //                 {this.state.onBackground
        //                     ? <PercentageCircle
        //                         radius={17}
        //                         bgcolor={Colors.background}
        //                         borderWidth={3}
        //                         percent={this.state.uploadingPercentage}
        //                         color={Colors.primary}>
        //                         <TextView weight="medium" style={{ fontSize: 11, color: 'black' }}>
        //                             {this.state.uploadingPercentage}
        //                         </TextView>
        //                     </PercentageCircle>
        //                     : <Ionicons size={70} name="ios-checkmark" color="black" />}
        //                 <TextView style={{ color: 'black' }} weight="bold">{this.state.selectedFile.name}</TextView>
        //                 <TextView style={{ color: 'black', marginTop: 5 }} weight="bold">{bytesToSize(this.state.selectedFile.size)}</TextView>
        //             </TouchableOpacity>
        //         </View>
        //     )
        // else
        //     return (
        //         <View style={{ marginTop: 10 }}>
        //             <TouchableOpacity style={{ height: 150, alignItems: 'center', justifyContent: 'center', backgroundColor: Colors.background }}
        //                 onPress={() => this.openFilePicker()}>
        //                 <Ionicons size={40} name="ios-attach" color="black" />
        //             </TouchableOpacity>
        //         </View>
        //     )
        return (
            <TouchableOpacity
                style={{ alignItems: 'center', backgroundColor: Colors.background, flexDirection: 'row', flex: 1, marginTop: 20, padding: 10 }}
                onPress={() =>
                    this.openFilePicker()}>
                <View style={{ flex: 0.1, alignItems: 'center' }}>
                    {this.state.onBackground
                        ? <PercentageCircle
                            radius={17}
                            bgcolor={Colors.background}

                            borderWidth={3}
                            percent={this.state.uploadingPercentage}
                            color={Colors.primary}>

                            <TextView weight="medium" style={{ fontSize: 11, color: 'black' }}>
                                {this.state.uploadingPercentage}
                            </TextView>
                        </PercentageCircle>
                        : this.state.uploadingSuccess ?
                            <Ionicons size={30} name="ios-checkmark" color="black" />
                            : <Ionicons size={30} name="ios-attach" color="black" />
                    }

                </View>
                <View style={{ flex: 0.8 }}>
                    <TextView style={{ color: 'black', marginStart: 10 }} weight="bold">
                        {this.state.selectedFile !== null ? this.state.selectedFile.name + "(" + bytesToSize(this.state.selectedFile.size) + ")" : this.props.main.languageResource.r_announcement_new_add_attach || strings('r_announcement_new_add_attach')}
                    </TextView>
                </View>
                <TouchableOpacity style={{ flex: 0.1, alignItems: 'center' }}
                    onPress={() => {
                        this.setState({ selectedFile: null, onBackground: false })
                        this.setState({ fileUploadKey: createGuid() })
                    }} >
                    {this.state.selectedFile !== null ? <Ionicons size={20} name="ios-close" color="black" /> : null}
                </TouchableOpacity>
            </TouchableOpacity>
        )
    }
    _renderDeliveryDate() {
        if (this.state.addingActivityData.activityType === Constants.ActivityType.Assignment ||
            this.state.addingActivityData.activityType === Constants.ActivityType.Document ||
            this.state.addingActivityData.activityType === Constants.ActivityType.LinkActivity ||
            this.state.addingActivityData.activityType === Constants.ActivityType.Video)
            return (
                <View style={{ marginTop: 20 }}>
                    <TextView weight="bold" style={{ color: "black", fontSize: 18 }}>
                        {this.props.main.languageResource.r_activity_add_content_delivery_date || strings('r_activity_add_content_delivery_date')}
                    </TextView>
                    <View style={{ flexDirection: 'row', marginTop: 5, }}>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ deliveryDateProcessing: true, isDatePickerVisible: true, minimumDate: new Date() })}>
                            <View style={{ borderWidth: 1, padding: 10, borderColor: Colors.background, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TextView weight="bold" style={{ color: 'black' }}>{this.state.deliveryDate !== null ? this.state.deliveryDate : this.props.main.languageResource.r_activity_add_content_delivery_date_date || strings('r_activity_add_content_delivery_date_date')}</TextView>
                                <Ionicons size={20} name="ios-calendar" color="black" />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ isTimePickerVisible: true })}>
                            <View style={{ borderWidth: 1, padding: 10, borderColor: Colors.background, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TextView weight="bold" style={{ color: 'black' }}>{this.state.deliveryTime !== null ? this.state.deliveryTime : this.props.main.languageResource.r_activity_add_content_delivery_date_hour || strings('r_activity_add_content_delivery_date_hour')}</TextView>
                                <Ionicons size={20} name="md-time" color="black" />
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
            )
        else return null
    }


    _renderViewByCriterion() {
        return (
            <View style={{ marginTop: 20 }}>
                <TextView weight="bold" style={{ color: "black", fontSize: 18 }}>
                    {this.props.main.languageResource.r_activity_add_content_activity_view_criteria || strings('r_activity_add_content_activity_view_criteria')}
                </TextView>
                <ActivityByViewCriterion onChange={(criterion) => this.viewCriterionOnChange(criterion)} />
            </View>
        )
    }

    setSelectedWeeks(weeks) {
        var weekIds = [];
        weeks.map((week) => {
            console.log("week inf: ", week.termWeekId);
            weekIds.push(week.termWeekId)
        })
        console.log("week Ids: ", weekIds);
        this.setState({
            selecteedWeekIds: weekIds
        })
    }
    _renderViewByCriterionInputs() {
        switch (this.state.activityViewCriterion) {
            case Constants.ActivityViewCriterion.ByWeek:
                return (
                    <View style={{ marginTop: 10 }}>
                        <TextView style={{ color: "black" }}>
                            {this.props.main.languageResource.r_activity_add_content_display_weeks || strings('r_activity_add_content_display_weeks')}
                        </TextView>
                        <ActivityWeekList weeks={this.props.courseDetail.courseWeeks} selectedWeeks={(weeks) => this.setSelectedWeeks(weeks)} />
                    </View>
                )
            case Constants.ActivityViewCriterion.ByDateRange:
                return (
                    <View style={{ flexDirection: 'row', marginTop: 5, }}>
                        <TouchableOpacity style={{ flex: 1 }} onPress={() => this.setState({ viewByBeginDateProcessing: true, isDatePickerVisible: true, minimumDate: new Date() })}>
                            <View style={{ borderWidth: 1, padding: 10, borderColor: Colors.background, flexDirection: 'row', justifyContent: 'space-between' }}>
                                <TextView weight="bold" style={{ color: 'black' }}>{this.state.viewByBeginDate !== null ? this.state.viewByBeginDate : this.props.main.languageResource.r_activity_add_content_completion_date_start || strings('r_activity_add_content_completion_date_start')}</TextView>
                                <Ionicons size={20} name="ios-calendar" color="black" />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity disabled={this.state.viewByBeginDate == null ? true : false} style={{ flex: 1 }} onPress={() => this.setState({ viewByEndDateProcessing: true, isDatePickerVisible: true, minimumDate: new Date() })}>
                            <View style={{ borderWidth: 1, padding: 10, borderColor: Colors.background, flexDirection: 'row', justifyContent: 'space-between', opacity: this.state.viewByBeginDate == null ? 0.5 : 1 }}>
                                <TextView weight="bold" style={{ color: 'black' }}>{this.state.viewByEndDate !== null ? this.state.viewByEndDate : this.props.main.languageResource.r_activity_add_content_completion_date_end || strings('r_activity_add_content_completion_date_end')}</TextView>
                                <Ionicons size={20} name="ios-calendar" color="black" />
                            </View>
                        </TouchableOpacity>
                    </View>
                )
            case Constants.ActivityViewCriterion.ByActivity:
                return (
                    <View style={{ marginTop: 10 }}>

                        {this._renderSelectedActiviy()}
                        <TextInput
                            placeholder={this.props.main.languageResource.r_activity_add_content_search_activity || strings('r_activity_add_content_search_activity')}
                            style={{ borderColor: Colors.background, borderWidth: 1, padding: 5 }}
                            onChangeText={(text) =>
                                this.setState({
                                    conditionalActivities: this.props.addActivity.conditionalActivities.filter((activity) =>
                                        activity.activityName.startsWith(text)).slice(0, 5)
                                })
                            }
                            onTouchEnd={() => {
                                this.setState({ isConditionalActivitiesVisibility: !this.isConditionalActivitiesVisibility })
                            }} />
                        {this._renderConditionalActivitesFlatList()}
                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                            <TextInput keyboardType="numeric" style={{ borderColor: Colors.background, borderWidth: 1, padding: 5 }} onChangeText={(text) => {
                                if (text.length !== 0) {
                                    this.setState({ dateCriteria: 1 })
                                    this.setState({ datePrerequisiteActivityHours: parseInt(text) })
                                }
                                else {
                                    this.setState({ dateCriteria: 2 })
                                }
                            }} />
                            <TextView weight="regular" style={{ marginStart: 10 }}>
                                {this.props.main.languageResource.r_activity_add_content_completion_after_hour || strings('r_activity_add_content_completion_after_hour')}
                            </TextView>

                        </View>

                    </View>
                )
            default: return null
        }
    }
    _renderSelectedActiviy() {
        if (this.state.selectedConditionalActivity !== null) {
            return (
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', backgroundColor: Colors.background, marginTop: 5 }}>
                    <TouchableOpacity key={this.state.selectedConditionalActivity.id} style={{ margin: 5, padding: 5, backgroundColor: 'white', flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => this.removeConditionalActivity(this.state.selectedConditionalActivity)}>
                        <TextView weight="bold" style={{ color: 'black', fontSize: 12 }}>{this.state.selectedConditionalActivity.activityName}</TextView>
                        <Ionicons name="ios-close" size={20} color="black" style={{ marginStart: 10 }} />
                    </TouchableOpacity>
                </View>
            )
        } else return null;
    }

    conditionalActivitiesKeyExtractor = (item, index) => item.activityId
    _renderConditionalActivitesFlatList() {
        if (this.state.isConditionalActivitiesVisibility) {
            return (
                <FlatList
                    keyboardShouldPersistTaps="always"
                    keyExtractor={this.conditionalActivitiesKeyExtractor}
                    //data={this.props.addActivity.conditionalActivities}
                    data={this.state.conditionalActivities.slice(0, 5)}
                    renderItem={({ item }) => (
                        <TouchableOpacity style={{ padding: 10 }}
                            onPress={() => {
                                this.addConditionalActivity(item)
                            }}>
                            <TextView weight="regular" style={{ color: 'black' }}>{item.activityName}</TextView>
                        </TouchableOpacity>
                    )}
                />
            )
        } else return null
    }
    addConditionalActivity(activity) {
        this.setState({
            isConditionalActivitiesVisibility: false,
            selectedConditionalActivity: activity
        })
    }
    removeConditionalActivity(activity) {
        this.setState({ selectedConditionalActivity: null })
    }
    _renderCompletionCriterionValue() {
        if (this.state.ActivityCompletionType === Constants.ActivityCompletionTypes.Grade ||
            this.state.ActivityCompletionType === Constants.ActivityCompletionTypes.Progress) {
            return (
                <View style={{ marginTop: 5 }}>
                    <TextView weight="bold" style={{ color: 'black', fontSize: 15 }}>
                        {this.props.main.languageResource.r_activity_add_content_finishing_criteria_values || strings('r_activity_add_content_finishing_criteria_values')}
                    </TextView>
                    <TextInput
                        keyboardType="numeric"
                        onChangeText={(text) => { this.setState({ activityComplatetionValue: parseInt(text) }) }}
                        placeholder={this.props.main.languageResource.r_activity_add_content_finishing_criteria_value_input || strings('r_activity_add_content_finishing_criteria_value_input')}
                        style={{ borderColor: Colors.background, borderWidth: 1, padding: 5 }} />
                </View>
            )
        } else return null
    }
    _renderCompletionCriterion() {
        if (this.state.addingActivityData.activityType !== Constants.ActivityType.Dictionary)
            return (
                <View style={{ marginTop: 20 }}>
                    <TextView weight="bold" style={{ color: "black", fontSize: 18 }}>
                        {this.props.main.languageResource.r_activity_add_content_completion_criteria || strings('r_activity_add_content_completion_criteria')}
                    </TextView>
                    <ActivityCompletionCriterion
                        onChange={(criterion) => this.completionCriterionChange(criterion)}
                        activityType={this.state.addingActivityData.activityType}
                        selectedFile={this.state.selectedFile} />
                    {this._renderCompletionCriterionValue()}
                </View>
            )
        else return null
    }
    completionCriterionChange(criterion) {
        this.setState({
            ActivityCompletionType: criterion
        })
    }
    viewCriterionOnChange(criterion) {
        this.setState({
            activityViewCriterion: criterion
        })

        switch (criterion) {
            case Constants.ActivityViewCriterion.ByWeek:
                this.setState({ dateCriteria: 0 })
                break;
            case Constants.ActivityViewCriterion.ByDateRange:
                this.setState({ dateCriteria: 1 })
                break;
            case Constants.ActivityViewCriterion.ByActivity:
                this.setState({ dateCriteria: 2 })
                break;
        }
    }

    hideDatePicker = () => {
        this.setState({ isDatePickerVisible: false, deliveryDateProcessing: false, viewByBeginDateProcessing: false, viewByEndDateProcessing: false });
    };
    handleDatePicked = date => {
        if (this.state.deliveryDateProcessing) {
            this.setState({ deliveryDate: Moment(date).format('YYYY-MM-DD') })
        } else if (this.state.viewByBeginDateProcessing) {
            this.setState({ viewByBeginDate: Moment(date).format('YYYY-MM-DD') })
        } else if (this.state.viewByEndDateProcessing) {
            this.setState({ viewByEndDate: Moment(date).format('YYYY-MM-DD') })
        }
        this.hideDatePicker();
    };
    hideTimePicker = () => {
        this.setState({ isTimePickerVisible: false })
    }
    handleTimePicked = time => {
        this.setState({ deliveryTime: Moment(time).format('hh:mm') })
        this.hideTimePicker();

    }
    _renderDatePicker() {
        return (
            <DateTimePicker
                mode="date"
                minimumDate={this.state.minimumDate}
                isVisible={this.state.isDatePickerVisible}
                onConfirm={this.handleDatePicked}
                onCancel={this.hideDatePicker}
            />
        )
    }
    _renderTimePicker() {
        return (
            <DateTimePicker
                mode="time"
                isVisible={this.state.isTimePickerVisible}
                onConfirm={this.handleTimePicked}
                onCancel={this.hideTimePicker}
            />
        )
    }
    _renderActionButtons() {
        return (
            <View style={{ alignSelf: 'flex-end', bottom: 0, }}>
                <Button disabled={false} title={this.props.main.languageResource.r_activity_add_content_next_button || strings('r_activity_add_content_next_button')}
                    onPress={() => this.routeToActivitySettings()}
                    icon={<Icon name="arrowright" size={20} color="white" />}
                    iconRight
                    containerStyle={{ margin: 10 }}
                    buttonStyle={{ backgroundColor: Colors.primary }}
                    titleStyle={{ marginEnd: 8, fontFamily: Platform.OS === 'ios' ? 'SFProText-Medium' : 'Roboto-Medium' }} />
            </View>
        )
    }

    inputValidation() {

        //empty link adress
        if (this.state.addingActivityData.activityType === Constants.ActivityType.LinkActivity && this.state.linkActivityUrl === "") {
            Alert.alert('', this.props.main.languageResource.r_activity_add_content_error_empty_url || strings('r_activity_add_content_error_empty_url'))
            return false
        }
        //empty activity name
        if (this.state.activityName === "") {
            Alert.alert('', this.props.main.languageResource.r_activity_add_content_error_empty_name || strings('r_activity_add_content_error_empty_name'))
            return false
        }
        //empty delivery date for assignment
        if (this.state.addingActivityData.activityType === Constants.ActivityType.Assignment && this.state.deliveryDate === null) {
            Alert.alert('', this.props.main.languageResource.r_activity_add_content_error_empty_delivery_date || strings('r_activity_add_content_error_empty_delivery_date'))
            return false
        }
        //Aktivite Zamanlaması belli tarih aralığında seçilmiş olup, tarihler boş olursa
        if (this.state.activityViewCriterion === Constants.ActivityViewCriterion.ByDateRange && (this.state.viewByBeginDate === null || this.state.viewByEndDate === null)) {
            Alert.alert('', this.props.main.languageResource.r_activity_add_content_error_empty_start_end_date || strings('r_activity_add_content_error_empty_start_end_date'))
            return false
        }
        if (this.state.activityViewCriterion === Constants.ActivityViewCriterion.ByActivity && this.state.selectedConditionalActivity === null) {
            Alert.alert('', this.props.main.languageResource.r_activity_add_content_error_conditional_activity_empty || strings('r_activity_add_content_error_conditional_activity_empty'))
            return false
        }

        if (this.state.activityViewCriterion === Constants.ActivityViewCriterion.ByWeek && this.state.selecteedWeekIds.length === 0) {
            Alert.alert('', this.props.main.languageResource.r_activity_add_content_error_empty_weeks || strings('r_activity_add_content_error_empty_weeks'))
            return false
        }
        //activity activityComplatetionValue 0-100 arasında olmalı
        if ((this.state.ActivityCompletionType === Constants.ActivityCompletionTypes.Grade || this.state.ActivityCompletionType === Constants.ActivityCompletionTypes.Progress)
            && (this.state.activityComplatetionValue < 1 || this.state.activityComplatetionValue > 100)) {
            Alert.alert('', this.props.main.languageResource.r_activity_add_content_error_completion_value || strings('r_activity_add_content_error_completion_value'))
            return false
        }
        if (this.state.addingActivityData.activityType === Constants.ActivityType.Assignment && this.state.activityViewCriterion === Constants.ActivityViewCriterion.ByDateRange && (Moment(this.state.viewByEndDate) > Moment(this.state.deliveryDate))) {
            Alert.alert('', this.props.main.r_activity_add_content_error_deliverydate_viewdate || strings('r_activity_add_content_error_deliverydate_viewdate'))
            return false
        }
        return true;
    }
    routeToActivitySettings() {
        if (this.inputValidation()) {
            this.state.addingActivityData.name = this.state.activityName;
            this.state.addingActivityData.description = this.state.activityDescription;
            this.state.addingActivityData.fileInputKey = this.state.fileUploadKey
            this.state.addingActivityData.deadline = this.state.deliveryDate !== null ? this.state.deliveryDate + (this.state.deliveryTime ? "T"+this.state.deliveryTime : 'T00:00:00') : null;
            this.state.addingActivityData.dateCriteria = this.state.dateCriteria;
            this.state.addingActivityData.termWeekIds = this.state.selecteedWeekIds;
            this.state.addingActivityData.activityConditionalBeginDate = this.state.viewByBeginDate !== null ? this.state.viewByBeginDate + "T00:00:00" : null;
            this.state.addingActivityData.activityConditionalEndDate = this.state.viewByEndDate !== null ? this.state.viewByEndDate + "T00:00:00" : null;
            this.state.addingActivityData.datePrerequisiteActivityId = this.state.selectedConditionalActivity ? this.state.selectedConditionalActivity.activityId : null,
                this.state.addingActivityData.datePrerequisiteActivityHours = this.state.datePrerequisiteActivityHours;
            this.state.addingActivityData.prerequisiteActivityId = this.state.selectedConditionalActivity ? this.state.selectedConditionalActivity.activityId : null,
                this.state.addingActivityData.activityCompletionType = this.state.ActivityCompletionType;
            this.state.addingActivityData.activityComplatetionValue = this.state.activityComplatetionValue;
            this.state.addingActivityData.selectedFile = this.state.selectedFile;
            this.state.addingActivityData.allowDownload = this.state.videoAllowDownload;
            this.state.addingActivityData.fileEmbed = this.state.fileEmbed ? this.state.fileEmbed : null
            this.state.addingActivityData.videoType = this.state.videoType ? this.state.videoType : Constants.VideoTypes.Local

            console.log("adding Activity Data: ", this.state.addingActivityData);
            this.props.navigation.navigate('NewActivitySettings', { addingActivityData: this.state.addingActivityData })
        }
    }

    uploadOnCancel() {
        this.setState({ selectedFile: null, onBackground: false })
    }
    onError() {
        Alert.alert('', this.props.main.languageResource.r_activity_add_content_file_uploading_error || strings('r_activity_add_content_file_uploading_error'))
        this.setState({ selectedFile: null, onBackground: false })
    }
    uplaodOnBackground() {
        this.setState({ uploadingVisibility: false, onBackground: true })
    }
    _renderUploading() {
        if (this.state.selectedFile !== null) {
            return <Uploader uploadingData={this.uploadFileRequestApiBody}
                visibility={this.state.uploadingVisibility}
                onCancel={() => this.uploadOnCancel()}
                onError={() => this.onError()}
                onBackground={() => this.uplaodOnBackground()}
                uplaodingProgress={((percentage) => this.setState({ uploadingPercentage: percentage }))}
                onSuccess={() => {
                    this.state.addingActivityData.fileInputKey = this.state.fileUploadKey
                    this.setState({ uploadingSuccess: true, onBackground: false, uploadingVisibility: false })
                }
                }
            />
        } else return null
    }
    _renderUrlInput() {
        if (this.state.addingActivityData.activityType === Constants.ActivityType.LinkActivity) {
            return (
                <View style={{ marginTop: 10 }}>
                    <TextView weight="bold" style={{ color: "black", fontSize: 18 }}>
                        {this.props.main.languageResource.r_activity_add_content_activity_url || strings('r_activity_add_content_activity_url')}
                    </TextView>
                    <TextInput
                        keyboardType={Platform.OS === "ios" ? "url" : "default"}
                        style={{ fontSize: 16, height: 40, borderColor: Colors.background, borderWidth: 1, marginTop: 5 }}
                        placeholder={this.props.main.languageResource.r_activity_add_content_activity_url_input_placeholder || strings('r_activity_add_content_activity_url_input_placeholder')}
                        onChangeText={text => this.setState({ linkActivityUrl: text })} />
                </View>
            )
        } else return null;
    }

    _renderDownloadableVideo() {
        if (this.state.addingActivityData.activityType === Constants.ActivityType.Video) {
            return (
                <View style={{ marginTop: 10 }}>
                    <TextView weight="bold" style={{ color: "black", fontSize: 18, marginBottom: 5 }}>
                        {this.props.main.languageResource.r_activity_add_content_allow_download_video || strings('r_activity_add_content_allow_download_video')}
                    </TextView>
                    <Switch style={{ alignSelf: 'flex-start' }} value={this.state.videoAllowDownload}
                        onValueChange={() => this.setState({
                            videoAllowDownload: !this.state.videoAllowDownload
                        })} />
                </View>
            )
        }

    }
    render() {
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <Loader loading={this.props.courseDetail.courseWeeksFetching} />
                <ScrollView contentContainerStyle={{ padding: 10 }}>
                    {this._renderUrlInput()}
                    {this._renderActivityNameInput()}
                    {this._renderActivityDesciption()}
                    {this._renderFileSelector()}
                    {this._renderDeliveryDate()}
                    {this._renderViewByCriterion()}
                    {this._renderViewByCriterionInputs()}
                    {this._renderCompletionCriterion()}
                    {this._renderDatePicker()}
                    {this._renderTimePicker()}
                    {this._renderDownloadableVideo()}
                    {this._renderActionButtons()}
                </ScrollView>
                {this._renderUploading()}
            </SafeAreaView>
        )
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        getTagsConditionalActivity: (...args) => dispatch(AddActivityActions.conditionalActivitiesRequest(...args)),
        uploadFileRequest: (...args) => dispatch(AddActivityActions.uploadFileRequest(...args)),
        getFileSettings: (...args) => dispatch(SettingsActions.getFileSettingsRequest(...args)),

    }
}

const mapStateToProps = (state) => {
    return {
        addActivity: state.addActivity,
        courseDetail: state.courseDetail,
        main: state.main,
        settings: state.settings,

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewActivityContents)